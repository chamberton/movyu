//
//  MYUCriterion.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/03.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYUCriterion : NSObject
@property (readwrite) int ID;
@property (nonatomic,readwrite) NSString* name;
@property (readwrite) BOOL selected;
@end
