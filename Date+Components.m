//
//  Date+Components.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/15.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Date+Components.h"

@implementation NSDate(Date_Components)  
-(NSUInteger) year{
    if (self==nil)
        return 0;
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateComponents * comp = [cal components:( NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute|NSCalendarUnitSecond) fromDate:self];
    
    return comp.year;
}
-(NSUInteger) month{
    if (self==nil)
        return 0;
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateComponents * comp = [cal components:( NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute|NSCalendarUnitSecond) fromDate:self];
    
    return comp.month;
}

-(NSUInteger) day{
    if (self==nil)
        return 0;
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateComponents * comp = [cal components:( NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute|NSCalendarUnitSecond) fromDate:self];
    
    return comp.day;
}
@end
