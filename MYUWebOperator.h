//
//  MYUWebOperator.h
//  MovYU
//  Web operator, responsible for performing the Web services queries 
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUUserDefaults.h"

@interface MYUWebOperator : NSObject
@property (strong, readwrite,nonatomic) NSMutableArray* moviesInCinemas;

@property (strong, readwrite,nonatomic) NSMutableArray* moviesMostPopulars;
@property (strong, readwrite,nonatomic) NSMutableArray* latestTVShowsS;
@property (strong, readwrite,nonatomic) NSMutableArray* topRatedTVShowsS;
@property (strong, readwrite,nonatomic) NSMutableArray* moviesInSearch;
@property (strong, readwrite,nonatomic) NSMutableArray* TVSHowsInSearch;
@property (strong, readwrite,nonatomic) NSMutableArray* airingTodayTVShowsS;

@property (strong, readonly,nonatomic)  NSDictionary* API_URLs;

@property (strong, readwrite,nonatomic) NSDictionary* moviesInCinema;
@property (strong, readwrite,nonatomic) NSArray* moviesInCinemaFromDB;
@property (strong, readwrite,nonatomic) NSError* moviesInCinemaError;

@property (strong, readwrite,nonatomic) NSDictionary* mostPopularMovies;
@property (strong, readwrite,nonatomic) NSArray* mostPopularMoviesFromDB;
@property (strong, readwrite,nonatomic) NSError* mostPopularError;

@property (strong, readwrite,nonatomic) NSDictionary* airingTodayTVShows;
@property (strong, readwrite,nonatomic) NSArray* airingTodayTVShowsFromDB;
@property (strong, readwrite,nonatomic) NSError* airingTodayTVShowsError;

@property (strong, readwrite,nonatomic) NSDictionary* topRatedTVShows;
@property (strong, readwrite,nonatomic) NSArray* topRatedTVShowsFromDB;
@property (strong, readwrite,nonatomic) NSError* topRatedTVShowsError;


@property (strong, readwrite,nonatomic) NSDictionary* latestTVShows;
@property (strong, readwrite,nonatomic) NSArray* latestTVShowsFromDB;
@property (strong, readwrite,nonatomic) NSError* latestTVShowsError;

@property (strong, readwrite,nonatomic) NSDictionary*  movieCast;
@property (strong, readwrite,nonatomic) NSError*   movieCastError;

@property (strong, readwrite,nonatomic) NSDictionary*  tvShowCast;
@property (strong, readwrite,nonatomic) NSError*   tvShowCastError;

@property (strong, readwrite,nonatomic) NSDictionary*  tvShowDetails;
@property (strong, readwrite,nonatomic) NSError*   tvShowDetailsError;


@property (strong, readwrite,nonatomic) NSDictionary* searchedMovies;
@property (strong, readwrite,nonatomic) NSDictionary* searchedTVShows;
@property (strong, readwrite,nonatomic) NSError* searchedMoviesError;
@property (strong, readwrite,nonatomic) NSError*  searchedTVShowError;
@property BOOL connectedToInternet;
@property BOOL retrievedFromDBTV;
@property BOOL retrievedFromDBMovie;
@property (readwrite) BOOL hasLoadedMovieCast;
@property (readwrite) BOOL hasLoadedMovieDetails;
@property (readwrite) BOOL hasLoadedTVShowCast ;
@property (readwrite) BOOL hasLoadedTVShowDetails;
@property (readwrite) BOOL completedTVShowOperation;

@property (nonatomic) int moviesInCinemaPageNumber;
@property (nonatomic) int mostPopularMoviesInCinemaPageNumber;
@property (nonatomic) int topRatedTVShowPageNumber;
@property (nonatomic) int movieSearchPageNumber;
@property (nonatomic) int airingTodayTVShowPageNumber;
@property (nonatomic) int latestTVShowsPageNumber;
@property (nonatomic) int tvShowSearchPageNumber;

@property BOOL loadingMovies;
@property BOOL laodingTVShows;
+(MYUWebOperator*) sharedWebOperator;
-(NSString*) urlForMoviesInCinema;

-(void) refreshMoviesInCinema;
-(void) refreshMoviesMostPopularInCinema;

-(void) refreshTVShowsLatest;
-(void) refreshTVShowsAiringToday;
-(void) refreshTVShowsTopRated;

-(NSString*) urlForPhotoPath;
-(void) castForMovieWithID: (int) movieID;
-(void) detailsForMovieWithID: (int) movieID;
-(NSURL*)urlForMovieImage: (int) movieID;
- (NSURL*)urlForTVShowImage:(int)tvShowID;
-(void) castForTVWithID: (int) tvShowID;
-(void) detailsForTVWithID:(int) tvShowID;

- (void)trailerForMovieWithID:(int)movieID :(void (^)(NSArray *arrayOfTrailers))completion;
- (void)trailerForTVShowWithID:(int)movieID :(void (^)(NSArray *arrayOfTrailers))completion;

-(void) searchMovies:(BOOL) latestOnly
       titleKeyWord : (NSString*) titleKeyWord
       actorKeyWord : (NSString*) actorKeyWord
             genres : (NSArray*) genreIDs
      ratingLowBound: (float) ratingLowBound
    ratingUpperBound: (float) ratingUpperBound
              others: (NSDictionary*) other;

-(void) searchTVShows:(BOOL) latestOnly
        titleKeyWord : (NSString*) titleKeyWord
        actorKeyWord : (NSString*) actorKeyWord
              genres : (NSArray*)    genreIDs
       ratingLowBound: (float)       ratingLowBound
     ratingUpperBound: (float)       ratingUpperBound
               others: (NSDictionary*) other;
@end
