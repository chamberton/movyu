//
//  Seasons+CoreDataProperties.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Seasons+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Seasons (CoreDataProperties)

+ (NSFetchRequest<Seasons *> *)fetchRequest;

@property (nonatomic) int16_t numberOfEpisodes;
@property (nullable, nonatomic, copy) NSDate *releaseDate;
@property (nonatomic) int16_t seasonNumber;
@property (nullable, nonatomic, retain) TVShows *parent;

@end

NS_ASSUME_NONNULL_END
