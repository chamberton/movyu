//
//  MYUUserDefaults.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/13.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUUserDefaults.h"

@implementation MYUUserDefaults
+(void) storeInteger:(NSUInteger) integer
              forKey: (NSString*) key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:integer forKey:key];
    [defaults synchronize];
}
+(NSUInteger) getIntegerWithKey : (NSString*) key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSUInteger storedNumber = [defaults integerForKey:key];
    return storedNumber;
}
+(void) storeBool:(BOOL) boolean
           forKey: (NSString*) key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:boolean forKey:key];
    [defaults synchronize];
}
+(BOOL) getBoolWithKey : (NSString*) key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL storedBoolean = [defaults boolForKey:key];
    return storedBoolean;
}
+(void) storeString:(NSString*) string
             forKey: (NSString*) key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:string  forKey:key];
    [defaults synchronize];
}
+(NSString*) stringForKey : (NSString*) key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* storedSring = [defaults stringForKey:key];
    
    return storedSring;
}

@end
