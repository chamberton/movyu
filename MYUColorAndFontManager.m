//
//  MYUColorAndFontManager.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUColorAndFontManager.h"

@implementation MYUColorAndFontManager


+(UIColor*) backgroundColorForUIElementWithTag : (int) tagNumber{
    switch (tagNumber) {
        case 1:
            return [UIColor blackColor];
        case 2:
            return [UIColor grayColor];
        default:
            return [UIColor blackColor];
    }
}
+(UIColor*) textColorForUIElementWithTag   : (int) tagNumber{
    return [UIColor whiteColor];
}
+(UIColor*) borderColorForUIElementWithTag : (int) tagNumber{
    return [UIColor blackColor];
}
+(UIFont*) fontForUIElementWithTag : (int) tagNumber{
    return  [UIFont boldSystemFontOfSize:10];
}
+(UIColor*) textColorForTitleOfUIElementWithTag:(int) tag{
  return  [UIColor colorWithWhite:1.0f alpha:1.0f];
}
+(UIColor*) backgroundColorForTitleOfUIElementWithTag:(int) tag{
   return [UIColor clearColor];
}
+(UIColor*) shadowColorForTitleOfUIElementWithTag:(int) tag{
 return [UIColor colorWithWhite:1.0f alpha:0.5f];
}
+(UIFont*) fontForTitleOfUIElementWithTag:(int) tag{
  return   [UIFont boldSystemFontOfSize:15.0f];
}

@end
