//
//  Genres+CoreDataProperties.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Genres+CoreDataProperties.h"

@implementation Genres (CoreDataProperties)

+ (NSFetchRequest<Genres *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Genres"];
}

@dynamic genre;
@dynamic genreID;

@end
