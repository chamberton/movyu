//
//  Genres+CoreDataProperties.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Genres+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Genres (CoreDataProperties)

+ (NSFetchRequest<Genres *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *genre;
@property (nonatomic) int16_t genreID;

@end

NS_ASSUME_NONNULL_END
