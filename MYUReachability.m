//
//  MYUReachability.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/13.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//
#import "Reachability.h"
#import "MYUReachability.h"
#import "MYUWebOperator.h"

static MYUReachability* reachabilityController =nil;

@interface MYUReachability ()
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@end


@implementation MYUReachability

+(void) load{
    reachabilityController=[MYUReachability sharedInstance];
}
/// Singleton pattern

+(MYUReachability*) sharedInstance {
    if (reachabilityController==nil){
        reachabilityController = [[MYUReachability alloc] init];
    }
    return reachabilityController ;
}
+(void) createSharedInstance{
    if (reachabilityController==nil){
        reachabilityController = [[MYUReachability alloc] init];
    }
}


- (instancetype)init
{
    /*
     Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the method reachabilityChanged will be called.
     */
    self= [super init];
    if (self){
        _wifiRechabilityStatus = NO;
        _cellularRechabilityStatus = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        
        //Change the host name here to change the server you want to monitor.
        NSString *remoteHostName = @"www.apple.com";
        self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
        [self.hostReachability startNotifier];
        [self updateInterfaceWithReachability:self.hostReachability];
        
        self.internetReachability = [Reachability reachabilityForInternetConnection];
        [self.internetReachability startNotifier];
        [self updateInterfaceWithReachability:self.internetReachability];
    }
    return self;
}


/*!
 * Called by Reachability whenever status changes.
 */
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}


- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    if (reachability == self.hostReachability)
    {
         [reachability connectionRequired];
        
    }
    
    if (reachability == self.internetReachability)
    {
        DebugLog(@"internet connection active");
        [self checkReachability:reachability];
    }
    
}

+(void) checkForReachability{
    [[MYUReachability sharedInstance] checkReachability: [MYUReachability sharedInstance].internetReachability ];
    [[MYUReachability sharedInstance] checkReachability: [MYUReachability sharedInstance].hostReachability ];
    [[MYUReachability sharedInstance].internetReachability currentReachabilityStatus];
    [[MYUReachability sharedInstance].hostReachability currentReachabilityStatus];
    
}
- (void)checkReachability:(Reachability*) reachability
{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus)
    {
        case NotReachable:        {
            
            [MYUWebOperator sharedWebOperator].connectedToInternet = NO;
            statusString = NSLocalizedString(@"Access Not Available", @"Text field text for access is not available");
            DebugLog(@"No connection");
            /*
             Minor interface detail- connectionRequired may return YES even when the host is unreachable. We cover that up here...
             */
            connectionRequired = NO;
            
            break;
        }
            
        case ReachableViaWWAN:        {
            statusString = NSLocalizedString(@"Reachable WWAN", @"");
            DebugLog(@"Cellular network connected");
            _cellularRechabilityStatus  = YES;
            _wifiRechabilityStatus      = NO;
             [MYUWebOperator sharedWebOperator].connectedToInternet = YES;
            break;
        }
        case ReachableViaWiFi:        {
            DebugLog(@"Wifi network connected");
            _wifiRechabilityStatus = YES;
            _cellularRechabilityStatus  = NO;
              [MYUWebOperator sharedWebOperator].connectedToInternet = YES;
            break;
        }
    }
    
    if (connectionRequired)
    {
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
    
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end

