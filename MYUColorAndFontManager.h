//
//  MYUColorAndFontManager.h
//  MovYU
//  Font manager
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface MYUColorAndFontManager : NSObject
/*
 * returns the background color correspdoning to the given tag number
 */
+(UIColor*) backgroundColorForUIElementWithTag : (int) tagNumber;


/*
 * returns the text color correspdoning to the given tag number
 */
+(UIColor*) textColorForUIElementWithTag       : (int) tagNumber;

/*
 * returns the border color correspdoning to the given tag number
 */
+(UIColor*) borderColorForUIElementWithTag     : (int) tagNumber;

+(UIFont*) fontForUIElementWithTag : (int) tagNumber;

+(UIColor*) textColorForTitleOfUIElementWithTag:(int) tag;
+(UIColor*) backgroundColorForTitleOfUIElementWithTag:(int) tag;
+(UIColor*) shadowColorForTitleOfUIElementWithTag:(int) tag;
+(UIFont*) fontForTitleOfUIElementWithTag:(int) tag;
@end
