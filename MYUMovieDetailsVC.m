//
//  MYUMovieDetailsVC.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/14.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUMovieDetailsVC.h"
#import "MYUCache.h"
#import "MYUWebOperator.h"
#import "MYUThumbailFactory.h"
#import "MYUStringFactory.h"
#import "MYUVideoFactory.h"
#import "MYUTrailerTableViewController.h"
@import GoogleMobileAds;

@interface MYUMovieDetailsVC ()
@property (weak, nonatomic) GADBannerView *bannerView;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,weak) UIImage* image;
@property (weak, nonatomic) IBOutlet UIButton *showTrailerButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *orignalLanguage;
@property (weak, nonatomic) IBOutlet UILabel *ratingAverage;
@property (weak, nonatomic) IBOutlet UILabel *popularity;
@property (weak, nonatomic) IBOutlet UISwitch *adultOnlySwitch;
@property (weak, nonatomic) IBOutlet UITextView *synopsisTextView;
@property (weak, nonatomic) IBOutlet UITextView *genreTextView;
@property (weak, nonatomic) IBOutlet UITextView *castActorsTextView;
@property (weak, nonatomic) IBOutlet UITextField *dateDayTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateMonthTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateYearTextField;
@property (weak, nonatomic) IBOutlet UILabel *directorsLabel;
@property (nonatomic, strong) NSOperationQueue *backgroundImageQueue;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) NSArray *trailers;
@property (weak, nonatomic) IBOutlet UILabel *movieInfoTitle;
@property (weak, nonatomic) IBOutlet UILabel *starringLabel;
@property (weak, nonatomic) IBOutlet UILabel *synopsisLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *directorLabel;
@property (weak, nonatomic) IBOutlet UILabel *popularityLabel;
@property (weak, nonatomic) IBOutlet UILabel *adultOnlyLabel;
@property (weak, nonatomic) IBOutlet UILabel *voteAverageLabel;
@property (weak, nonatomic) IBOutlet UILabel *originLanguageLabel;
@property   int scrollinPosition  ;
@property CGFloat requireHeight;
@property CGFloat actualHeight;
@property CGFloat  scrollingIncrement;
@end
static dispatch_queue_t loadingCastQueue;


@implementation MYUMovieDetailsVC

+(void) load{
    loadingCastQueue = dispatch_queue_create("loadingCastQueue", NULL);
}

- (void)setupLocalizedTitles {
    self.starringLabel.text = NSLocalizedString(@"Starring", @"Starring");
    self.synopsisLabel.text = NSLocalizedString(@"Synopsis", @"Synopsis");
    self.genreLabel.text = NSLocalizedString(@"Genre", @"Genre");
    self.directorLabel.text = NSLocalizedString(@"Director", @"Director");
    self.popularityLabel.text = NSLocalizedString(@"Popularity", @"Popularity");
    self.adultOnlyLabel.text = NSLocalizedString(@"Adult Only", @"Adult Only");
    self.voteAverageLabel.text = NSLocalizedString(@"Vote Average", @"Vote Average");
    self.originLanguageLabel.text = NSLocalizedString(@"Original Language", @"Original Language");
    self.movieInfoTitle.text = NSLocalizedString(@"Movie Info", @"Movie Info");
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateNormal];
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateDisabled];
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateSelected];
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateHighlighted];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLocalizedTitles];
    
    self.showTrailerButton.enabled = NO;
    self.showTrailerButton.alpha  = DISABLED_BUTTON_ALPHA;
    
    [[MYUWebOperator sharedWebOperator] trailerForMovieWithID:self.movieItem.movieID :^(NSArray *arrayOfTrailers) {
        if (arrayOfTrailers.count>0){
            self.trailers = arrayOfTrailers;
            self.showTrailerButton.enabled = YES;
            self.showTrailerButton.alpha  = ENABLED_BUTTON_ALPHA;
        }
    }];
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    self.bannerView.adUnitID = @"ca-app-pub-3836110455040592/5443794461";
    self.bannerView.rootViewController = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateGenreTextField) name:kGenreIDNotification object:nil];
    if (_movieItem==nil){
        _image =[[MYUCache sharedCache] missingPicture];
    }else{
        _image=_movieItem.videoThumbnail.poster;
    }
    
    if(_image==nil){
        self.backgroundImageQueue = [[NSOperationQueue alloc] init];
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            
            UIImage *image =  [MYUThumbailFactory imageWithTitle:_movieItem.videoThumbnail.title andURL:_movieItem.videoThumbnail.imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                // display the item if is currently on the screen
                _imageView.image = image;
                self.backgroundView.image =image;
            });
        }];
        operation.queuePriority =  NSOperationQueuePriorityHigh ;
        
        [self.backgroundImageQueue addOperation:operation];
    }else{
        _imageView.image = _image;
        if([[MYUCache sharedCache] missingPicture] !=_image)
            self.backgroundView.image =_image;
    }
    
    _imageView.layer.masksToBounds = YES;
    _imageView.layer.borderColor = [UIColor blackColor].CGColor;
    _imageView.layer.borderWidth =  8;
    _imageView.layer.cornerRadius = 5;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.opaque = NO;
    self.scrollView.opaque = NO;
    self.backgroundView.alpha = 0.25;
    
    
    __weak MYUWebOperator* operator = [MYUWebOperator sharedWebOperator];
    [operator addObserver:self forKeyPath:MOVIE_CAST_OBTAINED options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = NSLocalizedString(@"Loading", @"HUD loading title");
    _hud.detailsLabel.text = NSLocalizedString(@"Parsing data", @"HUD title");
    _genreTextView.text = @"";
    dispatch_async(loadingCastQueue,^{
        [operator castForMovieWithID:_movieItem.movieID];
        
    });
    
    _orignalLanguage.text=_movieItem.originalLanguage;
    _ratingAverage.text =[NSString stringWithFormat:@"%0.3f", _movieItem.voteAverage];
    _popularity.text =[NSString stringWithFormat:@"%0.3f", _movieItem.popularity];
    _adultOnlySwitch.on = (_movieItem.isAdultOnly)?YES:NO;
    _synopsisTextView.text =_movieItem.synopsis;
    self.castActorsTextView.textColor = [UIColor redColor];
    self.directorsLabel.textColor = [UIColor redColor];
    self.castActorsTextView.text= NSLocalizedString( @"NOT RETRIEVED",@"NOT RETRIEVED");
    self.directorsLabel.text= NSLocalizedString( @"NOT RETRIEVED",@"NOT RETRIEVED");
    
    [self updateGenreTextField];
    NSInteger month = [_movieItem.releaseDate month];
    if (month>0 && month <=12){
        NSString *monthName = [[[NSDateFormatter alloc] init] monthSymbols][month-1];
        _dateDayTextField.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_movieItem.releaseDate day]];
        _dateMonthTextField.text = [NSString stringWithFormat:@"%@",monthName];
        _dateYearTextField.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_movieItem.releaseDate year]];
    }
    _scrollinPosition=0;
    
}
- (IBAction)watchTrailerButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"ShowTrailerList" sender:self.movieItem];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowTrailerList"]){
        MYUTrailerTableViewController* destinationVC = segue.destinationViewController;
        destinationVC.videoItem = sender;
        destinationVC.trailerList = self.trailers;
        destinationVC.title =[NSString stringWithFormat:@"%@ (%@)",self.movieItem.title ,NSLocalizedString(@"Trailers", @"Trailers")];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [_synopsisTextView setContentOffset:CGPointZero animated:NO];
    [self shouldShowMore];
}

- (IBAction)scrollDown:(id)sender {
    
    if(_scrollinPosition<(_requireHeight - _actualHeight))
    {
        _scrollinPosition+=_scrollingIncrement;
        [_synopsisTextView setContentOffset:CGPointMake(0, _scrollinPosition)];
    }
    
}
- (IBAction)scrollUp:(id)sender {
    if(_scrollinPosition>_scrollingIncrement)
    {
        _scrollinPosition-=_scrollingIncrement;
        [_synopsisTextView setContentOffset:CGPointMake(0, _scrollinPosition)];
    }else{
        [_synopsisTextView setContentOffset:CGPointZero animated:NO];
        _scrollinPosition = 0;
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [scrollView resignFirstResponder];
}

-(void) updateGenreTextField{
    NSMutableString* concatenatedGenres =[NSMutableString string];
    for(NSNumber* item in _movieItem.genreIDS){
        concatenatedGenres =(concatenatedGenres.length==0)?[NSMutableString stringWithFormat:@"%@",item]:[NSMutableString stringWithFormat:@"%@,%@",concatenatedGenres,item];
    }
    if (concatenatedGenres.length>0){
        _genreTextView.text = concatenatedGenres;
        self.directorsLabel.textColor = [UIColor blackColor];
    }
    
}

-(BOOL) shouldShowMore{
    _actualHeight= _synopsisTextView.frame.size.height;
    CGFloat fixedWidth = _synopsisTextView.frame.size.width;
    
    CGSize newSize = [_synopsisTextView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    if(newSize.height>(_actualHeight-5)){
        _requireHeight = newSize.height;
        _scrollingIncrement = _actualHeight/2+0.1;
        return YES;
    }
    _upButton.hidden= YES;
    _downButton.hidden = YES;
    return NO;
    
}
-(void) viewDidAppear:(BOOL)animated{
    
    [_castActorsTextView setContentOffset:CGPointZero animated:NO];
    [_synopsisTextView setContentOffset:CGPointZero animated:NO];
    
    [_synopsisTextView resignFirstResponder];
    [self shouldShowMore];
    [self.bannerView loadRequest:[GADRequest request]];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if (!keyPath  || keyPath.length<1){
        return;
    }
    if([keyPath isEqualToString:MOVIE_CAST_OBTAINED]){
        NSDictionary* castDisctionary = [MYUWebOperator sharedWebOperator].movieCast;
        __weak MYUMovieItem* weakMovieItem = _movieItem;
        _movieItem = [MYUVideoFactory addCastDetails:castDisctionary to:weakMovieItem];
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.castActorsTextView.textColor = [UIColor blackColor];
        self.directorsLabel.textColor = [UIColor blackColor];
        self.castActorsTextView.text = _movieItem.combinedActors;
        self.directorsLabel.text =_movieItem.combinedDirectors;
    });
    
    @try{
        [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:MOVIE_CAST_OBTAINED];
        [MYUWebOperator sharedWebOperator].hasLoadedMovieCast = NO;
    }@catch(NSException* anException){
        NSLog(@"%@",anException.description);
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_hud hideAnimated:YES];
    });
}

- (IBAction)dismissView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:NO];
    [self.backgroundImageQueue cancelAllOperations];
    
    @try{
        [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:MOVIE_CAST_OBTAINED];
        [MYUWebOperator sharedWebOperator].hasLoadedMovieCast = NO;
    }@catch(NSException* anException){
        NSLog(@"%@",anException.description);
    }
}

@end
