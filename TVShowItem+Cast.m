//
//  TVShowItem+Cast.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/16.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "TVShowItem+Cast.h"

@implementation MYUTVShowItem(TVShowItem_Cast)
-(void) addCastDetails:(NSDictionary*) castDisctionary{
    NSArray* cast =[castDisctionary objectForKey:@"cast"];
    NSMutableString* actors = [NSMutableString string];
    for(NSDictionary* item in cast){
        NSString* str = [item valueForKey:@"name"];
        if ([item valueForKey:@"character"]!=nil && ![[item valueForKey:@"charactrer"] isKindOfClass:[NSNull class]] ){
            actors =(actors.length==0)?[NSMutableString stringWithFormat:@"%@",str]:[NSMutableString stringWithFormat:@"%@,%@",actors,str];
        }
    }
    self.combinedActors = actors;
   
}
@end
