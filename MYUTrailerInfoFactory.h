//
//  MYUTrailerInfoFactory.h
//  MovYU
//
//  Created by Serge Mbamba on 2017/01/23.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//
#import "MYUTrailerInfo.h"
#import <Foundation/Foundation.h>

@interface MYUTrailerInfoFactory : NSObject

+ (NSArray<MYUTrailerInfo *> *) createFromJson:(NSDictionary *)jsonResponse;

@end
