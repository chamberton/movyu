//
//  DBConnector.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/16.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUCoreDataConnector.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MYUAppDelegate.h"
#import "Images+CoreDataProperties.h"
#import "Genres+CoreDataProperties.h"
#import "Searches+CoreDataClass.h"
#import "MYUUtilities.h"


@implementation MYUCoreDataConnector

+(BOOL) updateOrInsertGenre:(NSString*) name
                     withID:(int) genreID{
    
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    Genres *genreToAdd = (Genres *)[NSEntityDescription insertNewObjectForEntityForName:@"Genres"inManagedObjectContext:moc];
    genreToAdd.genre = name;
    genreToAdd.genreID = genreID;
    NSError *error;
    if (![moc save:&error]) {
        NSLog(@"Error saving new color: %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
    
}

+(NSMutableArray*) searchResultsForID : (int) searchID{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Searches"
                                              inManagedObjectContext:moc];
    NSError *error;
    [fetchRequest setEntity:entity];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"searchID == %d",searchID];
    [fetchRequest setPredicate:predicate];
    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    if(error || fetchedObjects.count<=0){
        return nil;
    }
    for (Searches* _in in fetchedObjects){
        NSLog(@"%d",_in.searchID);
    }
    NSSet<Movies*>* movies = ((Searches*)fetchedObjects[0]).foundMovies;
    NSSet<TVShows*>* tvshows = ((Searches*)fetchedObjects[0]).foundTVShows;
    NSMutableArray* savedMovies = [NSMutableArray arrayWithCapacity:movies.count];
    NSMutableSet* titles = [NSMutableSet set];
    
    for (Movies* video in movies){
        
        MYUMovieItem* movieItem = [[MYUMovieItem alloc] init];
        MYUThumbnail* thumbnail = [[MYUThumbnail alloc] init];
        thumbnail.title = video.title;
        if ([titles containsObject:video.title])
            continue;
        thumbnail.poster = [UIImage imageWithData:video.posterCompressed];
        
        movieItem.imageURL = video.imageURL;
        movieItem.synopsis = video.synopsis;
        movieItem.releaseDate = video.releaseDate;
        movieItem.popularity = video.popularity;
        movieItem.originalLanguage = video.originalLanguage;
        movieItem.movieID = video.movieID;
        movieItem.isAdultOnly = video.isAdultOnly;
        movieItem.genreIDS  =  [NSMutableSet setWithArray: [video.genreIDs   componentsSeparatedByString:@","]];;
        movieItem.directors =  [NSMutableSet setWithArray: [video.directors   componentsSeparatedByString:@","]];;
        movieItem.actors    =  [NSMutableSet setWithArray: [video.actors   componentsSeparatedByString:@","]];;
        movieItem.videoThumbnail = thumbnail;
        
        [savedMovies addObject:movieItem];
        [titles addObject:video.title];
    }
    for (TVShows* video in tvshows){
        
        MYUTVShowItem* tvShowItem = [[MYUTVShowItem alloc] init];
        MYUThumbnail* thumbnail = [[MYUThumbnail alloc] init];
        thumbnail.title = video.title;
        if ([titles containsObject:video.title])
            continue;
        thumbnail.poster = [UIImage imageWithData:video.posterCompressed];
        
        tvShowItem.imageURL = video.imageURL;
        tvShowItem.synopsis = video.synopsis;
        tvShowItem.airDate = video.releasDate;
        tvShowItem.popularity = video.popularity;
        tvShowItem.originalLanguage = video.originalLanguage;
        tvShowItem.tvShowID = video.tvShowID;
        tvShowItem.genreIDS  =  [NSMutableSet setWithArray: [video.genreIDs   componentsSeparatedByString:@","]];;
        tvShowItem.actors    =  [NSMutableSet setWithArray: [video.actors   componentsSeparatedByString:@","]];;
        tvShowItem.videoThumbnail = thumbnail;
        
        [savedMovies addObject:tvShowItem];
        [titles addObject:video.title];
    }
    return savedMovies;
    
}
+ (BOOL) insertSearchResult: (int) genreID
                withActors: (NSString*) actors
                withGenres: (NSString*) genres
                   isMovie: (BOOL) isMovie
            ratingLoweBoud: (float) ratingLowerBound
          ratingUpperBound: (float) ratingUpperBound
          searchID        : (int16_t) searchID
                     title: (NSString*)title
              foundVideos : (NSArray<MYUAbstractVideoItem *> *)foundVideos
{
    
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    Searches *searchAndResults = (Searches *)[NSEntityDescription insertNewObjectForEntityForName:@"Searches" inManagedObjectContext:moc];
    
    switch (genreID) {
        case CINEMAALL:
        case CINEMAMOSTPOPULAR:
        case TVHSHOWLATEST:
        case TVHSHOWTOPRATED:
        case TVSHOWAIRINGTODAY:
            searchAndResults.actors = @"";
            searchAndResults.genres = @"";
            
            break;
        default:
            searchAndResults.actors = actors;
            searchAndResults.genres = genres;
            searchAndResults.isMovie = isMovie;
            searchAndResults.ratingLowerBound = ratingLowerBound;
            searchAndResults.ratingUpperBound = ratingUpperBound;
            return false;
    }
    
    searchAndResults.searchID  = searchID;
    NSMutableSet* movies =[ NSMutableSet set];
    NSMutableSet* tvShows =[ NSMutableSet set];
    if (genreID==CINEMAALL || genreID==CINEMAMOSTPOPULAR){
        for ( MYUAbstractVideoItem *item in foundVideos){
            if ([item isKindOfClass:[MYUMovieItem class]]){
                Movies* movieItem= (Movies *)[NSEntityDescription insertNewObjectForEntityForName:@"Movies"
                                                                           inManagedObjectContext:moc];
                MYUMovieItem* video = (MYUMovieItem*) item;
                movieItem.title = video.videoThumbnail.title;
                movieItem.synopsis = video.synopsis;
                movieItem.releaseDate = video.releaseDate;
                movieItem.posterCompressed = UIImageJPEGRepresentation( video.videoThumbnail.poster, low) ;
                movieItem.popularity = video.popularity;
                movieItem.originalLanguage = video.originalLanguage;
                movieItem.movieID = video.movieID;
                movieItem.isAdultOnly = video.isAdultOnly;
                movieItem.imageURL = video.imageURL;
                NSMutableString* concatenated =[NSMutableString string];
                for(NSString* item in video.genreIDS){
                    concatenated =(concatenated.length==0)?[NSMutableString stringWithFormat:@"%@",item]:[NSMutableString stringWithFormat:@"%@,%@",concatenated,item];
                }
                
                movieItem.genreIDs = concatenated;
                concatenated=[NSMutableString string];;
                for(NSString* item in video.directors){
                    concatenated =(concatenated.length==0)?[NSMutableString stringWithFormat:@"%@",item]:[NSMutableString stringWithFormat:@"%@,%@",concatenated,item];
                }
                movieItem.directors = concatenated;
                concatenated=[NSMutableString string];;
                for(NSString* item in video.actors){
                    concatenated =(concatenated.length==0)?[NSMutableString stringWithFormat:@"%@",item]:[NSMutableString stringWithFormat:@"%@,%@",concatenated,item];
                }
                movieItem.actors = concatenated;
                [movies addObject:movieItem];
            }else if ([item isKindOfClass:[MYUTVShowItem class]]){
                Movies* tvShowItem= (Movies *)[NSEntityDescription insertNewObjectForEntityForName:@"TVShows"
                                                                            inManagedObjectContext:moc];
                
                [tvShows addObject:tvShowItem];
            }
            
        }
    }else{
        
        for ( MYUAbstractVideoItem *item in foundVideos){
             if ([item isKindOfClass:[MYUTVShowItem class]]){
                TVShows* tvShowItem= (TVShows *)[NSEntityDescription insertNewObjectForEntityForName:@"TVShows"
                                                                              inManagedObjectContext:moc];
                MYUTVShowItem* video = (MYUTVShowItem*) item;
                tvShowItem.title = video.videoThumbnail.title;
                tvShowItem.synopsis = video.synopsis;
                tvShowItem.releasDate = video.airDate;
                tvShowItem.posterCompressed = UIImageJPEGRepresentation( video.videoThumbnail.poster, low) ;
                tvShowItem.popularity = video.popularity;
                tvShowItem.originalLanguage = video.originalLanguage;
                tvShowItem.tvShowID = video.tvShowID;
                tvShowItem.imageURL = video.imageURL;
                NSMutableString* concatenated =[NSMutableString string];
                for(NSNumber* item in video.genreIDS){
                    concatenated =(concatenated.length==0)?[NSMutableString stringWithFormat:@"%@",item]:[NSMutableString stringWithFormat:@"%@,%@",concatenated,item];
                }
                
                tvShowItem.genreIDs = concatenated;
                
                concatenated=[NSMutableString string];;
                for(NSString* item in video.actors){
                    concatenated =(concatenated.length==0)?[NSMutableString stringWithFormat:@"%@",item]:[NSMutableString stringWithFormat:@"%@,%@",concatenated,item];
                }
                tvShowItem.actors = concatenated;
                [tvShows  addObject:tvShowItem];
                
            }else if ([item isKindOfClass:[MYUMovieItem class]]){
                Movies* tvShowItem= (Movies *)[NSEntityDescription insertNewObjectForEntityForName:@"Movies"
                                                                            inManagedObjectContext:moc];
                
                [movies addObject:tvShowItem];
            }
            
        }

    }
    [searchAndResults addFoundMovies:movies];
    [searchAndResults addFoundTVShows:tvShows];
    
    NSMergePolicy *mergePolicy = [[NSMergePolicy alloc] initWithMergeType:NSMergeByPropertyStoreTrumpMergePolicyType];
    [moc setMergePolicy:mergePolicy];
    NSError *error;
    if (![moc save:&error]) {
        // Something's gone seriously wrong
        NSLog(@"Error saving new color: %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
    
}

#define _10KB 10000
+ (void)updateOrInsertImage:(UIImage*) image
                        withID:(NSString*) movieTitle {

    dispatch_async(dispatch_get_main_queue(), ^{
        NSData* data  = UIImageJPEGRepresentation(image,0.25);
        AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *moc = ad.managedObjectContext;
        moc.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
        Images *imageToAdd = (Images *)[NSEntityDescription insertNewObjectForEntityForName:@"Images"inManagedObjectContext:moc];
        imageToAdd.movieTitle = movieTitle;
        imageToAdd.rawData = data;
        imageToAdd.compressedRawData =  UIImageJPEGRepresentation(image,0.10);
        NSError *error;
        [moc save:&error];
    });
}


+ (NSArray *)getImages {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Images"
                                              inManagedObjectContext:moc];
    NSError *error;
    [fetchRequest setEntity:entity];
    [fetchRequest setPropertiesToFetch:
     [NSArray arrayWithObjects:@"movieTitle", @"compressedRawData", nil]];
    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    if(error){
        return nil;
    }
    return fetchedObjects;
    
}



+ (NSArray *)getGenres {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *moc = ad.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Genres"
                                              inManagedObjectContext:moc];
    NSError *error;
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [moc executeFetchRequest:fetchRequest error:&error];
    if(error){
        return nil;
    }
    return fetchedObjects;
    
}
@end
