//
//  MovYUTests.m
//  MovYUTests
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OCMock.h"
#import "MYUMovieItem.h"
#import "MYUVideoFactory.h"
#import "SBJson4.h"
#import "MYUStringFactory.h"
#import "MYUCoreDataConnector.h"
#import "MYUTVShowSeasonDetails.h"
#import "MYUTVShowDetailsCellVC.h"
#import "MYUMovieDetailsVC.h"
#import "MYUTVShowDetailsVC.h"
#import <Foundation/Foundation.h>
@interface MovYUTests : XCTestCase

@end

@implementation MovYUTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


#pragma mark - Utilities test

-(void) testStringFromDate_PrimaryFormat{
    // Change seconds
    NSString* dateString = @"1970-01-01 00:00:33 +000";
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:33];
    NSDate* dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:PRIMARY_DATE_FORMAT];
    
    XCTAssert([date isEqualToDate:dateAcquired], @"Pass");
    
    dateString = @"1970-01-01 00:00:00 +000";
    date = [NSDate dateWithTimeIntervalSince1970:0];
    dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:PRIMARY_DATE_FORMAT];
    
    XCTAssert([date isEqualToDate:dateAcquired], @"Pass");
    
    // Change month
    dateString = @"1970-02-01 00:00:00 +000";
    date = [NSDate dateWithTimeIntervalSince1970:31*24*3600];
    dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:PRIMARY_DATE_FORMAT];
    
    XCTAssert([date isEqualToDate:dateAcquired], @"Pass");
    
    // Change year
    dateString = @"1971-01-01 00:00:00 +000";
    date = [NSDate dateWithTimeIntervalSince1970:24*3600*365];
    dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:PRIMARY_DATE_FORMAT];
    
    
    XCTAssert([date isEqualToDate:dateAcquired], @"Pass");
    
    // nil date
    dateString = @"1971-01-01 00:00:00 +000";
    date = [NSDate dateWithTimeIntervalSince1970:24*3600*365];
    dateAcquired  = [MYUUtilities dateFromString:nil withFormat:PRIMARY_DATE_FORMAT];
    BOOL isNULL = dateAcquired==nil;
    
    XCTAssert(isNULL, @"Pass");
    
    // nil format
    dateString = @"1971-01-01 00:00:00 +000";
    date = [NSDate dateWithTimeIntervalSince1970:24*3600*365];
    dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:nil];
    isNULL = dateAcquired==nil;
    
    XCTAssert(isNULL, @"Pass");
}
-(void) testStringFromDate_DayFormat{
    
    
    // Change seconds
    NSString* dateString = @"1970-01-01";
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:0];
    NSDate* dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:DAY_DATE_FORMAT];
    XCTAssert([date isEqualToDate:dateAcquired], @"Pass");
    
    
    
    // Change month
    dateString = @"1970-02-01";
    date = [NSDate dateWithTimeIntervalSince1970:31*24*3600];
    dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:DAY_DATE_FORMAT];
    
    XCTAssert([date isEqualToDate:dateAcquired], @"Pass");
    
    // Change year
    dateString = @"1971-01-01";
    date = [NSDate dateWithTimeIntervalSince1970:24*3600*365];
    dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:DAY_DATE_FORMAT];
    
    
    XCTAssert([date isEqualToDate:dateAcquired], @"Pass");
    
    // nil date
    dateString = @"1971-01-01";
    date = [NSDate dateWithTimeIntervalSince1970:24*3600*365];
    dateAcquired  = [MYUUtilities dateFromString:nil withFormat:DAY_DATE_FORMAT];
    BOOL isNULL = dateAcquired==nil;
    
    XCTAssert(isNULL, @"Pass");
    
    // nil format
    dateString = @"1971-01-01";
    date = [NSDate dateWithTimeIntervalSince1970:24*3600*365];
    dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:nil];
    isNULL = dateAcquired==nil;
    
    XCTAssert(isNULL, @"Pass");
    
}

#pragma mark - Categorie test

-(void) testDateDay{
    
    NSString* dateString = @"1970-02-01 00:00:00 +000";
    NSDate*  dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:PRIMARY_DATE_FORMAT];
    XCTAssert([dateAcquired day]==1, @"Pass");
}
-(void) testDateMonth{
    
    NSString* dateString = @"1970-02-01 00:00:00 +000";
    NSDate*  dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:PRIMARY_DATE_FORMAT];
    XCTAssert([dateAcquired month]==2, @"Pass");
}
-(void) testDateYear{
    
    NSString* dateString = @"1970-02-01 00:00:00 +000";
    NSDate*  dateAcquired  = [MYUUtilities dateFromString:dateString withFormat:PRIMARY_DATE_FORMAT];
    XCTAssert([dateAcquired year]==1970, @"Pass");
}


#pragma mark - Item size
-(void) testsizeForVideoItem{
    
    NSDictionary* dictionaryOfDimenson =[MYUUtilities  sizeForVideoItem:UIDeviceOrientationLandscapeLeft];
    
    XCTAssert([[dictionaryOfDimenson valueForKey:@"numberOfColumn"] integerValue]==3, @"Pass");
    
    XCTAssert([[dictionaryOfDimenson valueForKey:@"titleHeigh"] integerValue]==20, @"Pass");
    
    XCTAssert([[dictionaryOfDimenson valueForKey:@"verticalSpacing"] integerValue]==12, @"Pass");
    
    CGSize size  = [(NSValue*)[dictionaryOfDimenson valueForKey:@"itemsize"] CGSizeValue];
    
    if([MYUUtilities deviceType]<kiPhone6P){
        
        XCTAssert(size.height==150, @"Pass");
        XCTAssert(floor(size.width)==159, @"Pass");
    }
    
    else{
        XCTAssert(size.height==200, @"Pass");
        XCTAssert(floor(size.width)==159, @"Pass");
    }
    
}


-(void) testFormatStringWithDecimalOutPut{
    float decimal = 3.14159;
    
    NSString* string =  [MYUUtilities formatStringWithDecimalOutPut:decimal numberOfDecimalPoint:3];
    XCTAssert([string isEqualToString:@"3.142"], @"Pass");
    
    string =  [MYUUtilities formatStringWithDecimalOutPut:decimal numberOfDecimalPoint:1];
    XCTAssert([string isEqualToString:@"3.1"], @"Pass");
    
    
    string =  [MYUUtilities formatStringWithDecimalOutPut:decimal numberOfDecimalPoint:5];
    XCTAssert([string isEqualToString:@"3.14159"], @"Pass");
    
}


#pragma mark - Factory tests

-(void) testCreateFromMovieDictionay{
    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"movieItem" ofType:@"json"];
    NSString *formatString = [NSString stringWithContentsOfFile:jsonFile encoding:NSUTF8StringEncoding error:nil];
    
    
    NSError *jsonError;
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[formatString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonError];
    
    MYUMovieItem* item =[MYUVideoFactory createFromMovieDictionay:jsonDict];
    NSString* title = item.videoThumbnail.title;
    NSString* synopsis = item.synopsis;
    BOOL isAdult=item.isAdultOnly ;
    NSString* language =  item.originalLanguage;
    NSMutableSet* _set = item.genreIDS;
    float pop = item.popularity;
    float avr = item.voteAverage;
    NSDate* date = item.releaseDate;
    
    XCTAssert([title isEqualToString:@"The Magnificent Seven"], @"Pass");
    
    XCTAssert([_set containsObject:@"Adventure"], @"Pass");
    XCTAssert([_set containsObject:@"Western"], @"Pass");
    XCTAssert([_set containsObject:@"Action"], @"Pass");
    XCTAssert(pop==32, @"Pass");
    XCTAssert(avr==4, @"Pass");
    XCTAssert([language isEqualToString:@"English"], @"Pass");
    XCTAssert(!isAdult, @"Pass");
    XCTAssert([date isEqualToDate:[MYUUtilities dateFromString:@"2016-09-14" withFormat:DAY_DATE_FORMAT]], @"Pass");
    
    XCTAssert([synopsis isEqualToString:@"A big screen remake of John Sturges' classic western The Magnificent Seven, itself a remake of Akira Kurosawa's Seven Samurai. Seven gun men in the old west gradually come together to help a poor village against savage thieves."], @"Pass");

    
}
-(void) testCreateFromTVShowDictionay{
    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"TVShowItem" ofType:@"json"];
    NSString *formatString = [NSString stringWithContentsOfFile:jsonFile encoding:NSUTF8StringEncoding error:nil];
    
    
    NSError *jsonError;
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[formatString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonError];
    
    MYUTVShowItem* item =[MYUVideoFactory createFromTVShowDictionay:jsonDict];
    NSString* title = item.videoThumbnail.title;
    NSString* synopsis = item.synopsis;
    NSString* language =  item.originalLanguage;
    NSMutableSet* _set = item.genreIDS;
    float pop = item.popularity;
    float avr = item.voteAverage;
    NSDate* releaseDate = item.airDate;
    
    XCTAssert([title isEqualToString:@"Westworld"], @"Pass");
    XCTAssert([_set containsObject:@"Science Fiction"], @"Pass");
    XCTAssert([_set containsObject:@"Western"], @"Pass");
  
    XCTAssert(pop==40, @"Pass");
    XCTAssert(avr==7, @"Pass");
    XCTAssert([language isEqualToString:@"English"], @"Pass");
    XCTAssert([releaseDate isEqualToDate:[MYUUtilities dateFromString:@"2016-10-02" withFormat:DAY_DATE_FORMAT]], @"Pass");
    XCTAssert([synopsis isEqualToString:@"A dark odyssey about the dawn of artificial consciousness and the future of sin. The story revolves around a futuristic theme park staffed by robots that help guests live out their fantasies. The park breaks down, however, and two guests taking a Wild West adventure find themselves stalked by a gun-slinging android."], @"Pass");
    XCTAssert(YES, @"Pass");
}

-(void) testCreateConcatenatedStringFromArrayEntries{
    
    NSArray* array =@[@"first",@"second",@"third"];
    NSString* str =[MYUStringFactory createConcatenatedStringFromArrayEntries:array];
    XCTAssert([str isEqualToString:@"first,second,third"], @"Pass");
    array =@[@"first"];
    str =[MYUStringFactory createConcatenatedStringFromArrayEntries:array];
    XCTAssert([str isEqualToString:@"first"], @"Pass");
    array =@[];
    str =[MYUStringFactory createConcatenatedStringFromArrayEntries:array];
     XCTAssert(str.length==0, @"Pass");
    array =nil;
    str =[MYUStringFactory createConcatenatedStringFromArrayEntries:nil];
    XCTAssert(str.length==0, @"Pass");
}

-(void) testCreateConcatenatedStringFromDictionaryEntries {
    NSArray* objects =@[@"first",@"hello"];
    NSArray* keys =@[@"count",@"notcount"];
    NSDictionary* dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    objects =@[@"second",@"hello"];
    keys =@[@"count",@"notcount"];
    NSDictionary* dictionary2 = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    NSArray* Array =[NSArray arrayWithObjects:dictionary,dictionary2,nil];
    NSString* str =[MYUStringFactory  createConcatenatedStringFromDictionaryEntries : Array
                                                                        withUniqueKey:@"count"];
    XCTAssert([str isEqualToString:@"first,second"], @"Pass");
}

-(void) testFullLanguageNameFromLanguageCode{
    
    NSArray* objects =@[@"US",@"ZA"];
    NSString* obj =[MYUStringFactory concatenatedFullCountryNamesCodeArray:objects
                                                               countLimit:1];
    
     XCTAssert([obj isEqualToString:@"United States"], @"Pass");
    
    obj =[MYUStringFactory concatenatedFullCountryNamesCodeArray:objects
                                                      countLimit:2];
    
    XCTAssert([obj isEqualToString:@"United States,South Africa"], @"Pass");
    obj =[MYUStringFactory concatenatedFullCountryNamesCodeArray:objects
                                                      countLimit:3];
    
    XCTAssert([obj isEqualToString:@"United States,South Africa"], @"Pass");
    obj =[MYUStringFactory concatenatedFullCountryNamesCodeArray:objects
                                                      countLimit:0];
    
    XCTAssert(obj.length==0, @"Pass");
}



-(void) testConcatenatedFullCountryNames{
    
    NSString* fullaLanguage=[ MYUStringFactory fullLanguageNameFromLanguageCode:@"en"];
    XCTAssert([fullaLanguage isEqualToString:@"English"], @"Pass");

     fullaLanguage=[ MYUStringFactory fullLanguageNameFromLanguageCode:@"fr"];
    XCTAssert([fullaLanguage isEqualToString:@"French"], @"Pass");

    fullaLanguage=[ MYUStringFactory fullLanguageNameFromLanguageCode:@"en-ZA"];
    XCTAssert([fullaLanguage isEqualToString:@"English (South Africa)"], @"Pass");

}
- (void)testDisplaysRetriveTVShowSeasonDetailsItem
{
    
    
     id tableViewMock = OCMClassMock([UITableView class]);
    MYUTVShowSeasonDetails *tvShowsDetailsVC =[[MYUTVShowSeasonDetails alloc] init];
    MYUTVShowDetailsCellVC *cell =[[MYUTVShowDetailsCellVC alloc] init];
    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"TVShowItem" ofType:@"json"];
    NSString *formatString = [NSString stringWithContentsOfFile:jsonFile encoding:NSUTF8StringEncoding error:nil];
    
    
    NSError *jsonError;
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[formatString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonError];
    
    MYUTVShowItem* item =[MYUVideoFactory createFromTVShowDictionay:jsonDict];
    tvShowsDetailsVC.tvShowItem = item;
    OCMStub([tableViewMock dequeueReusableCellWithIdentifier:[OCMArg any]]).andReturn(cell);
             tvShowsDetailsVC.tableView = tableViewMock;
    
    [         tvShowsDetailsVC viewDidLoad];
    
     OCMVerify([item seasons]);
        
}
#import "MYUTVShowDetailsVC.h"
- (void)testHidesShowSynopsisButtonsForLongText
{
    
    
    id synopsisTextView = OCMClassMock([UITextView class]);
    MYUTVShowDetailsVC *tvShowsDetailsVC =[[MYUTVShowDetailsVC alloc] init];
    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"TVShowItem" ofType:@"json"];
    NSString *formatString = [NSString stringWithContentsOfFile:jsonFile encoding:NSUTF8StringEncoding error:nil];
    
    
    NSError *jsonError;
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[formatString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonError];
    
    MYUTVShowItem* item =[MYUVideoFactory createFromTVShowDictionay:jsonDict];
    tvShowsDetailsVC.tvShowItem = item;
    tvShowsDetailsVC.synopsisTextView = synopsisTextView;
   
    

    
    CGRect fixedSize = CGRectMake(0,0,100, 325);
    CGSize a = CGSizeMake(100, 50);  // small text size
    OCMStub([synopsisTextView sizeThatFits:CGSizeMake(100, MAXFLOAT)]).andReturn(a);
    OCMStub([synopsisTextView sizeThatFits:CGSizeMake(325, MAXFLOAT)]).andReturn(a);
    
    OCMStub([synopsisTextView text]).andReturn(item.synopsis);
    OCMStub([synopsisTextView frame]).andReturn(fixedSize);
    tvShowsDetailsVC.synopsisTextView = synopsisTextView;
    [tvShowsDetailsVC viewDidLoad];
    [tvShowsDetailsVC viewDidLayoutSubviews];
    [         tvShowsDetailsVC viewDidAppear:NO];
    
    
    XCTAssert(tvShowsDetailsVC.hiddenSynopsisNavigationButtons==YES, @"Pass");
}
- (void)testShowSynopsisButtonsForLongText
{
    
    
    id synopsisTextView = OCMClassMock([UITextView class]);
    MYUTVShowDetailsVC *tvShowsDetailsVC =[[MYUTVShowDetailsVC alloc] init];
    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"TVShowItem" ofType:@"json"];
    NSString *formatString = [NSString stringWithContentsOfFile:jsonFile encoding:NSUTF8StringEncoding error:nil];
    
    NSError *jsonError;
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[formatString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonError];
    
    MYUTVShowItem* item =[MYUVideoFactory createFromTVShowDictionay:jsonDict];
    tvShowsDetailsVC.tvShowItem = item;
    tvShowsDetailsVC.synopsisTextView = synopsisTextView;
   
    CGRect fixedSize = CGRectMake(0,0,100, 325);
    CGSize a = CGSizeMake(300, 900); // big text size
    OCMStub([synopsisTextView sizeThatFits:CGSizeMake(100, MAXFLOAT)]).andReturn(a);
    OCMStub([synopsisTextView sizeThatFits:CGSizeMake(325, MAXFLOAT)]).andReturn(a);
    
    OCMStub([synopsisTextView text]).andReturn(item.synopsis);
    OCMStub([synopsisTextView frame]).andReturn(fixedSize);

    tvShowsDetailsVC.synopsisTextView = synopsisTextView;
    
    [tvShowsDetailsVC viewDidAppear:NO];
    

    XCTAssert(tvShowsDetailsVC.hiddenSynopsisNavigationButtons==NO, @"Pass");
}


- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}


@end
