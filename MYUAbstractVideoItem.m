//
//  MYUAbstractVideoItem.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUAbstractVideoItem.h"


@implementation MYUAbstractVideoItem


#pragma Virtual Methods (these methods must to be overriden, by any drived class

- (instancetype) initWithThumbnail   : (MYUThumbnail*) customObject
                           andDetails: (NSDictionary*) details{
    
    if ([self isMemberOfClass:[MYUAbstractVideoItem class]]) { // Initialization was called by abstract object
        [self doesNotRecognizeSelector:_cmd];
        return nil;
    } else {  // initialization was called by derived classes
        self = [super init];
        if (self) {
            self.videoThumbnail  =customObject;
            self.details  = details;
        }
    }
    return  self;
}


#pragma Accessor Methods

-(NSString*)    videoTitle{
    
    return self.title;
}
-(UIImage*)     videoThumnailImage{
    return _videoThumbnail.poster;
}


@end
