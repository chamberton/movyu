//
//  MYUVideoFactory.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/15.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUVideoFactory.h"
#import "MYUCache.h"
#import "MYUWebOperator.h"
#import "MYUCoreDataConnector.h"
#import "MYUStringFactory.h"
#import "MYUThumbailFactory.h"

NSMutableDictionary* genres;

@implementation MYUVideoFactory

+ (void)addGenre:(NSNumber*)genreID forKey:(NSString*)genreName{
    [genres setValue:genreName forKey:[NSString stringWithFormat:@"%@",genreID]];
}

+(void) load{
    genres = [NSMutableDictionary new];
    int  genresIDs[]={28,12,16,35,80,105,99,18,82,2916,10751,10750,14,10753,36,10595,27,10756,10402,22,9648,10754,1115,10749,878,10755,9805,10758,10757,10748,53,10752,37,10770,10765,10759};
    
    NSArray* genreNames=@[  NSLocalizedString(@"Action", @"Action"), NSLocalizedString(@"Adventure",@"Adventure"), NSLocalizedString(@"Animation",@"Adventure"),NSLocalizedString(@"Comedy",@"Adventure"),NSLocalizedString(@"Crime",@"Adventure"),NSLocalizedString(@"Disaster",@"Adventure"),NSLocalizedString(@"Documentary",@"Documentary"),NSLocalizedString(@"Drama",@"Action & Adventure"),NSLocalizedString(@"Eastern",@"Action & Adventure"),NSLocalizedString(@"Erotic",@"Action & Adventure"),NSLocalizedString(@"Family",@"Action & Adventure"),NSLocalizedString( @"Fan Film",@"Action & Adventure"),NSLocalizedString( @"Fantasy",@"Action & Adventure"),NSLocalizedString(@"Film Noir",@"Action & Adventure"),NSLocalizedString(@"History",@"Action & Adventure"),NSLocalizedString( @"Holiday",@"Action & Adventure"),NSLocalizedString( @"Horror", @"Action & Adventure"),NSLocalizedString(@"Indie",@"Action & Adventure"),NSLocalizedString(@"Music",@"Action & Adventure"),NSLocalizedString(@"Musical",@"Action & Adventure"),NSLocalizedString( @"Mystery",@"Action & Adventure"),NSLocalizedString(@"Neo-noir",@"Action & Adventure"),NSLocalizedString(@"Road Movie",@"Action & Adventure"),NSLocalizedString(@"Romance",@"Action & Adventure"),NSLocalizedString(@"Science Fiction",@"Action & Adventure"),NSLocalizedString(@"Short",@"Action & Adventure"), NSLocalizedString(@"Sport",@"Action & Adventure"),NSLocalizedString( @"Sporting Event",@"Action & Adventure"),NSLocalizedString( @"Sports Film",@"Action & Adventure"),NSLocalizedString(@"Suspense",@"Action & Adventure"), NSLocalizedString(@"Thriller",@"Action & Adventure"),NSLocalizedString(@"War",@"Action & Adventure"),NSLocalizedString(@"Western",@"Action & Adventure"),NSLocalizedString(@"TV Movie" ,@"Action & Adventure"),NSLocalizedString( @"Sci-Fi & Fantasy",@"Action & Adventure"),NSLocalizedString(@"Action & Adventure",@"Action & Adventure")];
    int size  = sizeof(genresIDs)/sizeof(int);
    NSMutableArray* genreIDsinString=[NSMutableArray array];
    for (NSInteger i = 0; i < size; i++){
        [genreIDsinString addObject:[NSString stringWithFormat:@"%d",genresIDs[i]]];
    }
    genres =[NSMutableDictionary dictionaryWithObjects:genreNames forKeys:genreIDsinString];
}

+ (NSURL *)urlFrom:(NSDictionary *)dictionary {
    NSString* photoPath = [dictionary objectForKey:POSTER_PATH];
    MYUWebOperator* operator = [MYUWebOperator sharedWebOperator];
    NSURL* appended =(photoPath==nil || [photoPath isKindOfClass:[NSNull class]])?nil: [NSURL URLWithString:[operator.urlForPhotoPath stringByAppendingString:photoPath]];
    return appended;
}

+(MYUMovieItem*) createFromMovieDictionay:(NSDictionary*) dictionary{
    
    if (dictionary==nil)
        return nil;
    MYUWebOperator* operator = [MYUWebOperator sharedWebOperator];
    NSString* title =  [dictionary objectForKey:@"title"];
    if(!title || [title isKindOfClass:[NSNull class]]){
        title = [dictionary objectForKey:ORIGINAL_TITLE];
    }
    NSString* overview  = [dictionary objectForKey:@"overview"];
    NSString* release_dateString  = [dictionary objectForKey:@"release_date"];
    NSString* original_language = [dictionary objectForKey:@"original_language"];
    
    NSArray* genreID= [dictionary objectForKey:@"genre_ids"];
    NSNumber* adultOnly =[dictionary objectForKey:@"adult"];
    NSNumber* ID =[dictionary objectForKey:@"id"];
    NSNumber*  voteAverage =[dictionary objectForKey:@"vote_average"];
    NSNumber*  popularity =[dictionary objectForKey:@"popularity"];
    
    
    MYUMovieItem *movieItem = [[MYUMovieItem alloc] init];
    movieItem.title = title;
    NSString* photoPath = [dictionary objectForKey:POSTER_PATH];
    if(photoPath==nil || [photoPath isKindOfClass:[NSNull class]])
        photoPath = [dictionary objectForKey:BACKDROP_PATH];
    if (photoPath==nil){
        [[MYUCache sharedCache] addToMissingImages:title];
    }
    NSURL* appended =(photoPath==nil || [photoPath isKindOfClass:[NSNull class]])?nil: [NSURL URLWithString:[operator.urlForPhotoPath stringByAppendingString:photoPath]];
    movieItem.imageURL = appended.absoluteString;
    MYUThumbnail *thumbnail = [MYUThumbailFactory  thumnailWithImageURL:appended andTitle:title];
    if (thumbnail){
        [movieItem setVideoThumbnail:thumbnail];
        
    }else
        return nil;
    movieItem.isAdultOnly=(adultOnly==nil)?NO:[adultOnly boolValue];
    if(original_language==nil)
        original_language=@"en";
    movieItem.originalLanguage= [MYUStringFactory  fullLanguageNameFromLanguageCode:original_language];
    movieItem.movieID =[ID intValue];
    movieItem.genreIDS =[NSMutableSet set];
    movieItem.rating = [voteAverage integerValue];
    NSMutableString* combinedGenres = [NSMutableString string];
    
    for (NSNumber* num in genreID){
        NSString* genre =[genres valueForKey:[NSString stringWithFormat:@"%@",num]];
        if (genre==nil || [genre isKindOfClass:[NSNull class]]){ // genre is missing in the static list
            
            int movieID = (int)movieItem.movieID ;
            
            NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                [[MYUWebOperator sharedWebOperator] detailsForMovieWithID:movieID];
            }];
            operation.queuePriority =  NSOperationQueuePriorityHigh ;
            NSOperationQueue *backgnd= [[NSOperationQueue alloc] init];
            [backgnd addOperation:operation];
            
        }else{
            [movieItem.genreIDS addObject:genre];
            if(combinedGenres.length>0){
                [combinedGenres appendString:@","];
            }
            [combinedGenres appendString:genre];
        }
    }
    movieItem.combinedGenres = combinedGenres;
    
    movieItem.synopsis=(overview==nil || [overview isEqualToString:NSLocalizedString(@"Add the plot",@"Plot")])?@"":overview;
    movieItem.popularity=(popularity==nil)?-1:[popularity integerValue];
    movieItem.voteAverage =(voteAverage==nil)?-1:[voteAverage integerValue];
    movieItem.releaseDate=(release_dateString==nil)?nil:[MYUUtilities dateFromString:release_dateString withFormat:DAY_DATE_FORMAT];
    movieItem.movieID=(ID==nil)?-1:[ID intValue];
    movieItem.isMovie = YES;
    return movieItem;
}


+(MYUTVShowItem*) createFromTVShowDictionay:(NSDictionary*) dictionary{
    
    if (dictionary==nil)
        return nil;
    MYUWebOperator* operator = [MYUWebOperator sharedWebOperator];
    NSString* title = [dictionary objectForKey:@"name"];
    
    if(!title){
        title = [dictionary objectForKey:ORIGINAL_NAME];
    }
    NSString* overview  = [dictionary objectForKey:@"overview"];
    NSString* firstAirDate  = [dictionary objectForKey:@"first_air_date"];
    NSString* original_language = [dictionary objectForKey:@"original_language"];
    
    NSArray*  genreID= [dictionary objectForKey:@"genre_ids"];
    NSNumber* ID =[dictionary objectForKey:@"id"];
    NSNumber* voteAverage =[dictionary objectForKey:@"vote_average"];
    NSNumber* popularity =[dictionary objectForKey:@"popularity"];
    NSNumber* adultOnly =[dictionary objectForKey:@"adult"];
    
    MYUTVShowItem *tvShowItem = [[MYUTVShowItem alloc] init];
    tvShowItem.title = title;
    tvShowItem.isAdultOnly = adultOnly;
    NSString* photoPath = [dictionary objectForKey:POSTER_PATH];
    if(photoPath==nil || [photoPath isKindOfClass:[NSNull class]])
        photoPath = [dictionary objectForKey:BACKDROP_PATH];
    if (photoPath==nil){
        [[MYUCache sharedCache] addToMissingImages:title];
    }
    NSURL* appended =(photoPath==nil || [photoPath isKindOfClass:[NSNull class]])?nil: [NSURL URLWithString:[operator.urlForPhotoPath stringByAppendingString:photoPath]];
    tvShowItem.imageURL = appended.absoluteString;
    MYUThumbnail *thumbnail = [MYUThumbailFactory  thumnailWithImageURL:appended andTitle:title];
    if (thumbnail){
        [tvShowItem setVideoThumbnail:thumbnail];
        
    }else
        return nil;
    if(original_language==nil)
        original_language=@"en";
    NSLocale *enLocale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale currentLocale] localeIdentifier]] ;
    tvShowItem.originalLanguage= [enLocale displayNameForKey:NSLocaleIdentifier value:original_language];
    tvShowItem.tvShowID = [ID integerValue];
    tvShowItem.genreIDS =[NSMutableSet set];
    tvShowItem.rating = [voteAverage integerValue];
    NSMutableString* combinedGenres = [NSMutableString string];
    for (NSNumber* num in genreID){
        NSString* genre =[genres valueForKey:[NSString stringWithFormat:@"%@",num]];
        if (genre==nil || [genre isKindOfClass:[NSNull class]]){ // genre is missing in the static list
            
            int movieID = (int) tvShowItem.tvShowID ;
            
            NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                [[MYUWebOperator sharedWebOperator] detailsForMovieWithID:movieID];
            }];
            operation.queuePriority =  NSOperationQueuePriorityHigh ;
            NSOperationQueue *backgnd= [[NSOperationQueue alloc] init];
            [backgnd addOperation:operation];
            
        }else{
            [tvShowItem.genreIDS addObject:genre];
            [combinedGenres appendString:genre];
        }
    }
    tvShowItem.synopsis=(overview==nil || [overview isEqualToString:NSLocalizedString(@"Add the plot",@"Plot")])?@"":overview;
    tvShowItem.popularity=(popularity==nil)?-1:[popularity integerValue];
    tvShowItem.voteAverage =(voteAverage==nil)?-1:[voteAverage integerValue];
    tvShowItem.airDate=(firstAirDate==nil)?nil:[MYUUtilities dateFromString:firstAirDate withFormat:DAY_DATE_FORMAT];
    tvShowItem.tvShowID=(ID==nil)?-1:[ID intValue];
    tvShowItem.combinedGenres = combinedGenres;
    tvShowItem.isMovie = NO;
    tvShowItem.releaseDate = tvShowItem.airDate;
    return tvShowItem;
    
}
+(MYUMovieItem*) addCastDetails:(NSDictionary*) castDisctionary to:(MYUMovieItem*) originalItem{
    
    MYUMovieItem* copy = originalItem;
    copy.actors = [NSMutableSet set];
    copy.directors = [NSMutableSet set];
    
    if (castDisctionary!=nil){
        
        NSArray* cast =[castDisctionary objectForKey:@"cast"];
        NSArray* crew =[castDisctionary objectForKey:@"crew"];
        
        NSMutableString* actors = [NSMutableString string];
        
        [NSMutableString string];
        NSMutableString* directors = [NSMutableString string];
        for(NSDictionary* item in cast){
            NSString* str = [item valueForKey:@"name"];
            if ([item valueForKey:@"character"]!=nil && ![[item valueForKey:@"charactrer"] isKindOfClass:[NSNull class]] ){
                actors =(actors.length==0)?[NSMutableString stringWithFormat:@"%@",str]:[NSMutableString stringWithFormat:@"%@,%@",actors,str];
                [copy.actors addObject:str];
            }
        }
        for(NSDictionary* item in crew){
            NSString* str = [item valueForKey:@"name"];
            
            NSString* department= [item valueForKey:@"department"];
            if (department!=nil && ![department isKindOfClass:[NSNull class]]  && [department isEqualToString:@"Directing"]){
                directors =(directors.length==0)?[NSMutableString stringWithFormat:@"%@",str]:[NSMutableString stringWithFormat:@"%@,%@",directors,str];
                [copy.directors addObject:str];
                
            }
        }
        copy.combinedActors = actors;
        copy.combinedDirectors = directors;
        
        
    }
    return copy;
    
    
}

@end
