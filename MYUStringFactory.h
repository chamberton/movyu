//
//  MYUStringFactory.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYUStringFactory : NSObject
+(NSString*) createConcatenatedStringFromArrayEntries : ( NSArray*) Array;

+(NSString*) createConcatenatedStringFromDictionaryEntries : ( NSArray*) Array
                                              withUniqueKey:(NSString*) key;

+(NSString*) fullLanguageNameFromLanguageCode:(NSString*) codeLanguage;


+(NSString*) concatenatedFullLanguageNamesCodeArray:(NSArray*) codeLanguageArray;
+(NSString*) concatenatedFullCountryNamesCodeArray:(NSArray*) codeCountrArray
                                        countLimit: (int) limit;
@end
