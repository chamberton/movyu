//
//  Searches+CoreDataClass.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Movies, TVShows;

NS_ASSUME_NONNULL_BEGIN

@interface Searches : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Searches+CoreDataProperties.h"
