//
//  MYUThumbnail.h
//  MovYU
//  Class that reprensent a thumbnail object, encapsulating the image and title of a video item (movie, tv show)
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

@interface MYUThumbnail : NSObject

@property (nonatomic, strong, readwrite) NSURL             * imageURL;
@property (nonatomic, strong, readwrite) NSString          * title;
@property (nonatomic, strong, readwrite) UIImage           * poster;


@end
