//
//  TVShows+CoreDataProperties.h
//  MovYU
//
//  Created by Serge Mbamba on 2017/04/26.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "TVShows+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TVShows (CoreDataProperties)

+ (NSFetchRequest<TVShows *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *actors;
@property (nullable, nonatomic, copy) NSString *creators;
@property (nullable, nonatomic, copy) NSString *genreIDs;
@property (nullable, nonatomic, copy) NSString *languages;
@property (nullable, nonatomic, copy) NSString *originalCountry;
@property (nullable, nonatomic, copy) NSString *originalLanguage;
@property (nonatomic) float popularity;
@property (nullable, nonatomic, retain) NSData *posterCompressed;
@property (nullable, nonatomic, copy) NSString *productionCompanies;
@property (nonatomic) float rating;
@property (nullable, nonatomic, copy) NSDate *releasDate;
@property (nullable, nonatomic, copy) NSString *synopsis;
@property (nullable, nonatomic, copy) NSString *title;
@property (nonatomic) int64_t tvShowID;
@property (nullable, nonatomic, retain) NSSet<Seasons *> *relationship;
@property (nullable, nonatomic, retain) Searches *searchCall;
@property (nullable, nonatomic, copy) NSString *imageURL;

@end

@interface TVShows (CoreDataGeneratedAccessors)

- (void)addRelationshipObject:(Seasons *)value;
- (void)removeRelationshipObject:(Seasons *)value;
- (void)addRelationship:(NSSet<Seasons *> *)values;
- (void)removeRelationship:(NSSet<Seasons *> *)values;

@end

NS_ASSUME_NONNULL_END
