//
//  MYUCache.h
//  MovYU
//  Caching class (keeps images in the RAW for increased user experienc)
//  Created by Serge Mbamba on 2016/10/13.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//



@interface MYUCache : NSObject

@property (strong,readwrite,nonatomic) NSMutableArray* lastMoviesInCinema;
@property (strong,readwrite,nonatomic) NSMutableArray* lastMostPopularInCinema;
@property (strong,readwrite,nonatomic) NSMutableArray* lastLatestTVShows;
@property (strong,readwrite,nonatomic) NSMutableArray* lastTopRatedTVShows;
@property (strong,readwrite,nonatomic) NSMutableArray* lastAiringTodayTVShows;

+(MYUCache*) sharedCache;
-(void) clearCache:(BOOL) forcefully;
-(BOOL) addImage:(UIImage*) image
         forName:(NSString*) name;

-(UIImage*) imageForMovieTitle:(NSString*) title;
-(void) addToMissingImages:(NSString*) title;
-(UIImage*) missingPicture;
-(void) acquireMovieImagesFromDB;
-(void) startClearingThread;
-(void) stopClearingThread;
-(void)cancelSaving;
-(void) saveAll;
@end
