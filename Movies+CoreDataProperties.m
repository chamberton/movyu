//
//  Movies+CoreDataProperties.m
//  MovYU
//
//  Created by Serge Mbamba on 2017/04/26.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "Movies+CoreDataProperties.h"

@implementation Movies (CoreDataProperties)

+ (NSFetchRequest<Movies *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Movies"];
}

@dynamic actors;
@dynamic directors;
@dynamic genreIDs;
@dynamic isAdultOnly;
@dynamic movieID;
@dynamic originalLanguage;
@dynamic popularity;
@dynamic posterCompressed;
@dynamic releaseDate;
@dynamic synopsis;
@dynamic title;
@dynamic voteAverage;
@dynamic searchCall;
@dynamic imageURL;

@end
