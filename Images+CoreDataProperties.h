//
//  Images+CoreDataProperties.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/31.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Images+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Images (CoreDataProperties)

+ (NSFetchRequest<Images *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSData *compressedRawData;
@property (nullable, nonatomic, copy) NSString *movieTitle;
@property (nullable, nonatomic, retain) NSData *rawData;

@end

NS_ASSUME_NONNULL_END
