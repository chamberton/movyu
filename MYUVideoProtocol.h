//
//  MYUVideoProtocol.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/18.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#ifndef MYUVideoProtocol_h
#define MYUVideoProtocol_h
#import "MYUThumbnail.h"

@protocol isVideoItem <NSObject>

- (instancetype) initWithThumbnail   : (MYUThumbnail*) customObject
                           andDetails: (NSDictionary*) details;
- (NSString*)    videoTitle;
- (UIImage*)     videoThumnailImage;
@end

#endif /* MYUVideoProtocol_h */
