//
//  MUYVideoCollectionViewLayout.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//



#import "MYUVideoCollectionViewLayout.h"



static UIDeviceOrientation oldOrientation;
@interface MYUVideoCollectionViewLayout ()

@property (nonatomic, strong) NSDictionary* sizeDictionary;
@property (nonatomic, strong) NSDictionary *layoutInfo;

- (CGRect)frameForVideoThumbnailAtIndexPath:(NSIndexPath *)indexPath;
- (CGRect)frameForVideoThumbnailTitleAtIndexPath:(NSIndexPath *)indexPath;
- (CATransform3D)transformForVideoThumbnailAtIndex:(NSUInteger)index;

@end


@implementation MYUVideoCollectionViewLayout

+ (void)load {
    UIDevice *myDevice = [UIDevice currentDevice] ;
    UIDeviceOrientation deviceOrientation = myDevice.orientation;
    oldOrientation = deviceOrientation;
}
@synthesize itemSize=_itemSize,sizeDictionary=_sizeDictionary;


#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (self) {
        _verticalScroller = YES;
        self.horizontalSpacing = 5;
        [self setup];
    }
    self.itemInsets = UIEdgeInsetsMake(20.0f, 20.0f, 15.0f, 20.0f);
    return self;
}

- (instancetype)initWithVerticalScrollDirection:(BOOL) scrollVertically {
    self = [super init];
    if (self) {
        _verticalScroller = scrollVertically;
        self.horizontalSpacing = 5;
        [self setup];
    }
    self.itemInsets = UIEdgeInsetsMake(20.0f, 20.0f, 15.0f, 20.0f);
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _verticalScroller = YES;
        self.horizontalSpacing = 5;
        [self setup];
    }
    self.itemInsets = UIEdgeInsetsMake(20.0f, 20.0f, 15.0f, 20.0f);
    return self;
}

#pragma mark - Configuration base on device orientation

- (void)setup {
    
    self.horizontalSpacing = 5;
    UIInterfaceOrientation deviceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    
    
    self.sizeDictionary  = [MYUUtilities sizeForVideoItemInterface:deviceOrientation];
    self.itemSize        = [(NSValue*)[self.sizeDictionary  objectForKey:ITEM_SIZE] CGSizeValue];
    
    if (!_verticalScroller){
        CGRect screen = [[UIScreen mainScreen] bounds];
      
        CGFloat height = CGRectGetHeight(screen);
        CGFloat width  = CGRectGetWidth(screen);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
              self.itemSize = CGSizeMake(width / 2.5,MIN(300,height/3 - 20));
        }else
        if (height>600){
            self.itemSize = CGSizeMake(width / 2.5,MIN(200,height/3 - 20));
        }else{
            self.itemSize = CGSizeMake(width / 2.5, MIN(150,height/4 - 20));
        }
    }
    
    self.verticalSpacing = [(NSNumber*)[self.sizeDictionary objectForKey:VERTICAL_SPACING] floatValue];
    self.numberOfColumns = [(NSNumber*)[self.sizeDictionary objectForKey:NUMER_OF_COLUMN]  integerValue];
    self.titleHeight     = [(NSNumber*)[self.sizeDictionary objectForKey:TITLE_HEIGHT]     floatValue];
}



- (void)rotate:(UIInterfaceOrientation)orientation {
    
    self.horizontalSpacing = 5;
    
    self.sizeDictionary  = [MYUUtilities sizeForVideoItemInterface:orientation];
    self.itemSize        = [(NSValue*)[self.sizeDictionary  objectForKey:ITEM_SIZE] CGSizeValue];
    
    if (!_verticalScroller){
        CGRect screen = [[UIScreen mainScreen] bounds];
        
        CGFloat height = CGRectGetHeight(screen);
        CGFloat width  = CGRectGetWidth(screen);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            self.itemSize = CGSizeMake(width / 2.5,MIN(300,height/3 - 20));
        }else
            if (height>600){
                self.itemSize = CGSizeMake(width / 2.5,MIN(200,height/3 - 20));
            }else{
                self.itemSize = CGSizeMake(width / 2.5, MIN(150,height/4 - 20));
            }
    }
    
    self.verticalSpacing = [(NSNumber*)[self.sizeDictionary objectForKey:VERTICAL_SPACING] floatValue];
    self.numberOfColumns = [(NSNumber*)[self.sizeDictionary objectForKey:NUMER_OF_COLUMN]  integerValue];
    self.titleHeight     = [(NSNumber*)[self.sizeDictionary objectForKey:TITLE_HEIGHT]     floatValue];
}






#pragma mark - Collection Layout
- (void)prepareLayout {
    [self setup];
    NSMutableDictionary *layoutInfo     = [NSMutableDictionary dictionary];
    NSInteger numberOfSection           = [self.collectionView numberOfSections];
    NSIndexPath *indexPath              ;
    
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *titleLayoutInfo = [NSMutableDictionary dictionary];
    
    for (NSInteger section = 0; section < numberOfSection; section++) { // apply rotation and border to all the cells
        
        indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
        UICollectionViewLayoutAttributes *itemAttributes =
        [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        itemAttributes.frame = [self frameForVideoThumbnailAtIndexPath:indexPath];
        itemAttributes.transform3D = [self transformForVideoThumbnailAtIndex:section];
        cellLayoutInfo[indexPath] = itemAttributes;
        
        if (indexPath.item == 0) {
            UICollectionViewLayoutAttributes *titleAttributes =
            [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:ThumbnailCellTitle withIndexPath:indexPath];
            titleAttributes.frame = [self frameForVideoThumbnailTitleAtIndexPath:indexPath];
            titleLayoutInfo[indexPath] = titleAttributes;
        }
        
    }
    
    layoutInfo[ThumbnailCell] = cellLayoutInfo;
    layoutInfo[ThumbnailCellTitle] = titleLayoutInfo;
    self.layoutInfo = layoutInfo;
}



- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *elementIdentifier, NSDictionary *elementsInfo, BOOL *stop) {
        [elementsInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath, UICollectionViewLayoutAttributes *attributes, BOOL *innerStop) {
            if (CGRectIntersectsRect(rect, attributes.frame)) {
                [allAttributes addObject:attributes];
            }
        }];
    }];
    
    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.layoutInfo[ThumbnailCell][indexPath];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind
                                                                     atIndexPath:(NSIndexPath *)indexPath {
    return self.layoutInfo[ThumbnailCellTitle][indexPath];
}


#pragma mark - Private

- (CGRect)frameForVideoThumbnailAtIndexPath:(NSIndexPath *)indexPath {
    // Create the frame for the thumbnail
    if(_verticalScroller){
        NSInteger row = indexPath.section / self.numberOfColumns;
        NSInteger column = indexPath.section % self.numberOfColumns;
        CGFloat horizontalSpacing = self.collectionView.bounds.size.width - self.itemInsets.left -self.itemInsets.right -(self.numberOfColumns * self.itemSize.width);
        horizontalSpacing= (self.numberOfColumns > 1)? horizontalSpacing / (self.numberOfColumns - 1):horizontalSpacing;
        CGFloat originX = floorf(self.itemInsets.left + (self.itemSize.width + horizontalSpacing) * column);
        CGFloat originY = floor(self.itemInsets.top +
                                (self.itemSize.height + self.titleHeight + self.verticalSpacing) * row);
        return CGRectMake(originX, originY, self.itemSize.width, self.itemSize.height);
    }
    else{
        CGFloat originX = floorf(self.itemInsets.left + (self.itemSize.width + self.horizontalSpacing) *  indexPath.section);
        
        CGFloat originY =  0 ;
        
        
        return CGRectMake(originX, originY, self.itemSize.width, self.itemSize.height);
    }
    
}

- (CGSize)collectionViewContentSize {
    // Re-adjust the collection view height, to fit the data
    NSUInteger sections = [ self.collectionView numberOfSections];
    if (self.verticalScroller){
        NSInteger numberOfRow = sections / self.numberOfColumns;
        numberOfRow=([self.collectionView numberOfSections] % self.numberOfColumns)?numberOfRow+1:numberOfRow;
        CGFloat height = self.itemInsets.top + numberOfRow * self.itemSize.height + (numberOfRow - 1) * self.verticalSpacing + numberOfRow * self.titleHeight +
        self.itemInsets.bottom;
        return CGSizeMake(self.collectionView.bounds.size.width, height);
    }
    else{
      
        NSInteger numberOfColumns = sections ;
        CGFloat width = self.itemInsets.right + (numberOfColumns) * self.itemSize.width + (numberOfColumns - 1) * self.horizontalSpacing  + self.itemInsets.right;
        width =(sections % 2)==0? width :width + (self.itemSize.width + self.horizontalSpacing)*0.5;
        return CGSizeMake(width , self.collectionView.bounds.size.height);
    }
    
}

- (CGRect)frameForVideoThumbnailTitleAtIndexPath:(NSIndexPath *)indexPath {
    // Create the frame for the thumbnail title (well in our case most like the subtitle)
    CGRect frame = [self frameForVideoThumbnailAtIndexPath:indexPath];
    frame.origin.y += frame.size.height;
    frame.size.height = self.titleHeight;
    return frame;
}


- (CATransform3D)transformForVideoThumbnailAtIndex:(NSUInteger)index{
    // Apply the rotation
    return [_transforms[index]  CATransform3DValue];
}



#pragma mark - Properties

- (void)setItemSize:(CGSize)itemSize
{
    if (CGSizeEqualToSize(_itemSize, itemSize))
        return;
    
    _itemSize = itemSize;
    
    [self invalidateLayout];
}

- (void)setVerticalSpacing:(CGFloat)verticalSpacing
{
    if (_verticalSpacing == verticalSpacing)
        return;
    
    _verticalSpacing = verticalSpacing;
    
    [self invalidateLayout];
}

- (void)setNumberOfColumns:(NSInteger)numberOfColumns
{
    if (_numberOfColumns == numberOfColumns) return;
    
    _numberOfColumns = numberOfColumns;
    
    [self invalidateLayout];
}

- (void)setTitleHeight:(CGFloat)titleHeight
{
    if (_titleHeight == titleHeight) return;
    
    _titleHeight = titleHeight;
    
    [self invalidateLayout];
}


@end


