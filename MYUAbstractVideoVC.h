//
//  MYUAbstractVC.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/13.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//
#import "MYUVideoCollectionViewLayout.h"
#import "MYUVideoTitleReusable.h"

@interface MYUAbstractVideoVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate>
@property (nonatomic, strong,readwrite)   NSMutableArray *videos;
@property (nonatomic, strong,readwrite)   NSArray *filteredVideos;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic,strong)  NSString* searchText;
@property (readwrite )int lastSearchTextLength;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (nonatomic, strong) NSMutableSet *videosNames;
@property (nonatomic, strong) NSSet *filteredVideoNames;
@property (nonatomic, retain,readwrite) NSTimer* updateTimer;
@property (nonatomic,strong)           NSThread* updateThread;
@property (nonatomic, strong) NSOperationQueue *thumbnailQueue;
@property (retain, nonatomic) IBOutlet MYUVideoCollectionViewLayout *videosLayout;
@property (retain, nonatomic) NSMutableArray *transforms;
@property (nonatomic,strong)  MBProgressHUD * hud;
@property (nonatomic,retain) dispatch_queue_t savingQueue;
@property (nonatomic,retain) NSTimer* hidingTimer;
@property (weak, nonatomic)  IBOutlet UICollectionView  *collectionView;
@property  BOOL showing;
@property  BOOL firstToShow;
@property  BOOL hasAppearedAlready;
@property  BOOL isVisible;
@property  BOOL containsMovie;
/*
 * Reset data sources
 */
-(void) reset;

/*
 * Register for web operator notifications
 */
-(void) registerForNotifications;

/*
 * Start automatic upates of list of vidoes that should be displayed
 */
-(void) stopAutomaticUpdates;

/*
 * Stop automatic upates of list of vidoes that should be displayed
 */
-(void) startAutomaticUpdates;
/*
 * Displays alert notifcation
 */
-(void)displayOutdatedInfoWarning;
-(void) setFromDB:(NSString*) noteName;
-(void) requestUpate:(BOOL) forceUpdate;
- (void)orientationChanged:(NSNotification *)note;

@end
