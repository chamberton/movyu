//
//  Utilities.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MYUDefinitions.h"

@interface MYUUtilities : NSObject

/*
 * returns the number of video item per rows given the orientation
 */
+(NSUInteger)   videoItemPerRowOnOrientation : (UIInterfaceOrientation) orientation;
+(NSUInteger) videoItemPerRowOnDeviceOrientation : (UIDeviceOrientation) orientation;
+(NSDictionary*) sizeForVideoItem:(UIDeviceOrientation) orientation;
+ (NSDictionary *)sizeForVideoItemInterface:(UIInterfaceOrientation)orientation;

/*
 *  returns date from a string with a given format,
 *  returns nil if format or dateString is null
 */
+(NSDate*) dateFromString:(NSString*) dateString
               withFormat:(NSString*) format;


/*
 *  returns formatted string  from date in a specified format
 *  returns nil if format or date is null
 */
+(NSString*) stringFromDate:(NSDate*) date
                 withFormat:(NSString*) format;


/*
 *  returns the string format to display a floating point with a specified number of decimal point
 */
+(NSString*) formatStringWithDecimalOutPut:(float) floatingNumber
                      numberOfDecimalPoint:(int) decimalPoint;


/*
 * Writes a given text with a given font at a specified location of a picture
 */
+(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
           withFont :(UIFont*)   font;
/*
 * Draws a monochromatic image with a given color
 */

+(UIImage*)imageFromColor:(UIColor*) color
                 withSize:(CGRect) rect;

/*
 * returns the current visual video item size
 */
+(CGRect)videoItemSize;

/*
 * returns the default size of the visual video item
 */
+(CGRect)defaultImageSize;

/*
 * check and set for semaphone lock
 */
+(BOOL) checkAndSet:(BOOL*) running;

+(DeviceType) deviceType;

+(NSData*) compress : (UIImage*) image
          desiredSize: (float) desiredSize
               isJPEG: (BOOL) isJPEG;
@end
