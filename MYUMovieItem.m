//
//  MYUMovieItem.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUMovieItem.h"

@implementation MYUMovieItem
- (NSComparisonResult)compare:(MYUMovieItem *)otherObject {
    return self.popularity<otherObject.popularity;
}
@end
