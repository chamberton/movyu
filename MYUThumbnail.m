//
//  MYUThumbnail.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "MYUThumbnail.h"
#import "MYUUtilities.h"
#import "MYUCache.h"
#import "MYUColorAndFontManager.h"
#import "MYUWebOperator.h"
#import "MYUCoreDataConnector.h"

@interface MYUThumbnail(){
    NSDictionary  *details;
}
@end

@implementation MYUThumbnail
@synthesize poster=_poster;

-(instancetype) init{
    self =[super init];
    if (self){
        _imageURL=nil;
        _poster= nil;
        _title =  @"";
    }
    return self;
}


-(void) dealloc{
    _poster = nil;
    _imageURL=nil;
    
}

@end
