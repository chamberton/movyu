
/*
 File: APLTableViewController.m
 Abstract: Table view controller to manage display of values from various criterions.
 The controller supports opening and closing of sections. To do this it maintains information about each section using an array of SectionInfo objects.
 
 Version: 3.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Apple Inc. All Rights Reserved.
 
 */
#import <QuartzCore/QuartzCore.h>
#import "APLTableViewController.h"
#import "MYUQuoteCell.h"
#import "APLSectionInfo.h"
#import "APLSectionHeaderView.h"
#import "iToast.h"
#import "MYUCriteria.h"
#import "MYUCriterion.h"
#import "MYUWebOperator.h"
#import "MYUAbstractVideoItem.h"
#import "MYUMovieItem.h"
#import "MYUTVShowItem.h"
#import "MYUVideoFactory.h"
#import "MYUMovieItem+Cast.h"
#import "MYUMovieDetailsVC.h"
#import "MYUInCinemasVC.h"
#import "MYUTVShowsVC.h"
#pragma mark - APLTableViewController

static NSString *SectionHeaderViewIdentifier = @"SectionHeaderViewIdentifier";
static dispatch_queue_t loadingCastQueue;


@interface APLTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property BOOL  latestOnlyWasSelectedAtInstantOfSearchLaunch;
@property (weak, nonatomic) IBOutlet UITextField *actorsTextField;
@property (weak, nonatomic) IBOutlet UIImageView *movieImageView;
@property (weak, nonatomic) IBOutlet UIImageView *tvShowImageView;
@property (retain, nonatomic) IBOutlet MYUVideoCollectionViewLayout *videosLayout;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (nonatomic) NSMutableArray *sectionInfoArray;
@property (nonatomic) NSIndexPath *pinchedIndexPath;
@property (nonatomic) NSInteger openSectionIndex;
@property (nonatomic) CGFloat initialPinchHeight;
@property BOOL movieSelected;
@property BOOL tvShowsSelected;
@property BOOL latestOnlySelected;
@property BOOL stopped;
@property (strong, readwrite, nonatomic) UILabel* filterInfo;
@property (nonatomic) IBOutlet APLSectionHeaderView *sectionHeaderView;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;

// use the uniformRowHeight property if the pinch gesture should change all row heights simultaneously
@property (nonatomic) NSInteger  uniformRowHeight;
@property (nonatomic) NSUInteger results;
@property (nonatomic) NSUInteger neededResults;
@property BOOL  isVisible;


@end
static int completedMovies  =0 ;
static int completedTVShows = 0;
static int currentpagesTVShow = 0;
static int currentpagesMovies = 0;
static int pagesTVShows = 0;
static int pagesMovies = 0;

#pragma mark -

#define DEFAULT_ROW_HEIGHT 88
#define HEADER_HEIGHT 48


@implementation APLTableViewController
static NSMutableArray *transforms;

+(void) load{
    loadingCastQueue = dispatch_queue_create("loadingCastQueue", NULL);
    transforms = [NSMutableArray arrayWithCapacity:5000];
    dispatch_async(loadingCastQueue, ^{
        for(int i=0; i<5000; i++){
            CGFloat rotationPercentage  = ((CGFloat)(arc4random() % 220) - 110) * 0.0001f;
            usleep(500);
            CGFloat angle = 2 * M_PI * (1.0f + rotationPercentage);
            CATransform3D transform = CATransform3DMakeRotation(angle, 0.0f, 0.0f, 1.0f);
            [transforms  addObject:[NSValue valueWithCATransform3D:transform]];
        }
    });
    
    
}

- (void)viewDidLoad {
    self.thumbnailQueue = [[NSOperationQueue alloc] init];
    self.filterVideos = [NSMutableArray array];
    self.filterVideoNames = [NSMutableSet set];
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewSelection:) name:@"Selection" object:nil];
    
    // Add a pinch gesture recognizer to the table view.
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self.tableView addGestureRecognizer:pinchRecognizer];
    self.tableView.sectionHeaderHeight = HEADER_HEIGHT;
    self.uniformRowHeight = DEFAULT_ROW_HEIGHT;
    self.openSectionIndex = NSNotFound;
    [self.searchButton.layer setBorderWidth:1.0];
    [self.searchButton.layer setBorderColor:[[UIColor blackColor] CGColor]];
    self.searchButton.layer.cornerRadius = 3;
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];
    [self setFilterData];
    UITapGestureRecognizer *newTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activateDesactiveMovie:)];
    UITapGestureRecognizer *newTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activateDesactivateTVShow:)];
    [self.movieImageView setUserInteractionEnabled:YES];
    [self.movieImageView addGestureRecognizer:newTap];
    [self.tvShowImageView setUserInteractionEnabled:YES];
    [self.tvShowImageView addGestureRecognizer:newTap2];
    
    
    if ((self.sectionInfoArray == nil) ||
        ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.tableView])) {
        
        // For each criterion, set up a corresponding SectionInfo object to contain the default height for each row.
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        for (MYUCriteria *criterion in self.criteria) {
            
            APLSectionInfo *sectionInfo = [[APLSectionInfo alloc] init];
            sectionInfo.criteria = criterion;
            sectionInfo.open = NO;
            
            NSNumber *defaultRowHeight = @(DEFAULT_ROW_HEIGHT);
            NSInteger countOfvalues = [[sectionInfo.criteria values] count];
            for (NSInteger i = 0; i < countOfvalues; i++) {
                [sectionInfo insertObject:defaultRowHeight inRowHeightsAtIndex:i];
            }
            
            [infoArray addObject:sectionInfo];
        }
        
        self.sectionInfoArray = infoArray;
        _movieSelected          = NO;
        _tvShowsSelected        = NO;
        _latestOnlySelected     = NO;
        _stopped                = NO;
        self.stopButton.enabled = YES;
        
    }
    
    _savingQueue = dispatch_queue_create("ImageSavingQueue", NULL);
    
    _transforms = [transforms copy];
    
    _videosLayout= [[MYUVideoCollectionViewLayout alloc] initWithVerticalScrollDirection:NO];
    _videosLayout.transforms = _transforms;
    
    _videosLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    // other setup
    
    [_videosLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _videosLayout.verticalScroller = NO;
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:_videosLayout];
    // initalialize containersxx
    self.movies = [NSMutableArray array];
    self.moviesNames= [NSMutableSet set];
  
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerClass:[MYUVideoCell class]
            forCellWithReuseIdentifier:MYUTVSearchCellIdentifier];
    
    [self.collectionView registerClass:[MYUVideoTitleReusable class]
            forSupplementaryViewOfKind:ThumbnailCellTitle
                   withReuseIdentifier:ThumbnailCellTitle];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.isVisible = YES;
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.isVisible = NO;
}

- (IBAction)stopLoadingAndDeregister:(id)sender {
    [_titleTextField    resignFirstResponder];
    [_actorsTextField   resignFirstResponder];
}

-(void) cancel{
    _stopped = YES;
    
    if (self.movies.count>completedMovies ){
        while (self.movies.count>completedMovies) {
            MYUMovieItem* item = self.movies.lastObject;
            if (item ==nil)
                break;
            [self.moviesNames removeObject:item.videoThumbnail.title];
            [self.movies removeObject:item];
        }
        completedTVShows=0;
    }
    if (self.tvShows.count>completedTVShows){
        while (self.tvShows.count>completedTVShows) {
            MYUTVShowItem* item = self.tvShows.lastObject;
            if (item==nil)
                break;
            [self.tvShowsNames removeObject:item.videoThumbnail.title];
            [self.tvShows removeObject:item];
        }
        
        
        completedTVShows=0;
    }
    [self filterOnTitle:YES];
    [self deristerAndFilter:YES];
    
    [self filterOnTitle:NO];
    [self deristerAndFilter:NO];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.hud hideAnimated:NO];
        [self.collectionView reloadData];
    });
}




- (BOOL)canBecomeFirstResponder {
    
    return YES;
}
-(void) addMovies:(NSDictionary*) dictionary{
    
    NSArray* movies = [dictionary objectForKey:RESULTS];
    if([movies isKindOfClass:[NSArray class]]){
        for (NSDictionary* item in movies){
            
            MYUMovieItem* movieItem =[MYUVideoFactory createFromMovieDictionay:item];
            if ([self.moviesNames containsObject:[movies valueForKey:ORIGINAL_TITLE]]
                || [self.moviesNames containsObject:movieItem.videoThumbnail.title] ) {// if the image for the movie already exist
                
                continue;
            }
            else if (movieItem){
                
                [self.moviesNames addObject:movieItem.videoThumbnail.title];
                [self.movies addObject:movieItem];
                
            }
        }
    }
    self.currentMovies = self.movies;
    self.currentMoviesNames = self.moviesNames;
    
}

-(void) addTVShows:(NSDictionary*) dictionary{
    
    NSArray* tvShows = [dictionary objectForKey:RESULTS];
    if([tvShows isKindOfClass:[NSArray class]]){
        for (NSDictionary* item in tvShows){
            
            MYUTVShowItem* tvShowItem =[MYUVideoFactory createFromTVShowDictionay:item];
            if ([self.tvShowsNames containsObject:[tvShows valueForKey:ORIGINAL_TITLE]]
                ||  [self.tvShowsNames containsObject:tvShowItem.videoThumbnail.title]) // if the image for the TV show already exists
                continue;
            else if (tvShowItem){
                [self.tvShows addObject:tvShowItem];
                [self.tvShowsNames addObject:tvShowItem.videoThumbnail.title];
                
            }
        }
    }
    
}



#pragma mark - Observers and notifications

-(void) deristerAndFilter:(BOOL) movies{
    if(_results==_neededResults){
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    @try {
        if (movies){
            [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:MOVIE_CAST_OBTAINED];
        }else{
            [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:TV_SHOW_CAST_OBTAINED];
        }
        [self filterActors:movies];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
        
        [self filterActors:movies];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewSelection:) name:@"Selection" object:nil];
    
    
}


-(void) handleNotifications:(NSNotification*) note{
    
    NSString* noteName = [note name];
    
    BOOL movieSearched = [noteName isEqualToString:SEARCH_MOVIES_UPDATES];
    BOOL tvShowSearched = [noteName isEqualToString:SEARCH_TV_SHOWS_UPDATES];
    
    if(movieSearched){
        pagesTVShows = [[[note userInfo] objectForKey:@"pages"] intValue ];
        currentpagesTVShow = [[[note userInfo] objectForKey:@"currentPages"] intValue ];
        
        NSDictionary* newMovies = [MYUWebOperator sharedWebOperator].searchedMovies;
        [self addMovies:newMovies];
        
        
    }else if (tvShowSearched){
        pagesMovies = [[[note userInfo] objectForKey:@"pages"] intValue ];
        currentpagesMovies = [[[note userInfo] objectForKey:@"currentPages"] intValue ];
        
        NSDictionary* newTVShows = [MYUWebOperator sharedWebOperator].searchedTVShows;
        [self addTVShows:newTVShows];
    }
    
    [ self.hud.progressObject setCompletedUnitCount:currentpagesMovies+currentpagesTVShow];
    [ self.hud.progressObject setTotalUnitCount:pagesMovies+pagesTVShows];
    
    // update the collection view
    if ([[[note userInfo] objectForKey:@"completedMovies"] boolValue]){
        
        _results++;
        if (_latestOnlyWasSelectedAtInstantOfSearchLaunch){
            [self filterOnTitle:YES];
        }
        if (self.movies.count>0){
            __weak MYUWebOperator* operator = [MYUWebOperator sharedWebOperator];
            [operator addObserver:self forKeyPath:MOVIE_CAST_OBTAINED options:NSKeyValueObservingOptionNew |NSKeyValueObservingOptionOld context:nil];
            [self.MoviesIDs addObject:[NSNumber numberWithInt: ((MYUMovieItem*) self.movies[0]).movieID]];
            [operator castForMovieWithID:((MYUMovieItem*) self.movies[0]).movieID];
        }
        if (_results==_neededResults){
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            if (self.movies.count==0){
                
                [self.collectionView reloadData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.hud hideAnimated:NO];
                    self.filterInfo.hidden = NO;
                    self.filterInfo.text = NSLocalizedString(@"NoResultFound", @"NoResultFound");
                });
            }
        }
    }
    else if ([[[note userInfo] objectForKey:@"completedTVShows"] boolValue]){
        
        _results++;
        
        
        if (_latestOnlyWasSelectedAtInstantOfSearchLaunch){
            
            [self filterOnTitle:NO];
        }
        if (self.tvShows.count>0){
            
            __weak MYUWebOperator* operatorForast = [MYUWebOperator sharedWebOperator];
            [operatorForast addObserver:self forKeyPath:TV_SHOW_CAST_OBTAINED options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
            [self.TVShowIDs addObject:[NSNumber numberWithInt: ((MYUTVShowItem*) self.tvShows[0]).tvShowID]];
            [operatorForast castForTVWithID:((MYUTVShowItem*) self.tvShows[0]).tvShowID];
            
        }
        if (_results==_neededResults){
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            if (self.tvShows.count==0){
                [self.collectionView reloadData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.hud hideAnimated:NO];
                    self.filterInfo.hidden = NO;
                    self.filterInfo.text = NSLocalizedString(@"NoResultFound", @"NoResultFound");
                    
                });
            }
        }
        
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewSelection:) name:@"Selection" object:nil];
    
}

static time_t start, end;

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if( _stopped){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.hud hideAnimated:NO];
        });
        
        return;
    }
    
    if (completedTVShows==1 || completedMovies==1){
        self.hud.label.text = NSLocalizedString(@"Parsing...", @"HUD loading title");
        [self.hud.button setTitle:NSLocalizedString(@"Stop", @"HUD cancel button title") forState:UIControlStateNormal];
    }
    if([keyPath isEqualToString:MOVIE_CAST_OBTAINED]){
        
        NSDictionary* castDisctionary = [MYUWebOperator sharedWebOperator].movieCast ;
        if (castDisctionary==nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.hud hideAnimated:NO];
            });
            return;
        }
        
        const int movieID =  [[castDisctionary objectForKey:@"id"] intValue];
        NSPredicate* predicate  = [NSPredicate predicateWithFormat:@"(movieID = %d)",movieID ];
        completedMovies++;
        //  NSLog(@"counting %d of %lu",completedMovies,(unsigned long)self.movies.count);
        NSArray* results = [self.movies filteredArrayUsingPredicate:predicate];
        if (results==nil || results.count==0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.hud hideAnimated:NO];
            });
            return;
        }
        [results[0] addCastDetails:castDisctionary];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completedMovies==1){
                
                [self.hud.progressObject setTotalUnitCount:self.movies.count+self.tvShows.count];
                
            }else{
                [ self.hud.progressObject setCompletedUnitCount:completedMovies+completedTVShows];
            }
        });
        if (completedMovies==self.movies.count){
            
            [self deristerAndFilter:YES];
            
        }else {
            if (self.movies.count>completedMovies){
                MYUMovieItem* item = (MYUMovieItem*) self.movies[completedMovies];
                [self.MoviesIDs addObject:[NSNumber numberWithInt: item.movieID]];
                [[MYUWebOperator sharedWebOperator] castForMovieWithID:item.movieID];
            }
        }
    }
    else if ([keyPath isEqualToString:TV_SHOW_CAST_OBTAINED]){
        NSDictionary* castDisctionary = [MYUWebOperator sharedWebOperator].tvShowCast ;
        const int tvShowID =  [[castDisctionary objectForKey:@"id"] intValue];
        NSPredicate* predicate  = [NSPredicate predicateWithFormat:@"(tvShowID = %d)",tvShowID ];
        completedTVShows++;
        //  NSLog(@"counting tv shows %d of %lu",completedTVShows,(unsigned long)self.tvShows.count);
        NSArray* results = [self.tvShows filteredArrayUsingPredicate:predicate];
        if (results==nil || results.count==0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.hud hideAnimated:NO];
            });
            return;
        }
        [results[0] addCastDetails:castDisctionary];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completedTVShows==1){
                
                [self.hud.progressObject setTotalUnitCount:MAX(self.movies.count,10)+ self.tvShows.count];
                
            }else{
                
                [ self.hud.progressObject setCompletedUnitCount:completedTVShows+completedMovies];
                
            }
        });
        if (completedTVShows==self.tvShows.count){
            
            [self deristerAndFilter:NO];
            
        }else {
            if (self.tvShows.count>completedTVShows){
                MYUTVShowItem* item = (MYUTVShowItem*) self.tvShows[completedTVShows];
                [self.TVShowIDs addObject:[NSNumber numberWithInt: item.tvShowID]];
                [[MYUWebOperator sharedWebOperator] castForTVWithID:item.tvShowID];
            }
        }
        
        
    }
    else if ([keyPath isEqualToString:@"fractionCompleted"]) {
        NSProgress *progress = (NSProgress *)object;
        [self.hud setProgress:progress.fractionCompleted];
        
    }
    
    if (completedMovies==self.movies.count && completedTVShows==self.tvShows.count){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
            [self.hud hideAnimated:NO];
        });
    }
}

#pragma mark - Data Filtering

-(void) filterOnTitle:(BOOL) filteringMovies{
    NSString* title  = _titleTextField.text;
    NSString *probablyEmpty = [title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (![probablyEmpty isEqualToString:@""]){
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(title CONTAINS[cd] %@)",probablyEmpty  ];
        if (filteringMovies){
            __weak NSMutableArray* array = self.movies;
            self.movies =[NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicate]];
        }else{
            __weak NSMutableArray* array = self.tvShows;
            self.tvShows =[NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicate]];
        }
    }
    if (filteringMovies){
        self.moviesNames = [NSMutableSet set];
        for (MYUAbstractVideoItem* item in self.movies){
            [self.moviesNames addObject:item.videoThumbnail.title];
        }
        self.currentMovies = self.movies;
        self.currentMoviesNames = self.moviesNames;
    }else{
        self.TVShowNames = [NSMutableSet set];
        for (MYUAbstractVideoItem* item in self.movies){
            [self.TVShowNames addObject:item.videoThumbnail.title];
        }
        self.currentTVShows = self.tvShows;
        self.currentTVShowsNames = self.TVShowNames;
    }
}

- (void)filterActors:(BOOL)filteringMovie {
    NSString* actors  = _actorsTextField.text;
    NSString *probablyEmpty = [actors stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![probablyEmpty isEqualToString:@""]){
        NSArray *listItems = [actors componentsSeparatedByString:@","];
        NSMutableArray* predicates  = [NSMutableArray array];
        
        for (NSString* actorname in  listItems){
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(combinedActors CONTAINS[cd] %@)",actorname ];
            [predicates addObject:predicate];
        }
        NSPredicate *compoundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
        if (filteringMovie){
            self.currentMovies  =[NSMutableArray arrayWithArray:[self.movies filteredArrayUsingPredicate:compoundPredicate]];
            self.currentMoviesNames = [NSMutableSet set];
            
            for (MYUMovieItem* movie in self.currentMovies){
                if ([self.currentMoviesNames containsObject:movie.videoThumbnail.title]) {
                    [self.currentMovies  removeObject:movie];
                    continue;
                }else{
                    [self.currentMoviesNames addObject:movie.videoThumbnail.title];
                }
            }
        }else{
            self.currentTVShows  =[NSMutableArray arrayWithArray:[self.tvShows filteredArrayUsingPredicate:compoundPredicate]];
            self.currentTVShowsNames = [NSMutableSet set];
            
            for (MYUTVShowItem* tvShow in self.currentTVShows){
                if ([self.currentTVShowsNames containsObject:tvShow.videoThumbnail.title]) {
                    [self.currentTVShows  removeObject:tvShow];
                    continue;
                }else{
                    [self.currentTVShowsNames addObject:tvShow.videoThumbnail.title];
                }
            }
        }
    }
    else{
        
        
        if (filteringMovie){
            self.currentMovies  = self.movies;
            self.currentMoviesNames = [NSMutableSet set];
            
            for (MYUMovieItem* movie in self.currentMovies){
                if ([self.currentMoviesNames containsObject:movie.videoThumbnail.title]) {
                    [self.currentMovies  removeObject:movie];
                    continue;
                }else{
                    [self.currentMoviesNames addObject:movie.videoThumbnail.title];
                }
            }
        }else{
            self.currentTVShows  = self.tvShows;
            self.currentTVShowsNames = [NSMutableSet set];
            
            for (MYUTVShowItem* tvShow in self.currentTVShows){
                if ([self.currentTVShowsNames containsObject:tvShow.videoThumbnail.title]) {
                    [self.currentTVShows  removeObject:tvShow];
                    continue;
                }else{
                    [self.currentTVShowsNames addObject:tvShow.videoThumbnail.title];
                }
            }
        }
    }
    
    
    
}
-(BOOL)areAllSectionClosed{
    return self.openSectionIndex == NSNotFound;
}
-(void) showExtraFilterText{
    if([self areAllSectionClosed]){
        if (self.filterInfo){
            [self.filterInfo removeFromSuperview];
        }
        self.filterInfo = [[UILabel alloc] initWithFrame:CGRectMake(0, self.tableView.frame.size.height - 20 , self.tableView.frame.size.width, 20)];
        if (self.videos.count >0 ){
            
            self.filterInfo.text = [NSString stringWithFormat:@"%@ %lu %@ %lu",NSLocalizedString(@"Showing", @"Showing"),(unsigned long)self.filterVideos.count,NSLocalizedString(@"of", @"of"),(unsigned long)self.videos.count];
            self.filterInfo.textAlignment = NSTextAlignmentCenter;
            [self.filterInfo setTextColor:[UIColor blackColor]];
            [self.filterInfo setBackgroundColor:[UIColor clearColor]];
            [self.filterInfo setFont:[UIFont fontWithName: @"Trebuchet MS" size: 14.0f]];
            [self.tableView addSubview:self.filterInfo];
        }else if(self.titleTextField.text.length>0 || self.actorsTextField.text.length>0){
            self.filterInfo.text = NSLocalizedString(@"NoResultFound", @"NoResultFound");
            self.filterInfo.textAlignment = NSTextAlignmentCenter;
            [self.filterInfo setTextColor:[UIColor blackColor]];
            [self.filterInfo setBackgroundColor:[UIColor clearColor]];
            [self.filterInfo setFont:[UIFont fontWithName: @"Trebuchet MS" size: 14.0f]];
            [self.tableView addSubview:self.filterInfo];
        }
    }
}
-(void) hideExtraFilterText{
    if (self.filterInfo){
        self.filterInfo.hidden = YES;
        [self.filterInfo removeFromSuperview];
        self.filterInfo = nil;
    }
}

#pragma mark - Navigation

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.filterVideos.count<=indexPath.section)
        return;
    if ([self.filterVideos[indexPath.section] isKindOfClass:[MYUMovieItem class]]){
        MYUMovieItem *item =  self.filterVideos[indexPath.section];
        [self performSegueWithIdentifier:@"ShowMovieDetails" sender:item];
    }else   if ([self.filterVideos[indexPath.section] isKindOfClass:[MYUTVShowItem class]]){
        MYUTVShowItem *item =  self.filterVideos[indexPath.section];
        [self performSegueWithIdentifier:@"ShowTVShowDetails" sender:item];
    }
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if ([segue.identifier isEqualToString:@"ShowMovieDetails"]){
        UINavigationController *navigationController = segue.destinationViewController;
        MYUMovieDetailsVC *moviesVC = [navigationController viewControllers][0];
        MYUMovieItem *item = (MYUMovieItem *) sender;
        [moviesVC setMovieItem:item];
        moviesVC.title =item.videoThumbnail.title;
        [navigationController setTitle:[NSString stringWithFormat:@"%@",item.videoThumbnail.title]];
    }else{
        UINavigationController *navigationController  = segue.destinationViewController;
        MYUTVShowDetailsVC *tvShowsVC  =  [navigationController viewControllers][0];;
        MYUTVShowItem *tvShow =(MYUTVShowItem *) sender;
        tvShow.videoThumbnail.poster= [[MYUCache sharedCache] imageForMovieTitle:tvShow.videoThumbnail.title];
        if (tvShow.videoThumbnail.poster==nil){
            MYUThumbnail *thumbnail = [MYUThumbailFactory  thumnailWithImageURL:tvShow.videoThumbnail.imageURL andTitle:tvShow.videoThumbnail.title];
            tvShow.videoThumbnail = thumbnail;
        }
        [tvShowsVC setTvShowItem:tvShow];
        tvShowsVC.title =tvShow.videoThumbnail.title;
        [tvShowsVC setTitle:[NSString stringWithFormat:@"%@",tvShow.videoThumbnail.title]];
    }
    
}


#pragma mark - Search

-(void) performSearch{
    
    // reset the collection data source
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.movies      =[NSMutableArray array];
    self.moviesNames =[NSMutableSet set];
    self.MoviesIDs         = [NSMutableSet set];
    self.currentMovies = [NSMutableArray array];
    self.currentMoviesNames = [NSMutableSet set];
    
    self.tvShows      =[NSMutableArray array];
    self.tvShowsNames =[NSMutableSet set];
    self.TVShowIDs         = [NSMutableSet set];
    self.currentTVShows = [NSMutableArray array];
    self.currentTVShowsNames = [NSMutableSet set];
    _results = 0;
    _neededResults=0;
    completedTVShows= 0;
    completedMovies=0;
    _stopped = NO;
    currentpagesTVShow = 0;
    currentpagesMovies = 0;
    pagesTVShows = 0;
    pagesMovies = 0;
    self.tvShowsNames = [NSMutableSet set];
    self.tvShows = [NSMutableArray array];
    
    _latestOnlyWasSelectedAtInstantOfSearchLaunch = _latestOnlySelected;
    NSString* titleKeyword  = _titleTextField.text;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNotifications:)
                                                 name:SEARCH_MOVIES_UPDATES
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNotifications:)
                                                 name:SEARCH_TV_SHOWS_UPDATES
                                               object:nil];
    
    if (_movieSelected) {
        _neededResults++;
        [[MYUWebOperator sharedWebOperator] searchMovies:_latestOnlySelected titleKeyWord:[titleKeyword urlencode] actorKeyWord:@"" genres:nil ratingLowBound:0 ratingUpperBound:0 others:nil];
    }
    
    if (_tvShowsSelected){
        _neededResults++;
        [[MYUWebOperator sharedWebOperator] searchTVShows:_latestOnlySelected titleKeyWord:[titleKeyword urlencode] actorKeyWord:@"" genres:nil ratingLowBound:0 ratingUpperBound:0 others:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewSelection:) name:@"Selection" object:nil];
    
    
}
- (IBAction)search:(id)sender {
    
    completedMovies =0;
    completedTVShows=0;
    
    if(![self areAllSectionClosed]){
        [self closeSection:self.openSectionIndex];
        [self.tableView reloadData];
    }
    NSString *probablyEmptyTitle = [self.titleTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *probablyEmptyActors = [self.actorsTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    if (!_tvShowsSelected && !_movieSelected){
        
        
        NSString* toastMessage =  NSLocalizedString(@"Movies or/and TV Shows must selected", @"Movies or/and TV Shows must selected");
        
        [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
        return ;
    }
    
    if ([probablyEmptyTitle isEqualToString:@""]){
        if ([probablyEmptyActors isEqualToString:@""]){
            NSString* toastMessage =    NSLocalizedString(@"EmptytitleNoActors",@"Empty movie title");
            [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
            return ;
        }
        if (!(_latestOnlySelected && ![probablyEmptyActors isEqualToString:@""])){
            NSString* toastMessage =    NSLocalizedString(@"You must select 'Latest Only' as you did not provide title",@"Empty movie title");
            [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
            return ;
        }
        
    }
    if (!(probablyEmptyTitle.length>=3 || probablyEmptyActors.length>4)){
        NSString* toastMessage =    NSLocalizedString(@"Provide tilte or actor names too short",@"Provide tilte or actor names too short");
        [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
        return ;
    }
    
    
    if([MYUWebOperator sharedWebOperator].connectedToInternet){
        
        dispatch_async(dispatch_get_main_queue(),^{
            [self.hud hideAnimated:YES];
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            self.hud.mode = MBProgressHUDModeDeterminate;
            self.hud.label.text = NSLocalizedString(@"Requesting...", @"HUD loading title");
            NSProgress *progressObject = [[NSProgress alloc] initWithParent:[NSProgress currentProgress] userInfo:nil];
            [progressObject addObserver:self
                             forKeyPath:@"fractionCompleted"
                                options:NSKeyValueObservingOptionNew
                                context:NULL];
            self.hud.progressObject = progressObject;
            [self.hud.button addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
            
            // Configure a cancel button.
            [self.hud.button setTitle:NSLocalizedString(@"Cancel", @"HUD cancel button title") forState:UIControlStateNormal];
            
        });
        [self performSearch];
        
    }
    else{
        /// Display the offline toast
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString* toastMessage = NSLocalizedString(@"Offline", @"Offline");
            [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
        });
    }
    [_titleTextField resignFirstResponder];
    [_actorsTextField resignFirstResponder];
}

#pragma mark - UITableViewDataSource
- (void) setFilterData{
    
    if (_criteria == nil) {
        
        NSURL *url = [[NSBundle mainBundle] URLForResource:FILTER_CRIETRIA_ARRAY withExtension:@"plist"];
        NSArray *criterionDictionariesArray = [[NSArray alloc ] initWithContentsOfURL:url];
        _criteria = [NSMutableArray arrayWithCapacity:[criterionDictionariesArray count]];
        for (NSDictionary *criterionDictionary in criterionDictionariesArray) {
            
            MYUCriteria *criteria = [[MYUCriteria alloc] init];
            criteria.name = NSLocalizedString(criterionDictionary[@"name"], criterionDictionary[@"name"]);
            
            NSArray *genreArrays = criterionDictionary[@"values"]; // list of result ids
            NSMutableArray *values = [NSMutableArray arrayWithCapacity:[genreArrays count]];
            
            for (NSString *itemName in genreArrays) {
                
                MYUCriterion *criterion = [[MYUCriterion alloc] init];
                criterion.name = NSLocalizedString(itemName, itemName) ;
                if([criterion.name isEqualToString:NSLocalizedString(@"All", @"All") ]){
                    criterion.selected = YES;
                }else{
                    criterion.selected = NO;
                }
                [values addObject:criterion];
            }
            criteria.values = values;
            
            [self.criteria addObject:criteria];
        }
    }
    
    
}
#pragma mark - Table View Delegate and Data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.criteria count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CONVENIENT_HEIGHT_FOR_CELL - 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    APLSectionInfo *sectionInfo = (self.sectionInfoArray)[section];
    NSInteger numStoriesInSection = [[sectionInfo.criteria values] count];
    
    return sectionInfo.open ? numStoriesInSection : 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *QuoteCellIdentifier = @"QuoteCellIdentifier";
    MYUQuoteCell    *cell = (MYUQuoteCell*)[tableView dequeueReusableCellWithIdentifier:QuoteCellIdentifier];
    MYUCriteria     *criteria   = (MYUCriteria *)[(self.sectionInfoArray)[indexPath.section] criteria];
    MYUCriterion    *criterion  = (criteria.values)[indexPath.row];
    cell.section    = indexPath.section ;
    cell.criterion  = criterion;
    
    if(cell.criterion.selected){
        [cell.checkButton setImage:[UIImage imageNamed:@"Checked"] forState:UIControlStateNormal];
    }else{
        [cell.checkButton setImage:[UIImage imageNamed:@"Ok-Unchecked"] forState:UIControlStateNormal];
        
    }
    cell.criterionName.text = criterion.name;
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    APLSectionHeaderView *sectionHeaderView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderViewIdentifier];
    
    APLSectionInfo *sectionInfo = (self.sectionInfoArray)[section];
    sectionInfo.headerView = sectionHeaderView;
    
    sectionHeaderView.titleLabel.text = sectionInfo.criteria.name;
    sectionHeaderView.section = section;
    sectionHeaderView.delegate = self;
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return CONVENIENT_HEIGHT_FOR_CELL;
}


- (IBAction)activateDesactivateTVShow:(id)sender {
    
    _tvShowsSelected =  !_tvShowsSelected ;
    if(_tvShowsSelected){
        [((UIButton*) sender) setImage:[UIImage imageNamed:@"CircleSelected_small"] forState:UIControlStateNormal ];
    }else{
        [((UIButton*) sender) setImage:[UIImage imageNamed:@"CircleFullWhite_small"] forState:UIControlStateNormal ];
    }
}

- (IBAction)activateDesactiveMovie:(id)sender {
    _movieSelected =  !_movieSelected ;
    if(_movieSelected){
        [((UIButton*) sender) setImage:[UIImage imageNamed:@"CircleSelected_small"] forState:UIControlStateNormal ];
    }else{
        [((UIButton*) sender) setImage:[UIImage imageNamed:@"CircleFullWhite_small"] forState:UIControlStateNormal ];
    }
    
}
- (IBAction)activateDesactiveLatestOnly:(id)sender {
    _latestOnlySelected =  !_latestOnlySelected ;
    if(_latestOnlySelected){
        [((UIButton*) sender) setImage:[UIImage imageNamed:@"CircleSelected_small"] forState:UIControlStateNormal ];
    }else{
        [((UIButton*) sender) setImage:[UIImage imageNamed:@"CircleFullWhite_small"] forState:UIControlStateNormal ];
    }
    
}


#pragma mark - SectionHeaderViewDelegate

- (void)sectionHeaderView:(APLSectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    
    [self hideExtraFilterText];
    
    APLSectionInfo *sectionInfo = (self.sectionInfoArray)[sectionOpened];
    sectionInfo.open = YES;
    
    /*
     Create an array containing the index paths of the rows to insert: These correspond to the rows for each quotation in the current section.
     */
    NSInteger countOfRowsToInsert = [sectionInfo.criteria.values count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    if (previousOpenSectionIndex != NSNotFound) {
        
        APLSectionInfo *previousOpenSection = (self.sectionInfoArray)[previousOpenSectionIndex];
        previousOpenSection.open = NO;
        [previousOpenSection.headerView toggleOpenWithUserAction:NO];
        NSInteger countOfRowsToDelete = [previousOpenSection.criteria.values count];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
    }
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tableView endUpdates];
    self.openSectionIndex = sectionOpened;
}

- (void)closeSection:(NSInteger)sectionClosed {
    
    
    APLSectionInfo *sectionInfo = (self.sectionInfoArray)[sectionClosed];
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
}

- (void)sectionHeaderView:(APLSectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    
    [self closeSection:sectionClosed];
    
    [self.collectionView reloadData];
}

#pragma mark - Handling pinches

- (void)handlePinch:(UIPinchGestureRecognizer *)pinchRecognizer {
    
    if (pinchRecognizer.state == UIGestureRecognizerStateBegan) {
        
        CGPoint pinchLocation = [pinchRecognizer locationInView:self.tableView];
        NSIndexPath *newPinchedIndexPath = [self.tableView indexPathForRowAtPoint:pinchLocation];
        self.pinchedIndexPath = newPinchedIndexPath;
        
        APLSectionInfo *sectionInfo = (self.sectionInfoArray)[newPinchedIndexPath.section];
        self.initialPinchHeight = [[sectionInfo objectInRowHeightsAtIndex:newPinchedIndexPath.row] floatValue];
        [self updateForPinchScale:pinchRecognizer.scale atIndexPath:newPinchedIndexPath];
    }
    else {
        if (pinchRecognizer.state == UIGestureRecognizerStateChanged) {
            [self updateForPinchScale:pinchRecognizer.scale atIndexPath:self.pinchedIndexPath];
        }
        else if ((pinchRecognizer.state == UIGestureRecognizerStateCancelled) || (pinchRecognizer.state == UIGestureRecognizerStateEnded)) {
            self.pinchedIndexPath = nil;
        }
    }
}

- (void)updateForPinchScale:(CGFloat)scale atIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath && (indexPath.section != NSNotFound) && (indexPath.row != NSNotFound)) {
        
        CGFloat newHeight = round(MAX(self.initialPinchHeight * scale, DEFAULT_ROW_HEIGHT));
        APLSectionInfo *sectionInfo = (self.sectionInfoArray)[indexPath.section];
        [sectionInfo replaceObjectInRowHeightsAtIndex:indexPath.row withObject:@(newHeight)];
        BOOL animationsEnabled = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
        [UIView setAnimationsEnabled:animationsEnabled];
    }
}


#pragma mark - CollectionView delegate and datasource
-(void) mergeResults{
    self.videos = [NSMutableArray arrayWithArray:[self.currentMovies arrayByAddingObjectsFromArray:self.currentTVShows]];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    self.videos =[NSMutableArray arrayWithArray:[ self.videos sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]]];
}
-(void) handleNewSelection:(NSNotification*) note{
    
    NSUInteger section = [[[note userInfo] objectForKey:@"section"] integerValue];
    BOOL selected = [[[note userInfo] objectForKey:@"selected"] boolValue];
    MYUCriterion* sender = ((MYUCriterion*) [[note userInfo] objectForKey:@"object"]);
    NSString* name = sender.name ;
    NSPredicate* predicate  = [NSPredicate predicateWithFormat:@"(name = %@)",name ];
    NSArray* selectedArray  = ((MYUCriteria*)[self.criteria objectAtIndex:section]).values;
    NSArray<MYUCriterion*> *results = [selectedArray filteredArrayUsingPredicate:predicate];
    
    if(results.count!=1){
        return;
    } else{
        (results[0]).selected = selected;
    }
    
    if(selected){
        if(section==2){
            NSArray<NSIndexPath *> *indices = [self.tableView indexPathsForVisibleRows];
            NSMutableArray* paths =[NSMutableArray arrayWithCapacity:indices.count];
            const int count = (int)((MYUCriteria*) [self.criteria objectAtIndex:section]).values.count;
            if ([name isEqualToString:NSLocalizedString(@"MoviesOnly",@"MoviesOnly")]){
                for (int i=0; i < count; i++){
                    NSLog(@"%@",[self.criteria objectAtIndex:section].values[i].name);
                    if([[self.criteria objectAtIndex:section].values[i].name isEqualToString:NSLocalizedString(@"TVShowOnly",@"TVShowOnly")]){
                        ([self.criteria objectAtIndex:section].values[i]).selected = NO;
                        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:section];
                        if ([indices containsObject:path]){
                            [paths addObject:path];
                        }
                        break;
                    }
                }
            }
            if ([name isEqualToString:NSLocalizedString(@"TVShowOnly",@"TVShowOnly")]){
                for (int i=0; i < count; i++){
                    if([[self.criteria objectAtIndex:section].values[i].name isEqualToString:NSLocalizedString(@"MoviesOnly",@"MoviesOnly")]){
                        NSLog(@"%@",[self.criteria objectAtIndex:section].values[i].name);
                        
                        ([self.criteria objectAtIndex:section].values[i]).selected = NO;
                        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:section];
                        if ([indices containsObject:path]){
                            [paths addObject:path];
                        }
                        
                        break;
                    }
                }
            }
            
            if ([name isEqualToString:NSLocalizedString(@"ReleasedThisYear",@"ReleasedThisYear")]){
                for (int i=0; i < count; i++){
                    NSLog(@"%@",[self.criteria objectAtIndex:section].values[i].name);
                    if([[self.criteria objectAtIndex:section].values[i].name isEqualToString:NSLocalizedString(@"ReleasedThisMonth",@"ReleasedThisMonth")]){
                        ([self.criteria objectAtIndex:section].values[i]).selected = NO;
                        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:section];
                        if ([indices containsObject:path]){
                            [paths addObject:path];
                        }
                        break;
                    }
                }
            }
            if ([name isEqualToString:NSLocalizedString(@"ReleasedThisMonth",@"ReleasedThisMonth")]){
                for (int i=0; i < count; i++){
                    if([[self.criteria objectAtIndex:section].values[i].name isEqualToString:NSLocalizedString(@"ReleasedThisYear",@"ReleasedThisYear")]){
                        NSLog(@"%@",[self.criteria objectAtIndex:section].values[i].name);
                        
                        ([self.criteria objectAtIndex:section].values[i]).selected = NO;
                        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:section];
                        if ([indices containsObject:path]){
                            [paths addObject:path];
                        }
                        
                        break;
                    }
                }
            }
            if ([name isEqualToString:NSLocalizedString(@"OriginalLanguageEnglish",@"OriginalLanguageEnglish")]){
                for (int i=0; i < count; i++){
                    NSLog(@"%@",[self.criteria objectAtIndex:section].values[i].name);
                    if([[self.criteria objectAtIndex:section].values[i].name isEqualToString:NSLocalizedString(@"OriginalLanguageFrench",@"OriginalLanguageFrench")]){
                        ([self.criteria objectAtIndex:section].values[i]).selected = NO;
                        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:section];
                        if ([indices containsObject:path]){
                            [paths addObject:path];
                        }
                        break;
                    }
                }
            }
            if ([name isEqualToString:NSLocalizedString(@"OriginalLanguageFrench",@"OriginalLanguageFrench")]){
                for (int i=0; i < count; i++){
                    NSLog(@"%@ %@",[self.criteria objectAtIndex:section].values[i].name,NSLocalizedString(@"OriginalLanguageEnglish",@"OriginalLanguageEnglish"));
                    
                    if([[self.criteria objectAtIndex:section].values[i].name isEqualToString:NSLocalizedString(@"OriginalLanguageEnglish",@"OriginalLanguageEnglish")]){
                        
                        ([self.criteria objectAtIndex:section].values[i]).selected = NO;
                        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:section];
                        if ([indices containsObject:path]){
                            [paths addObject:path];
                        }
                        
                        break;
                    }
                }
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationNone];
            });
            
        }else{
            if ([name isEqualToString:NSLocalizedString(@"All", @"All")]){
                const int count = (int)((MYUCriteria*) [self.criteria objectAtIndex:section]).values.count;
                NSArray<NSIndexPath *> *indices = [self.tableView indexPathsForVisibleRows];
                NSMutableArray* paths =[NSMutableArray arrayWithCapacity:indices.count];
                for (int i=1; i < count; i++){
                    ([self.criteria objectAtIndex:section].values[i]).selected = NO;
                    NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:section];
                    if ([indices containsObject:path]){
                        [paths addObject:path];
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationNone];
                });
            }else{
                if ( [([self.criteria objectAtIndex:section].values[0]).name isEqualToString:NSLocalizedString(@"All", @"All")]){
                    ([self.criteria objectAtIndex:section].values[0]).selected = NO;
                    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:section];
                    NSArray<NSIndexPath *> *indices = [self.tableView indexPathsForVisibleRows];
                    
                    if ([indices containsObject:path]){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSArray* paths=[NSArray arrayWithObjects:path, nil];
                            [self.tableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationNone];
                        });
                        
                    }
                }
            }
        }
    }
}

-(void) filterResults{
    self.filterVideos = self.videos;
    NSMutableArray<MYUCriterion*>* selectedCotes  = [NSMutableArray array];
    NSMutableArray<MYUCriterion*>* selectedGenres = [NSMutableArray array];
    NSMutableArray<MYUCriterion*>* selectedOther  = [NSMutableArray array];
    NSMutableArray* current;
    NSUInteger index  = 0;
    for (MYUCriteria* object in self.criteria){
        NSArray* values = [object valueForKey:@"values"];
        
        switch (index) {
            case 0:
                current = selectedGenres;
                break;
            case 1:
                current = selectedCotes;
                break;
            case 2:
                current = selectedOther;
                break;
            default:
                break;
        }
        for (MYUCriterion* item in values){
            if (item.selected){
                [current addObject:item];
            }
        }
        index++;
    }
    
    NSMutableArray* predicates = [NSMutableArray array];
    if (selectedCotes.count>0 ){
        NSString* all = NSLocalizedString(@"All", @"All");
        if (selectedCotes.count==1 && [selectedCotes[0].name isEqualToString:all]){
            _filterVideos = self.videos;
        }
        else{
            for (MYUCriterion* item in selectedCotes){
                NSString* values = [item valueForKey:@"name"];
                NSArray* valuesSplit = [values componentsSeparatedByString:@"-"];
                NSUInteger lowerbound = 0;
                NSUInteger upperbound = 0;
                if(valuesSplit.count==2){
                    lowerbound =  [valuesSplit[0] integerValue];
                    upperbound =  [valuesSplit[1] integerValue];
                    NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(rating >= %d AND rating <%d)",lowerbound,upperbound ];
                    [predicates addObject:cot];
                    
                }else{
                    lowerbound = [[values componentsSeparatedByString:@"<"][0] integerValue];
                    upperbound = 10;
                    NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(rating >= %d )",upperbound ];
                    [predicates addObject:cot];
                    
                }
            }
            NSCompoundPredicate* ratingCompound = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
            
            _filterVideos = [NSMutableArray arrayWithArray:[self.videos filteredArrayUsingPredicate:ratingCompound]];
        }
    }
    predicates = [NSMutableArray array];
    if (selectedGenres.count>0 ){
        NSString* all = NSLocalizedString(@"All", @"All");
        if (!(selectedGenres.count==1 && [selectedGenres[0].name isEqualToString:all])){
            
            for (MYUCriterion* item in selectedGenres){
                NSString* genre = [item valueForKey:@"name"];
                
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(combinedGenres CONTAINS[cd] %@)",genre ];
                [predicates addObject:cot];
            }
            NSCompoundPredicate* genreCompound = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
            _filterVideos = [NSMutableArray arrayWithArray:[_filterVideos filteredArrayUsingPredicate:genreCompound]];
        }
    }
    predicates = [NSMutableArray array];
    if (selectedOther.count>0 ){
        
        for (MYUCriterion* item in selectedOther){
            NSString* otherName = [item valueForKey:@"name"];
            
            if ([otherName isEqualToString:NSLocalizedString(@"MoviesOnly",@"MoviesOnly")]){
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(isMovie == TRUE)" ];
                [predicates addObject:cot];
            }
            if ([otherName isEqualToString:NSLocalizedString(@"TVShowOnly",@"TVShowOnly")]){
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(isMovie == NO)"];
                [predicates addObject:cot];
            }
            if ([otherName isEqualToString:NSLocalizedString(@"AdultMoviesOnly",@"AdultMoviesOnly")]){
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(isAdultOnly == YES)"];
                [predicates addObject:cot];
            }
            NSDate* startOfThisYear = [APLTableViewController startOfThisYear];
            if ([otherName isEqualToString:NSLocalizedString(@"ReleasedThisYear","ReleasedThisYear")]){
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(releaseDate > %@ AND releaseDate < %@)",startOfThisYear,[startOfThisYear dateByAddingTimeInterval:365*24*3600]];
                [predicates addObject:cot];
            }
            NSDate* startOfthisMonth = [APLTableViewController startOfThisMonth];
            if ([otherName isEqualToString:NSLocalizedString(@"ReleasedThisMonth",@"ReleasedThisMonth")]){
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(releaseDate > %@ AND releaseDate < %@)",startOfthisMonth,[startOfthisMonth dateByAddingTimeInterval:30*24*3600]];
                [predicates addObject:cot];
            }
            
            if ([otherName isEqualToString:NSLocalizedString(@"OriginalLanguageFrench",@"OriginalLanguageFrench")]){
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(originalLanguage CONTAINS[cd] 'Fr')"];
                [predicates addObject:cot];
            }
            if ([otherName isEqualToString:NSLocalizedString(@"OriginalLanguageEnglish",@"OriginalLanguageEnglish")]){
                NSPredicate* cot  = [NSPredicate predicateWithFormat:@"(originalLanguage CONTAINS[cd] 'En')"];
                [predicates addObject:cot];
            }
        }
        NSCompoundPredicate* genreCompound = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        _filterVideos = [NSMutableArray arrayWithArray:[_filterVideos filteredArrayUsingPredicate:genreCompound]];
        
    }
}
+(NSDate*) startOfThisYear{
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateComponents * comp = [cal components:( NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [comp setSecond:0];
    [comp setMinute:0];
    [comp setHour:0];
    [comp setHour:0];
    [comp setDay:1];
    [comp setMonth:1];
    
    NSDate *startOfThisMonth = [cal dateFromComponents:comp];
    return startOfThisMonth;
}

+(NSDate*) startOfThisMonth{
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateComponents * comp = [cal components:( NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    [comp setSecond:0];
    [comp setMinute:0];
    [comp setHour:0];
    [comp setHour:0];
    [comp setDay:1];
    
    NSDate *startOfThisMonth = [cal dateFromComponents:comp];
    return startOfThisMonth;
}



-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    [self mergeResults];
    [self filterResults];
    [self showExtraFilterText];
    return  self.filterVideos.count;
}
-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

#define MY_HEADER_LABEL_TAG 128

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath;
{
    MYUVideoTitleReusable *titleView =
    [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                       withReuseIdentifier:ThumbnailCellTitle
                                              forIndexPath:indexPath];
    
    MYUAbstractVideoItem *item = self.filterVideos[indexPath.section];
    titleView.titleLabel.text =  item.videoThumbnail.title;
    return titleView;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MYUVideoCell *videoCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:MYUTVSearchCellIdentifier
                                              forIndexPath:indexPath];
    
    MYUAbstractVideoItem *video = self.filterVideos[indexPath.section];
    MYUThumbnail *thumbnail     = video.videoThumbnail;
    videoCell.imageView.image = [[MYUCache sharedCache] imageForMovieTitle:thumbnail.title];
    __strong NSIndexPath* indexPathCopy = [indexPath copy];
    
    if (! videoCell.imageView.image || [videoCell.imageView.image isEqual:[[MYUCache sharedCache] missingPicture]]){
        
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            NSURL *imageURL = thumbnail.imageURL;
            
            NSString* title = thumbnail.title;
            if (!imageURL){
                if([self.filterVideos[indexPath.section] isKindOfClass:[MYUMovieItem class]]){
                    imageURL = [[MYUWebOperator sharedWebOperator] urlForMovieImage:(int)((MYUMovieItem*)self.filterVideos[indexPath.section]).movieID];
                }else{
                    imageURL = [[MYUWebOperator sharedWebOperator] urlForTVShowImage:(int)((MYUTVShowItem*)self.filterVideos[indexPath.section]).tvShowID];
                }
            }
            UIImage *image  = nil;
            if (imageURL){
                image = [MYUThumbailFactory imageWithTitle:title andURL:imageURL];
            }
           
            if(image) {
                [[MYUCache sharedCache] addImage:image forName:thumbnail.title];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
                });
            }
            
        }];
        operation.queuePriority = operation.queuePriority = NSOperationQueuePriorityVeryHigh ;
        [self.thumbnailQueue addOperation:operation];
    }
    return videoCell;
}


@end
