//
//  MYUReachability.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/13.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//


@interface MYUReachability : NSObject
@property (readwrite) BOOL wifiRechabilityStatus;
@property (readwrite) BOOL cellularRechabilityStatus;

+(MYUReachability*) sharedInstance ;
+(void) createSharedInstance;
+(void) checkForReachability;
@end
