//
//  Images+CoreDataClass.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/31.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Images : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Images+CoreDataProperties.h"
