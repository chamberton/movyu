//
//  MYUMovieItem+Cast.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/11.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUMovieItem+Cast.h"

@implementation  MYUMovieItem(MYUMovieItem_Cast)
-(void) addCastDetails:(NSDictionary*) castDisctionary {
   
    self.actors = [NSMutableSet set];
    self.directors = [NSMutableSet set];
    
    if (castDisctionary!=nil){
        
        NSArray* cast =[castDisctionary objectForKey:@"cast"];
        NSArray* crew =[castDisctionary objectForKey:@"crew"];
        
        NSMutableString* actors = [NSMutableString string];
        
        [NSMutableString string];
        NSMutableString* directors = [NSMutableString string];
        for(NSDictionary* item in cast){
            NSString* str = [item valueForKey:@"name"];
            if ([item valueForKey:@"character"]!=nil && ![[item valueForKey:@"charactrer"] isKindOfClass:[NSNull class]] ){
                actors =(actors.length==0)?[NSMutableString stringWithFormat:@"%@",str]:[NSMutableString stringWithFormat:@"%@,%@",actors,str];
                [self.actors addObject:str];
            }
        }
        for(NSDictionary* item in crew){
            NSString* str = [item valueForKey:@"name"];
            
            NSString* department= [item valueForKey:@"department"];
            if (department!=nil && ![department isKindOfClass:[NSNull class]]  && [department isEqualToString:@"Directing"]){
                directors =(directors.length==0)?[NSMutableString stringWithFormat:@"%@",str]:[NSMutableString stringWithFormat:@"%@,%@",directors,str];
                [self.directors addObject:str];
                
            }
        }
        self.combinedActors = actors;
        self.combinedDirectors = directors;
        
        
    }
   

}
    
@end
