//
//  Images+CoreDataProperties.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/31.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Images+CoreDataProperties.h"

@implementation Images (CoreDataProperties)

+ (NSFetchRequest<Images *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Images"];
}
-(void) addImageToArray:array{
    UIImage* image =[UIImage imageWithData:self.compressedRawData];
    if (image==nil)
       image= [UIImage imageWithData:self.rawData];
    [array addObject:image];
}
@dynamic compressedRawData;
@dynamic movieTitle;
@dynamic rawData;

@end
