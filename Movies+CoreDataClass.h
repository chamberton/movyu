//
//  Movies+CoreDataClass.h
//  MovYU
//
//  Created by Serge Mbamba on 2017/04/26.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Searches;

NS_ASSUME_NONNULL_BEGIN

@interface Movies : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Movies+CoreDataProperties.h"
