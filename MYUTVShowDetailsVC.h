//
//  MYUTVShowDetailsVC.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MYUTVShowItem.h"

@interface MYUTVShowDetailsVC : UIViewController<UITextViewDelegate, UIScrollViewDelegate>
@property (retain, nonatomic,readwrite) MYUTVShowItem *tvShowItem;
@property (weak, nonatomic,readwrite) IBOutlet UITextView *synopsisTextView;
@property BOOL hiddenSynopsisNavigationButtons;
@end
