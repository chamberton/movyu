//
//  MUYVideoCollectionViewLayout.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//


@interface MYUVideoCollectionViewLayout : UICollectionViewFlowLayout

@property (nonatomic,readwrite) UIEdgeInsets itemInsets;
@property (nonatomic,readwrite) CGSize itemSize;
@property (nonatomic) CGFloat verticalSpacing;
@property (nonatomic) CGFloat horizontalSpacing;
@property (nonatomic) NSInteger numberOfColumns;
@property (nonatomic) CGFloat titleHeight;
@property (nonatomic,readwrite) NSMutableArray *transforms;
@property (readwrite) BOOL verticalScroller;

- (instancetype)initWithVerticalScrollDirection:(BOOL) scrollVertically;
- (void)rotate:(UIInterfaceOrientation)orientation;
@end
