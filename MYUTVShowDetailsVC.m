//
//  MYUTVShowDetailsVC.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUTVShowDetailsVC.h"
#import "MYUWebOperator.h"
#import "MYUUtilities.h"
#import "MYUCache.h"
#import "MYUStringFactory.h"
#import "TVShowItem+Cast.h"
#import "MYUThumbailFactory.h"
#import "iToast.h"
#import "MYUTrailerTableViewController.h"
@import GoogleMobileAds;

@interface MYUTVShowDetailsVC()
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@property (weak, nonatomic) IBOutlet UITextView *productionCompaniesTextView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *originCountries;
@property (nonatomic,weak) UIImage* image;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *orignalLanguage;
@property (weak, nonatomic) IBOutlet UILabel *ratingAverage;
@property (weak, nonatomic) IBOutlet UILabel *popularity;

@property (weak, nonatomic) IBOutlet UITextView *genreTextView;
@property (weak, nonatomic) IBOutlet UITextView *castActorsTextView;
@property (weak, nonatomic) IBOutlet UITextField *dateDayTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateMonthTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateYearTextField;
@property (weak, nonatomic) IBOutlet UILabel *directorsLabel;
@property (nonatomic, strong)        NSOperationQueue *backgroundImageQueue;
@property (nonatomic)                NSUInteger seasonCount;
@property (weak, nonatomic) IBOutlet UITextField *seasonTextField;
@property (weak, nonatomic) IBOutlet UITextField *episodesTextField;
@property (weak, nonatomic) IBOutlet UITextView *languages;
@property (weak, nonatomic) IBOutlet UITextView *creatorsTextView;
@property (nonatomic, strong)        MBProgressHUD *hud;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidth;
@property (weak, nonatomic) IBOutlet UIButton *showTrailerButton;
@property (nonatomic, strong) NSArray* trailers;
@property (weak, nonatomic) IBOutlet UILabel *tvShowInfoTitle;
@property (weak, nonatomic) IBOutlet UILabel *starringLabel;
@property (weak, nonatomic) IBOutlet UILabel *synopsisLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *creatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *popularityLabel;
@property (weak, nonatomic) IBOutlet UILabel *voteAverageLabel;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UILabel *originLanguageLabel;
@property (weak, nonatomic) IBOutlet UILabel *productionCompanies;
@property CGFloat  scrollingIncrement;
@property int      scrollinPosition;
@property CGFloat  requireHeight ;
@property CGFloat  actualHeight  ;

@end
@implementation MYUTVShowDetailsVC
- (void)setupLocalizedTitles {
    self.starringLabel.text = NSLocalizedString(@"Starring", @"Starring");
    self.synopsisLabel.text = NSLocalizedString(@"Synopsis", @"Synopsis");
    self.languageLabel.text = NSLocalizedString(@"Languages", @"Languages");
    self.productionCompanies.text = NSLocalizedString(@"Production Companies", @"Production Companies");
    self.genreLabel.text = NSLocalizedString(@"Genre", @"Genre");
    self.creatorLabel.text = NSLocalizedString(@"Creators", @"");
    self.popularityLabel.text = NSLocalizedString(@"Popularity", @"Popularity");
    self.voteAverageLabel.text = NSLocalizedString(@"Vote Average", @"Vote Average");
    self.originLanguageLabel.text = NSLocalizedString(@"Original Language", @"Original Language");
    self.tvShowInfoTitle.text = NSLocalizedString(@"TV show info", @"");
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateNormal];
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateDisabled];
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateSelected];
    [self.showTrailerButton setTitle:NSLocalizedString(@"Watch trailers", @"Watch trailers") forState:UIControlStateHighlighted];
}
- (void)viewDidLoad {
    _hiddenSynopsisNavigationButtons = NO;
    [super viewDidLoad];
    [self setupLocalizedTitles];
    self.showTrailerButton.enabled = NO;
    self.showTrailerButton.alpha  = DISABLED_BUTTON_ALPHA;
    
    [[MYUWebOperator sharedWebOperator] trailerForTVShowWithID:self.tvShowItem.tvShowID :^(NSArray *arrayOfTrailers) {
        if (arrayOfTrailers.count>0){
            self.trailers = arrayOfTrailers;
            self.showTrailerButton.enabled = YES;
            self.showTrailerButton.alpha  = ENABLED_BUTTON_ALPHA;
        }
    }];

    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    self.bannerView.adUnitID = @"ca-app-pub-3836110455040592/5443794461";
    self.bannerView.rootViewController = self;
    self.scrollView.frame =CGRectMake( self.view.frame.origin.x, self.view.frame.origin.y, [UIScreen mainScreen].bounds.size.width
                                      , self.view.frame.size.height) ;
    [self clearFields];
    _seasonCount = 0;
    self.backgroundImageQueue = [[NSOperationQueue alloc] init];
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.label.text = NSLocalizedString(@"Loading", @"HUD loading title");
    _hud.detailsLabel.text = NSLocalizedString(@"Parsing data", @"HUD title");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateGenreTextField) name:kGenreIDNotification object:nil];
    
    if (_tvShowItem==nil){
        _image =[[MYUCache sharedCache] missingPicture];
    }else{
        _image=_tvShowItem.videoThumbnail.poster;
    }
    
    if(_image==nil){
        
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            
            UIImage *image =  [MYUThumbailFactory imageWithTitle:_tvShowItem.videoThumbnail.title andURL:_tvShowItem.videoThumbnail.imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                // display the item if is currently on the screen
                _imageView.image = image;
                self.backgroundView.image =image;
            });
        }];
        operation.queuePriority =  NSOperationQueuePriorityVeryHigh ;
        [self.backgroundImageQueue addOperation:operation];
    }else{
        _imageView.image = _image;
        if([[MYUCache sharedCache] missingPicture] !=_image)
            self.backgroundView.image =_image;
    }
    
    _imageView.layer.masksToBounds = YES;
    _imageView.layer.borderColor = [UIColor blackColor].CGColor;
    _imageView.layer.borderWidth =  8;
    _imageView.layer.cornerRadius = 5;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.opaque = NO;
    self.scrollView.opaque = NO;
    self.backgroundView.alpha = 0.25;
    self.castActorsTextView.textColor = [UIColor redColor];
    self.directorsLabel.textColor = [UIColor redColor];
    self.castActorsTextView.text    =NSLocalizedString( @"NOT RETRIEVED",@"NOT RETRIEVED");
    self.directorsLabel.text        =NSLocalizedString( @"NOT RETRIEVED",@"NOT RETRIEVED");    [[MYUWebOperator sharedWebOperator] addObserver:self forKeyPath:TV_SHOW_DETAILS_OBTAINED options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    __weak MYUWebOperator* operatorForast = [MYUWebOperator sharedWebOperator];
    [operatorForast addObserver:self forKeyPath:TV_SHOW_CAST_OBTAINED options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    NSBlockOperation *castOperation = [NSBlockOperation blockOperationWithBlock:^{
        [operatorForast castForTVWithID:_tvShowItem.tvShowID];
        
    }];
    castOperation.queuePriority =  NSOperationQueuePriorityVeryHigh ;
    __weak MYUWebOperator* operatorForDetails = [MYUWebOperator sharedWebOperator];
    
    NSBlockOperation *detailsOperation = [NSBlockOperation blockOperationWithBlock:^{
        [operatorForDetails detailsForTVWithID:_tvShowItem.tvShowID];
        
    }];
    detailsOperation.queuePriority =  NSOperationQueuePriorityNormal ;
    [self.backgroundImageQueue addOperation:castOperation];
    [self.backgroundImageQueue addOperation:detailsOperation];
    
    _orignalLanguage.text=_tvShowItem.originalLanguage;
    
    _ratingAverage.text =   [MYUUtilities formatStringWithDecimalOutPut: _tvShowItem.voteAverage numberOfDecimalPoint:3];
    _popularity.text    =   [MYUUtilities formatStringWithDecimalOutPut: _tvShowItem.popularity numberOfDecimalPoint:3];
    
    _genreTextView.text = @"" ;
    
    _synopsisTextView.text =_tvShowItem.synopsis;
    
    [self updateGenreTextField];
    if (_tvShowItem.airDate){
        NSInteger month = [_tvShowItem.releaseDate month];
        if (month>0 && month <12){
        NSString *monthName = [[[NSDateFormatter alloc] init] monthSymbols][month-1];
        _dateDayTextField.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_tvShowItem.airDate day]];
        _dateMonthTextField.text = [NSString stringWithFormat:@"%@",monthName];
        _dateYearTextField.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_tvShowItem.airDate year]];
        }
    }
    _scrollingIncrement=0;
    _synopsisTextView.delegate = self;
    
}
-(void) updateGenreTextField{
    NSMutableString* concatenatedGenres =[NSMutableString string];
    for(NSNumber* item in _tvShowItem.genreIDS){
        concatenatedGenres =(concatenatedGenres.length==0)?[NSMutableString stringWithFormat:@"%@",item]:[NSMutableString stringWithFormat:@"%@,%@",concatenatedGenres,item];
    }
    if (concatenatedGenres.length>0){
        self.genreTextView.textColor = [UIColor blackColor];
        self.genreTextView.text = concatenatedGenres;
    }
    
}

- (IBAction)watchTrailerButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"ShowTrailerList" sender:self.tvShowItem];
}



- (IBAction)scrollDown:(id)sender {
    
    if(_scrollinPosition<(_requireHeight - _actualHeight))
    {
        _scrollinPosition+=_scrollingIncrement;
        [_synopsisTextView setContentOffset:CGPointMake(0, _scrollinPosition)];
    }
    
}
- (IBAction)scrollUp:(id)sender {
    if(_scrollinPosition>_scrollingIncrement)
    {
        _scrollinPosition-=_scrollingIncrement;
        [_synopsisTextView setContentOffset:CGPointMake(0, _scrollinPosition)];
    }else{
        [_synopsisTextView setContentOffset:CGPointZero animated:NO];
    }
}

-(BOOL) shouldShowMore{
    CGFloat fixedWidth = _synopsisTextView.frame.size.width;
    _actualHeight= _synopsisTextView.frame.size.height;
    
    CGSize newSize = [_synopsisTextView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    if(newSize.height>_actualHeight){
        _requireHeight = newSize.height;
        _scrollingIncrement = _actualHeight/2+0.1;
        _hiddenSynopsisNavigationButtons = NO;
        return YES;
    }
    _hiddenSynopsisNavigationButtons = YES;
    _upButton.hidden= YES;
    _downButton.hidden = YES;
    return NO;
}

- (void)viewDidLayoutSubviews
{
    self.contentViewWidth.constant = [UIScreen mainScreen].bounds.size.width;
    [self.view layoutSubviews];
    
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_castActorsTextView setContentOffset:CGPointZero animated:NO];
    [_synopsisTextView setContentOffset:CGPointZero animated:NO];
    
    [self shouldShowMore];
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [self.scrollView resignFirstResponder];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGPoint offset = _synopsisTextView.contentOffset;
    _scrollinPosition = offset.y;
    [self.scrollView becomeFirstResponder];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if (!keyPath  || keyPath.length<1){
        return;
    }
    if([keyPath isEqualToString:TV_SHOW_CAST_OBTAINED]){
        
        if (_castActorsTextView.text.length>0){
            _castActorsTextView.textColor =[UIColor blackColor];
        }
        NSDictionary* castDisctionary = [MYUWebOperator sharedWebOperator].tvShowCast;
        
        if (castDisctionary!=nil){
            
            [self.tvShowItem addCastDetails:castDisctionary];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.castActorsTextView.text = self.tvShowItem.combinedActors;
            });
        }
        
        @try{
            [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:TV_SHOW_CAST_OBTAINED];
            [MYUWebOperator sharedWebOperator].hasLoadedTVShowCast = NO;
        }@catch(NSException* anException){
            NSLog(@"%@",anException.description);
        }
        
    }
    
    else if ([keyPath isEqualToString:TV_SHOW_DETAILS_OBTAINED]){
        NSDictionary* detailsDictionary = [MYUWebOperator sharedWebOperator].tvShowDetails;
        if(detailsDictionary!=nil){
            
            NSArray* creatorsArray =[detailsDictionary valueForKey:@"created_by"];
            NSString* creators = [MYUStringFactory createConcatenatedStringFromDictionaryEntries:creatorsArray withUniqueKey:@"name"];
            NSNumber* episodes =[detailsDictionary valueForKey:@"number_of_episodes"];
            NSNumber* seasons =[detailsDictionary valueForKey:@"number_of_seasons"];
            NSArray* productionCompaniesArray =[detailsDictionary objectForKey:@"production_companies"];
            
            NSString* productionCompanies =[MYUStringFactory createConcatenatedStringFromDictionaryEntries:productionCompaniesArray withUniqueKey:@"name"];  [NSMutableString string];
            
            NSArray* countryArray  =[detailsDictionary valueForKey:@"origin_country"];
            NSArray* languagesArray =[detailsDictionary valueForKey:@"languages"];
            
            NSString* originCountries = [MYUStringFactory concatenatedFullCountryNamesCodeArray:countryArray countLimit:1];
            NSString* languages = [MYUStringFactory concatenatedFullLanguageNamesCodeArray:languagesArray];
            _tvShowItem.seasons =[NSMutableArray arrayWithArray:[detailsDictionary objectForKey:@"seasons"]];
            for (NSDictionary* item in _tvShowItem.seasons){
                if ([[item valueForKey:@"season_number"] intValue]==0){
                    [_tvShowItem.seasons removeObject:item];
                    break;
                }
            }
            _seasonCount =(_tvShowItem.seasons==nil)?0:_tvShowItem.seasons.count;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.creatorsTextView.text = creators;
                self.episodesTextField.text= (episodes==nil|| [episodes isMemberOfClass:[NSNull class]])? @"":[NSString stringWithFormat:@"%d",[episodes intValue]];
                self.seasonTextField.text= (seasons==nil|| [seasons isMemberOfClass:[NSNull class]])? @"":[NSString stringWithFormat:@"%lu",(unsigned long)_seasonCount];
                self.directorsLabel.hidden = YES;
                self.productionCompaniesTextView.text = productionCompanies;
                self.originCountries.text =originCountries;
                self.languages.text = languages;
                
            });
        }
        
        @try{
            [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:TV_SHOW_DETAILS_OBTAINED];
            [MYUWebOperator sharedWebOperator].hasLoadedTVShowCast = NO;
        }@catch(NSException* anException){
            NSLog(@"%@",anException.description);
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.hud hideAnimated:YES];
    });
    [self.bannerView loadRequest:[GADRequest request]];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:NO];
    [self.backgroundImageQueue cancelAllOperations];
    @try{
        [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:TV_SHOW_DETAILS_OBTAINED];
        [[MYUWebOperator sharedWebOperator] removeObserver:self forKeyPath:TV_SHOW_CAST_OBTAINED];
    }@catch(NSException* anException){
        NSLog(@"%@",anException.description);
    }
   
    [MYUWebOperator sharedWebOperator].hasLoadedTVShowCast = NO;
    [MYUWebOperator sharedWebOperator].hasLoadedTVShowCast = NO;
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showSeasonDetails"]) {
        MYUTVShowDetailsVC *tvShowsVC  =segue.destinationViewController;
        MYUTVShowItem *tvShow = (MYUTVShowItem*) sender;
        tvShow.videoThumbnail.poster= [[MYUCache sharedCache] imageForMovieTitle:tvShow.videoThumbnail.title];
        if (  tvShow.videoThumbnail.poster==nil){
            MYUThumbnail *thumbnail = [MYUThumbailFactory  thumnailWithImageURL:tvShow.videoThumbnail.imageURL andTitle:tvShow.videoThumbnail.title];
            tvShow.videoThumbnail = thumbnail;
        }
        
        [tvShowsVC setTvShowItem:tvShow];
        tvShowsVC.title =tvShow.videoThumbnail.title;
        [tvShowsVC setTitle:[NSString stringWithFormat:@"%@ %@",tvShow.videoThumbnail.title,@"seasons"]];
        
    }
    else if ([segue.identifier isEqualToString:@"ShowTrailerList"]){
        MYUTrailerTableViewController* destinationVC = segue.destinationViewController;
        destinationVC.videoItem = sender;
        destinationVC.trailerList = self.trailers;
        destinationVC.title =[NSString stringWithFormat:@"%@ (%@)",self.tvShowItem.title ,NSLocalizedString(@"Trailers", @"Trailers")];
    }
}

-(void) clearFields{
    _orignalLanguage.text=@"";
    _ratingAverage.text =@"";
    _popularity.text =@"";
    _synopsisTextView.text =@"";
    _castActorsTextView.text=@"";
    _languages.text=@"";
    _creatorsTextView.text=@"";
    _productionCompaniesTextView.text =@"";
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [_synopsisTextView setContentOffset:CGPointZero animated:NO];
    [self shouldShowMore];
}

- (IBAction)showSeasonDetails:(id)sender {
    
    
    if (_seasonCount>0){
        
        [self performSegueWithIdentifier:@"showSeasonDetails" sender:_tvShowItem ];
    }else{
        
        NSString* toastMessage = NSLocalizedString(@"NoSeasonInfo", @"NoSeasonInfo");
        [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
        
    }
}

@end
