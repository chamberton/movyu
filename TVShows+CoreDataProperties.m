//
//  TVShows+CoreDataProperties.m
//  MovYU
//
//  Created by Serge Mbamba on 2017/04/26.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "TVShows+CoreDataProperties.h"

@implementation TVShows (CoreDataProperties)

+ (NSFetchRequest<TVShows *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TVShows"];
}

@dynamic actors;
@dynamic creators;
@dynamic genreIDs;
@dynamic languages;
@dynamic originalCountry;
@dynamic originalLanguage;
@dynamic popularity;
@dynamic posterCompressed;
@dynamic productionCompanies;
@dynamic rating;
@dynamic releasDate;
@dynamic synopsis;
@dynamic title;
@dynamic tvShowID;
@dynamic relationship;
@dynamic searchCall;
@dynamic imageURL;

@end
