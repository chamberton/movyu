//
//  Movies+CoreDataProperties.h
//  MovYU
//
//  Created by Serge Mbamba on 2017/04/26.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "Movies+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Movies (CoreDataProperties)

+ (NSFetchRequest<Movies *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *actors;
@property (nullable, nonatomic, copy) NSString *directors;
@property (nullable, nonatomic, copy) NSString *genreIDs;
@property (nonatomic) BOOL isAdultOnly;
@property (nonatomic) int64_t movieID;
@property (nullable, nonatomic, copy) NSString *originalLanguage;
@property (nonatomic) float popularity;
@property (nullable, nonatomic, retain) NSData *posterCompressed;
@property (nullable, nonatomic, copy) NSDate *releaseDate;
@property (nullable, nonatomic, copy) NSString *synopsis;
@property (nullable, nonatomic, copy) NSString *title;
@property (nonatomic) float voteAverage;
@property (nullable, nonatomic, retain) Searches *searchCall;
@property (nullable, nonatomic, copy) NSString *imageURL;

@end

NS_ASSUME_NONNULL_END
