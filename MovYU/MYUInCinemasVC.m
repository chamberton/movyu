//
//  FirstViewController.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUInCinemasVC.h"
#import "MYUVideoCollectionViewLayout.h"
#import "MYUVideoTitleReusable.h"
#import "MYUVideoCell.h"
#import "MYUThumbnail.h"
#import "MYUWebOperator.h"
#import "MYUColorAndFontManager.h"
#import "MYUMovieItem.h"
#import "MYUCache.h"
#import "MYUMovieDetailsVC.h"
#import "MYUVideoFactory.h"
#import "MYUThumbailFactory.h"
#import "iToast.h"
#import "MBProgressHUD.h"
#import "MYUCoreDataConnector.h"

@interface MYUInCinemasVC ()

@property (weak, nonatomic)  IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic) BOOL   loadedCinemaMovies;
@property (nonatomic) BOOL   loadedMostPopularMovies;
@property (nonatomic,strong) MBProgressHUD * hud;
@property (strong) NSOperationQueue *downlaodingQueue;
@end

@implementation MYUInCinemasVC
@synthesize hud = _hud;
#pragma mark - Lifecycle

- (void)viewDidLoad {
    self.containsMovie = YES;
    self.downlaodingQueue = [[NSOperationQueue alloc] init];
    [super viewDidLoad];
    _loadedCinemaMovies =NO;
    _loadedMostPopularMovies=NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [MYUColorAndFontManager backgroundColorForUIElementWithTag:(int)self.collectionView.tag];
    
    [self.collectionView setCollectionViewLayout:self.videosLayout];
    
    
    [self.collectionView registerClass:[MYUVideoCell class]
            forCellWithReuseIdentifier:MYUMovieCellIdentifier];
    
    [self.collectionView registerClass:[MYUVideoTitleReusable class]
            forSupplementaryViewOfKind:ThumbnailCellTitle
                   withReuseIdentifier:ThumbnailCellTitle];
    
    
    [self registerForNotifications];
    
    [self startAutomaticUpdates];
    self.searchBar.userInteractionEnabled = YES;
    self.searchBar.delegate = self;
    self.searchBar.showsCancelButton = YES;
    self.lastSearchTextLength = 0;
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isVisible = YES;
}


- (void) viewWillDisappear:(BOOL)animated {
    self.isVisible = NO;
    [super viewWillDisappear:animated];
   
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

#pragma  mark - Notification handler

-(void) handleNotifications:(NSNotification*) note{
    
    NSString* noteName = [note name];
    
    BOOL popularMovies = [noteName isEqualToString:MOST_POPULAR_MOVIES_UPDATES];
    BOOL cinemaMovies = [noteName isEqualToString:MOVIE_IN_CINEMA_UPDATES];
    
    if (popularMovies || cinemaMovies){
        
        MYUWebOperator* operator = [MYUWebOperator sharedWebOperator];
        if(operator.moviesInCinemaError==nil || operator.mostPopularError==nil){
            if( operator.retrievedFromDBMovie==false){
                
                NSDictionary* newMovies = [[MYUWebOperator sharedWebOperator] valueForKey:noteName];
                [self addMovies:newMovies];
                
                
                if( self.videos.count>=MAX_NUMBER_OF_VIDEOS){ // limit the number of loaded  items
                    if(popularMovies){
                        [MYUCoreDataConnector insertSearchResult:CINEMAALL withActors:nil withGenres:nil isMovie:false ratingLoweBoud:0 ratingUpperBound:0 searchID:CINEMAALL title:nil foundVideos:self.videos ];
                    }else{
                        [MYUCoreDataConnector insertSearchResult:CINEMAMOSTPOPULAR withActors:nil withGenres:nil isMovie:false ratingLoweBoud:0 ratingUpperBound:0 searchID:CINEMAMOSTPOPULAR title:nil foundVideos:self.videos ];
                    }
                    [[NSNotificationCenter defaultCenter] removeObserver:self]; // ignore subsequent notifications
                    
                    [self registerForNotifications]; // register for all other types of notifications
                    
                    if(!_loadedMostPopularMovies)
                        _loadedMostPopularMovies = popularMovies;
                    if(!_loadedCinemaMovies)
                        _loadedCinemaMovies = cinemaMovies;
                    while (self.videos.count>MAX_NUMBER_OF_VIDEOS) {
                        MYUMovieItem* item = [self.videos lastObject];
                        [self.videosNames removeObject:item.videoTitle];
                    }
                    // update the collection view
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectionView reloadData];
                    });
                }
            }
            else{
                
                [self setFromDB:noteName];
                [[NSNotificationCenter defaultCenter] removeObserver:self];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionView reloadData];
                });
            }
        }
    }
}


-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MYUMovieItem *movie =  self.filteredVideos[indexPath.section];
    [self performSegueWithIdentifier:@"ShowMovieDetail" sender:movie ];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MYUVideoCell *videoCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:MYUMovieCellIdentifier
                                              forIndexPath:indexPath];
    
    MYUMovieItem *movie =  self.filteredVideos[indexPath.section];
    MYUThumbnail *thumbnail = movie.videoThumbnail;
    videoCell.imageView.image = [[MYUCache sharedCache] imageForMovieTitle:thumbnail.title];
    
    if (! videoCell.imageView.image || [videoCell.imageView.image isEqual:[[MYUCache sharedCache] missingPicture]]){
     
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            
            NSURL *imageURL = thumbnail.imageURL;
            if (!imageURL){
                imageURL = [[MYUWebOperator sharedWebOperator] urlForMovieImage:(int)movie.movieID];
            }
            NSString* title = thumbnail.title;
            UIImage *image = nil;
            if (imageURL){
                image = [MYUThumbailFactory imageWithTitle:title andURL:imageURL];
            }
            if(image) {
                [[MYUCache sharedCache] addImage:image forName:thumbnail.title];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
                });
            }
        }];
        operation.queuePriority = NSOperationQueuePriorityVeryHigh ;
        [self.downlaodingQueue addOperation:operation];
        
    }
    self.showing = NO;
    [self.hidingTimer invalidate];
    if (self.firstToShow){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.hud hideAnimated:YES];
            self.firstToShow = false;
        });
        
    }
    return videoCell;
}



-(void) actualUpdate:(BOOL) forceUpdate{
    
    // reset the collection data source
    
    self.videos =[NSMutableArray array];
    self.videosNames =[NSMutableSet set];
    switch (_segmentedControl.selectedSegmentIndex) {
        case All:
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleNotifications:)
                                                         name:MOVIE_IN_CINEMA_UPDATES
                                                       object:nil];
            [[MYUWebOperator sharedWebOperator] refreshMoviesInCinema] ;
            
            
            break;
        case MostPopularMovies:
            // Check whether the last update were acquired more than an hour ago
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleNotifications:)
                                                         name:MOST_POPULAR_MOVIES_UPDATES
                                                       object:nil];
            [[MYUWebOperator sharedWebOperator] refreshMoviesMostPopularInCinema] ;
            
            break;
        default:
            break;
    }
    [self registerForNotifications];
    
}

-(void) addMovies:(NSDictionary*) dictionary{
    
    NSArray* movies = [dictionary objectForKey:RESULTS];
    if([movies isKindOfClass:[NSArray class]]){
        for (NSDictionary* item in movies){
            if(self.videos.count==MAX_NUMBER_OF_VIDEOS)
                break;
            
            MYUMovieItem* movieItem =[MYUVideoFactory createFromMovieDictionay:item];
            if ([self.videosNames containsObject:[movies valueForKey:ORIGINAL_TITLE]]
                || [self.videosNames containsObject:movieItem.videoThumbnail.title]) {// if the image for the movie already exist
                
                continue;
            }
            else if (movieItem){
                [self.videosNames addObject:movieItem.videoThumbnail.title];
                [ self.videos addObject:movieItem];
                
                
            }
        }
    }
    
}


-(void) requestUpate:(BOOL) forceUpdate{
    
    self.showing = YES;
    [super requestUpate:forceUpdate];
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        [self.hud hideAnimated:NO];
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        self.hud.label.text = NSLocalizedString(@"Loading", @"HUD loading title");
        self.hud.detailsLabel.text = NSLocalizedString(@"Parsing data", @"HUD title");
        [self actualUpdate:forceUpdate];
        
    });
    
}

#pragma mark - Button actions

- (IBAction)switchedSegment:(id)sender {
    [self.searchBar resignFirstResponder];
    self.searchBar.text = @"";
    [self.hud hideAnimated:NO];
    [self reset];
    if(sender==_segmentedControl){ // validate the sender is the correct one
        [self requestUpate:NO];
        [self.collectionView reloadData];
    }
}

- (IBAction)refresh:(id)sender {
    if([MYUWebOperator sharedWebOperator].connectedToInternet){
        [self requestUpate:YES];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString* toastMessage = NSLocalizedString(@"Offline", @"Offline");
            [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] show];
        });
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ShowMovieDetail"]) {
        
        UINavigationController *navigationController = segue.destinationViewController;
        MYUMovieDetailsVC *moviesVC = [navigationController viewControllers][0];
        MYUMovieItem *movie = (MYUMovieItem*) sender;
        
        movie.videoThumbnail.poster= [[MYUCache sharedCache] imageForMovieTitle:movie.videoThumbnail.title];
        if (  movie.videoThumbnail.poster==nil){
            MYUThumbnail *thumbnail = [MYUThumbailFactory  thumnailWithImageURL:movie.videoThumbnail.imageURL andTitle:movie.videoThumbnail.title];
            movie.videoThumbnail = thumbnail;
        }
        
        // set the movie item
        [moviesVC setMovieItem:movie];
        // set the view controller title
        moviesVC.title =movie.videoThumbnail.title;
        [navigationController setTitle:[NSString stringWithFormat:@"%@",movie.videoThumbnail.title]];
        
    }
}

-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if ( self.updateTimer.isValid){
        [self.updateTimer invalidate];
    }
    [self.updateThread cancel];
}


#pragma mark - UI search bar delegate

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [searchText stringByTrimmingCharactersInSet:charSet];
    if (![trimmedString isEqualToString:@""]) {
        self.searchText = searchText;
        self.lastSearchTextLength = (int)searchBar.text.length;
        [self.collectionView reloadData];
    }else{
        if(self.lastSearchTextLength>0){
            self.lastSearchTextLength = (int)searchBar.text.length;
            self.searchText = @"";
            [self.collectionView reloadData];
        }
    }
}


@end




