//
//  AppDelegate.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUAppDelegate.h"
#import "MYUCache.h"
#import "MYUReachability.h"
@import GoogleMobileAds;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-3836110455040592~3967061261"];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
   
    
    if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]) {
        if ([[UIDevice currentDevice] isMultitaskingSupported]) { //Check if device supports mulitasking
            UIApplication *application = [UIApplication sharedApplication]; //Get the shared application instance
            
            
            
            _savingPictureTast = [application beginBackgroundTaskWithExpirationHandler: ^ {
                [application endBackgroundTask: _savingPictureTast];
                _savingPictureTast = UIBackgroundTaskInvalid;             }];
            
            //Asychronous execution
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                
                NSLog(@"\n\nRunning in the background!\n\n");
                [[MYUCache sharedCache] saveAll];
                
                [application endBackgroundTask: _savingPictureTast];
                _savingPictureTast = UIBackgroundTaskInvalid; // invalidate
            });
        }
    }
      [[MYUCache sharedCache] stopClearingThread];
  
}

-(void) applicationDidReceiveMemoryWarning:(UIApplication *)application{
    [[MYUCache sharedCache] clearCache:YES];
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    if (_savingPictureTast != UIBackgroundTaskInvalid) {
        [application endBackgroundTask:_savingPictureTast];
        _savingPictureTast = UIBackgroundTaskInvalid;
    }

    [MYUReachability checkForReachability];
     [[MYUCache sharedCache] cancelSaving];
     [[MYUCache sharedCache] startClearingThread];
  
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "isolvtech.com.Pzl" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MYUModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"UDADataModel.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],
                                       NSMigratePersistentStoresAutomaticallyOption,
                                       [NSNumber numberWithBool:YES],
                                       NSInferMappingModelAutomaticallyOption,
                                       nil];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:optionsDictionary error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        DebugLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            DebugLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)aSource
                                      entityMapping:(NSEntityMapping *)mapping
                                            manager:(NSMigrationManager *)migrationManager
                                              error:(NSError **)error {
    NSEntityDescription *aSourceEntityDescription = [aSource entity];
    NSString *aSourceName = [aSourceEntityDescription valueForKey:@"name"];
    
    NSManagedObjectContext *destinationMOC = [migrationManager destinationContext];
    NSManagedObject *destEnvironment;
    NSString *destEntityName = [mapping destinationEntityName];
    
//    if ([aSourceName isEqualToString:kEnvironment])
//    {
//        destEnvironment = [NSEntityDescription
//                           insertNewObjectForEntityForName:destEntityName
//                           inManagedObjectContext:destinationMOC];
//        
//        // attribute feedID
//        NSNumber *sourceFeedID = [aSource valueForKey:kFeedID];
//        if (!sourceFeedID)
//        {
//            // Defensive programming.
//            // In the source model version, feedID was required to have a value
//            // so excecution should never get here.
//            [destEnvironment setValue:[NSNumber numberWithInteger:0] forKey:kFeedID];
//        }
//        else
//        {
//            NSInteger sourceFeedIDInteger = [sourceFeedID intValue];
//            if (sourceFeedIDInteger < 0)
//            {
//                // To correct previous negative feedIDs, add 2^16 = 65536
//                NSInteger kInt16RolloverOffset = 65536;
//                NSInteger destFeedIDInteger = (sourceFeedIDInteger + kInt16RolloverOffset);
//                NSNumber *destFeedID = [NSNumber numberWithInteger:destFeedIDInteger];
//                [destEnvironment setValue:destFeedID forKey:kFeedID];
//                
//            } else
//            {
//                // attribute feedID previous value is not negative so use it as is
//                [destEnvironment setValue:sourceFeedID forKey:kFeedID];
//            }
//        }
//        
//        // attribute title (don't change this attribute)
//        NSString *sourceTitle = [aSource valueForKey:kTitle];
//        if (!sourceTitle)
//        {
//            // no previous value, set blank
//            [destEnvironment setValue:@"" forKey:kTitle];
//        } else
//        {
//            [destEnvironment setValue:sourceTitle forKey:kTitle];
//        }
//        
//        [migrationManager associateSourceInstance:aSource
//                          withDestinationInstance:destEnvironment
//                                 forEntityMapping:mapping];
//        
//        return YES;
//    } else
//    {
//        // don't remap any other entities
//        return NO;
//    }
    
    return YES;
}
@end
