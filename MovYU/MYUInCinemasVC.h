//
//  FirstViewController.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//


#import "MYUAbstractVideoVC.h"

@interface MYUInCinemasVC : MYUAbstractVideoVC

-(void) reset;
-(void) requestUpate:(BOOL) forceUpdate;

@end

