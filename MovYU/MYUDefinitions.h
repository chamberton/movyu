//
//  MYUDefinitions.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#ifndef MYUDefinitions_h
#define MYUDefinitions_h


#if DEBUG == 0
#define DebugLog(...)
#elif DEBUG == 1
#define DebugLog(...) NSLog(__VA_ARGS__)
#endif

#pragma mark - Macors

#define ONE_HOUR_IN_SECONDS     3600
#define TEN_MINUTES              600
#define TWO_MINUTES              120
#define TEN_SECONDS               10
#define MAXIMUM_NUMBER_OF_PAGES   60
#define MAX_NUMBER_OF_VIDEOS     200
#define CONVENIENT_HEIGHT_FOR_CELL 40
#define ENABLED_BUTTON_ALPHA 1
#define DISABLED_BUTTON_ALPHA 0.3

typedef enum{
    lowest  =5,
    low     =4,
    medium  =3,
    high    =2,
    highest =1
}JPEGCompressionLevel;

typedef enum{
    CINEMAALL,
    CINEMAMOSTPOPULAR,
    TVHSHOWLATEST,
    TVHSHOWTOPRATED,
    TVSHOWAIRINGTODAY
} SpecialSearchID;
#pragma Enumeration
typedef enum _movieIndices{
    All=0,
    MostPopularMovies
} moviesIndices;

typedef enum _TVShowIndices{
    Latest=0,
    TopRated,
    airingToday
} TVShowIndices;

typedef enum : NSInteger {
    NotReachable = 0,
    ReachableViaWiFi,
    ReachableViaWWAN
} NetworkStatus;

typedef enum _deviceType {
    kUnknownPlatform = 0,
    kiPhone1G,
    kiPhone3G,
    kiPhone3GS,
    kiPhone4,
    kiPhone4Verizon,
    kiPhone4S,
    kiPhone5GSM,
    kiPhone5CDMA,
    kiPhone5CGSM,
    kiPhone5CCDMA,
    kiPhone5SGSM,
    kiPhone5SCDMA,
    kiPhone6,
    kiPhone6P,
    kiPhone6S,
    kiPhone6SP,
    kiPodTouch1G,
    kiPodTouch2G,
    kiPodTouch3G,
    kiPodTouch4G,
    kiPodTouch5G,
    kiPad,
    kiPad2Wifi,
    kiPad2GSM,
    kiPad2CMDA,
    kiPad3Wifi,
    kiPad3GSM,
    kiPad3CMDA,
    kiPad4Wifi,
    kiPad4GSM,
    kiPad4CMDA,
    kiPadAirWifi,
    kiPadAirCellular,
    kiPadMiniWifi,
    kiPadMiniGSM,
    kiPadMiniCDMA,
    kiPadMini2GWifi,
    kiPadMini2GCellular,
    kSimulator
} DeviceType;

static NSString * const MYUMovieCellKind;
static NSString * const MYUTVShowsCellKind;
static NSString * const ThumbnailCellTitle = @"ThumbnailCellTitle";
static NSString * const MYUMovieCellIdentifier = @"MYUMovieCellIdentifier";
static NSString * const MYUTVShowCellIdentifier = @"MYUTVShowCellIdentifier";
static NSString * const MYUTVSearchCellIdentifier =@"MYUSearchCelldentifier";
static NSString * const AlbumTitleIdentifier = @"AlbumTitle";
static NSString * const ThumbnailCell = @"ThumbnailCell";
static NSString * const ITEM_SIZE = @"itemsize";
static NSString * const TITLE_HEIGHT =  @"titleHeigh";
static NSString * const ITEM_HEIGHT= @"itemHeight";
static NSString * const NUMER_OF_COLUMN= @"numberOfColumn";
static NSString * const VERTICAL_SPACING= @"verticalSpacing";
static NSString * const MOVIES_INCINEMA=@"Movies In Cinema";
static NSString * const MOST_POPULAR_MOVIES=@"Most Popular Movies";
static NSString * const MOVIES_WITHIN_DATE=@"Movies within dates";
static NSString * const TV_SHOWS_WITHIN_DATE=@"TVShowsWithinDates";
static NSString * const API_URLS=@"API URLs";
static NSString * const API_KEY=@"API App Key";
static NSString * const  MOVIE_TRAILER_KEY=@"TrailerInfoMovie";
static NSString * const   TV_TRAILER_KEY=@"TrailerInfoTV";
#pragma mark -  Date String Format
static NSString * const PRIMARY_DATE_FORMAT  =@"yyyy-MM-dd HH:mm:ss Z";
static NSString * const MINUTe_DATE_FORMAT =@"yyyy-MM-dd HH:mm";
static NSString * const HOUR_DATE_FORMAT   =@"yyyy-MM-dd HH";
static NSString * const DAY_DATE_FORMAT    =@"yyyy-MM-dd";

#pragma mark -  KVC - KVO
static NSString * const MOVIE_IN_CINEMA_UPDATES    =@"moviesInCinema";
static NSString * const MOST_POPULAR_MOVIES_UPDATES=@"mostPopularMovies";
static NSString * const LATEST_TV_SHOWS_UPDATES    =@"latestTVShows";
static NSString * const TOP_RATED_TV_SHOWS_UPDATES   =@"topRatedTVShows";
static NSString * const airing_TODAY_TV_SHOWS_UPDATES=@"airingTodayTVShows";
static NSString * const SEARCH_MOVIES_UPDATES=@"searchMovies";
static NSString * const SEARCH_TV_SHOWS_UPDATES=@"searchTVShows";
static NSString * const MOVITE_CAST=@"movieCast";
static NSString * const MOVIE_CAST_OBTAINED=@"hasLoadedMovieCast";
static NSString * const TV_SHOW_CAST_OBTAINED=@"hasLoadedTVShowCast";
static NSString * const TV_SHOW_DETAILS_OBTAINED=@"hasLoadedTVShowDetails";

#pragma mark -  Plist Strings
static NSString * const POSTER_PATH      =@"poster_path";
static NSString * const POSTER_PREFIX    =@"PosterPath";
static NSString * const infoOutdatedNotificationString=@"OutdateInfo";
static NSString * const MOST_POPULAR_TV_SHOWS=@"Most Polular TV Shows";
static NSString * const AIRING_TODAY_TV_SHOWS=@"Airing Today";
static NSString * const SEARCHING_MOVIES=@"MovieSearch";
static NSString * const SEARCHING_TVs =@"TVSearch";
static NSString * const TOP_RATED_TV_SHOWS=@"Top Rated TV Shows";
static NSString * const LATEST_TV_SHOWS=@"Latest TV Shows";
static NSString * const MOVIE_CAST=@"Movie Cast";
static NSString * const MOVIE_DETAILS=@"Movie Details";
static NSString * const TV_SHOW_DETAILS=@"TV Shows Details";
static NSString * const TV_SHOW_CREDIT=@"TV Shows Credits";
static NSString * const LATEST_MOVIES=@"Latest Movies";
#pragma mark -  API JSON key
static NSString * const BACKDROP_PATH =@"backdrop_path";
static NSString * const RESULTS =@"results";
static NSString * const ORIGINAL_TITLE =@"original_title";
static NSString * const ORIGINAL_NAME=@"original_name";
static NSString * const TOTAL_PAGES =@"total_pages";
static NSString * const MISSING_POSTER=@"Image not found";

static NSString* FILTER_CRIETRIA_ARRAY = @"filterCriteria";
static NSString * const LAST_UPDATE_TIME_MOST_POPULAR_MOVIES=@"lastUpdateTimeForPopular";
static NSString * const LAST_UPDATE_TIME_CINEMA_MOVIES=@"lastUpdateTimeForCinema";
static NSString * const LAST_UPDATE_LATEST_TV_SHOWS=@"lastUpdateForLatestTVShow";
static NSString * const LAST_UPDATE_TIME_AIRING_TODAY_TV_SHOWS=@"lastUpdateForAiringTodayVShow";
static NSString * const LAST_UPDATE_TIME_TOP_RATED_TV_SHOWS=@"lastUpdateForMostTopRatedTVShow";
#pragma mark -  Notifcations names
static NSString * const infoOutdatedNotification = @"kDisplayOutdatedInfoWarning";
static NSString *kReachabilityChangedNotification = @"kNetworkReachabilityChangedNotification";
static NSString *kGenreIDNotification =@"kGenreIDsUpdated";


#endif /* MYUDefinitions_h */
