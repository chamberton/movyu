//
//  Searches+CoreDataProperties.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Searches+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Searches (CoreDataProperties)

+ (NSFetchRequest<Searches *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *actors;
@property (nullable, nonatomic, copy) NSString *genres;
@property (nonatomic) BOOL isMovie;
@property (nonatomic) float ratingLowerBound;
@property (nonatomic) float ratingUpperBound;
@property (nonatomic) int16_t searchID;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) NSSet<Movies *> *foundMovies;
@property (nullable, nonatomic, retain) NSSet<TVShows *> *foundTVShows;

@end

@interface Searches (CoreDataGeneratedAccessors)

- (void)addFoundMoviesObject:(Movies *)value;
- (void)removeFoundMoviesObject:(Movies *)value;
- (void)addFoundMovies:(NSSet<Movies *> *)values;
- (void)removeFoundMovies:(NSSet<Movies *> *)values;

- (void)addFoundTVShowsObject:(TVShows *)value;
- (void)removeFoundTVShowsObject:(TVShows *)value;
- (void)addFoundTVShows:(NSSet<TVShows *> *)values;
- (void)removeFoundTVShows:(NSSet<TVShows *> *)values;

@end

NS_ASSUME_NONNULL_END
