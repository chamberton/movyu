//
//  MYUVideoFactory.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/15.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUMovieItem.h"
#import "MYUTVShowItem.h"

@interface MYUVideoFactory : NSObject

+ (void)addGenre:(NSNumber*)genreID forKey:(NSString*)genreName;
+ (MYUMovieItem*) createFromMovieDictionay:(NSDictionary*) dictionary;
+ (MYUTVShowItem*) createFromTVShowDictionay:(NSDictionary*) dictionary;
+ (NSURL *)urlFrom:(NSDictionary *)dictionary ;
+ (MYUMovieItem*) addCastDetails:(NSDictionary*) castDictionary to:(MYUMovieItem*) originalItem;
@end
