//
//  UIKit.h
//  MovYU
//  Class representing the video cell item
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

@interface MYUVideoCell : UICollectionViewCell

@property (nonatomic, strong, readwrite) UIImageView *imageView;


-(void) willBeRemovedFromCollectionView;
@end
