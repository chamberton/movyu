//
//  TVShowItem+Cast.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/16.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYUTVShowItem.h"
@interface MYUTVShowItem(TVShowItem_Cast)
-(void) addCastDetails:(NSDictionary*) castDisctionary;
@end
