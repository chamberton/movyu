//
//  Seasons+CoreDataProperties.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Seasons+CoreDataProperties.h"

@implementation Seasons (CoreDataProperties)

+ (NSFetchRequest<Seasons *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Seasons"];
}

@dynamic numberOfEpisodes;
@dynamic releaseDate;
@dynamic seasonNumber;
@dynamic parent;

@end
