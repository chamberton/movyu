//
//  NSString+URL.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/09.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_Extended)
- (NSString *)urlencode ;

@end
