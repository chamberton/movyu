//
//  MYUTrailerInfoFactory.m
//  MovYU
//
//  Created by Serge Mbamba on 2017/01/23.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "MYUTrailerInfoFactory.h"

@implementation MYUTrailerInfoFactory

+ (NSArray<MYUTrailerInfo *> *) createFromJson:(NSDictionary *)jsonResponse {
    
    if (!jsonResponse){
        return  nil;
    }
    
    NSMutableArray* array = [NSMutableArray array];
    NSArray* arrayOftrailers = jsonResponse[@"results"];
    
    if (arrayOftrailers && ![arrayOftrailers isKindOfClass:[NSNull class]]){
        
        for (NSDictionary* dic in arrayOftrailers){
            MYUTrailerInfo* info = [[MYUTrailerInfo alloc] initWithDictionary:dic];
            
            if( [info.site caseInsensitiveCompare:@"youtube"] == NSOrderedSame ) {
                [array addObject:info];
            }
        }
    }

    return [array copy];
}
@end
