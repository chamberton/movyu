//
//  MYUFilterVCViewController.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/19.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUFilterVCViewController.h"

@interface MYUFilterVCViewController ()

@end

@implementation MYUFilterVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)removeView:(id)sender {
    [self.view removeFromSuperview];
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
