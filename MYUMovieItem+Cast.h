//
//  MYUMovieItem+Cast.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/11.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYUMovieItem.h"
@interface MYUMovieItem(MYUMovieItem_Cast)
-(void) addCastDetails:(NSDictionary*) castDisctionary;
@end
