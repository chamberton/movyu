////
//  MYUAbstractVC.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/13.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUAbstractVideoVC.h"
#import "MYUVideoCell.h"
#import "MYUAbstractVideoItem.h"
#import "MYUColorAndFontManager.h"
#import "MYUWebOperator.h"
#import "iToast.h"
#import "MYUCache.h"
#import "MYUTVShowItem.h"
#import "MYUMovieItem.h"

@import GoogleMobileAds;

@interface MYUAbstractVideoVC()
@property (weak, nonatomic) GADBannerView *bannerView;

@end

@implementation MYUAbstractVideoVC{
    // Use kGADAdSizeSmartBannerLandscape if your app is running in landscape.
    
}

-(instancetype) init{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)reset{
    [self.thumbnailQueue cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.videos = [NSMutableArray array];
    self.videosNames= [NSMutableSet set];
    self.filteredVideos=[NSArray array];
    self.filteredVideoNames = [NSSet set];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.bannerView loadRequest:[GADRequest request]];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.bannerView.adUnitID = @"ca-app-pub-3836110455040592/5443794461";
    self.bannerView.rootViewController = self;
    self.hasAppearedAlready = NO;
    self.searchText =@"";
    _savingQueue = dispatch_queue_create("ImageSavingQueue", NULL);
    
    _transforms = [NSMutableArray array];
    
    //Create rotations for visual video items
    for(int i=0; i<MAX_NUMBER_OF_VIDEOS; i++){
        CGFloat rotationPercentage  = ((CGFloat)(arc4random() % 220) - 110) * 0.0001f;
        usleep(500);
        CGFloat angle = 2 * M_PI * (1.0f + rotationPercentage);
        CATransform3D transform = CATransform3DMakeRotation(angle, 0.0f, 0.0f, 1.0f);
        [_transforms  addObject:[NSValue valueWithCATransform3D:transform]];
    }
    
    _videosLayout= [[MYUVideoCollectionViewLayout alloc] init];
    self.videosLayout.verticalScroller = YES;
    _videosLayout.transforms = _transforms;
    _firstToShow = false;
    // initalialize containers
    self.videos = [NSMutableArray array];
    self.videosNames= [NSMutableSet set];
    self.thumbnailQueue = [[NSOperationQueue alloc] init];
    self.thumbnailQueue.maxConcurrentOperationCount = 5;
    self.searchBar.userInteractionEnabled = YES;
    self.searchBar.delegate = self;
    self.searchBar.showsCancelButton = YES;
    
}




- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self registerForNotifications];
    self.showing = YES;
    self.topView.backgroundColor = [MYUColorAndFontManager backgroundColorForUIElementWithTag:(int)self.topView.tag];
    UIInterfaceOrientation toInterfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    NSDictionary* dic = [MYUUtilities sizeForVideoItemInterface:toInterfaceOrientation];
    self.videosLayout.numberOfColumns = [(NSNumber*)[dic objectForKey:NUMER_OF_COLUMN]  integerValue];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.showing = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.hud hideAnimated:NO];
    });
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:NO];
    [self.thumbnailQueue cancelAllOperations];
}

-(void) registerForNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopAutomaticUpdates) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopAutomaticUpdates) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startAutomaticUpdates) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(displayOutdatedInfoWarning) name:infoOutdatedNotification object:nil];
}


-(void)displayOutdatedInfoWarning{
    if (self.videos.count>0){// If we are displaying something
        NSString *info = NSLocalizedString(infoOutdatedNotificationString, @"Outdated info");
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Info"
                                      message:info
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alert addAction:ok];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
}




#pragma mark - UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath;
{
    MYUVideoTitleReusable *titleView =
    [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                       withReuseIdentifier:ThumbnailCellTitle
                                              forIndexPath:indexPath];
    
    MYUAbstractVideoItem *item =  self.filteredVideos[indexPath.section];
    titleView.titleLabel.text =  item.videoThumbnail.title;
    return titleView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    _firstToShow = true;
    BOOL searching  = NO;
    if(self.searchText && self.searchText.length>0){
        searching  = YES;
        NSPredicate* titlepredicate  = [NSPredicate predicateWithFormat:@"(title CONTAINS[cd] %@)",self.searchBar.text];
        self.filteredVideos = [self.videos filteredArrayUsingPredicate:titlepredicate];
    }else{
        self.filteredVideos = self.videos;
        self.searchText = nil;
    }
    
    if(!self.searchBar.text.length && self.searchText.length>0){
        self.filteredVideos = self.videos;
        self.searchText = nil;
    }
    NSUInteger count =  self.filteredVideos.count;
    
    if (count>0)
        self.searchBar.userInteractionEnabled = YES;
    else{
        if(!searching){
            if(self.containsMovie){
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate: [NSDate dateWithTimeIntervalSince1970:0] withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_TIME_CINEMA_MOVIES];
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate dateWithTimeIntervalSince1970:0]  withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_TIME_MOST_POPULAR_MOVIES];
            }else{
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate: [NSDate dateWithTimeIntervalSince1970:0] withFormat:PRIMARY_DATE_FORMAT]  forKey:LAST_UPDATE_TIME_AIRING_TODAY_TV_SHOWS];
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate dateWithTimeIntervalSince1970:0]  withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_TIME_TOP_RATED_TV_SHOWS];
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate dateWithTimeIntervalSince1970:0]  withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_LATEST_TV_SHOWS];
            }
        }
    }
    return count;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self reset];
    [self requestUpate:YES];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isMemberOfClass:[MYUAbstractVideoVC class]]) {
        [self doesNotRecognizeSelector:_cmd];
        return nil;
    }
    return nil;
}


- (BOOL)shouldAutorotate{
    return YES;
}
#pragma mark - View Rotation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (MYUVideoCell* cell in self.collectionView.visibleCells){
            [cell willBeRemovedFromCollectionView];
        }
        [self.collectionView reloadData];
    });
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    self.videosLayout.numberOfColumns = [MYUUtilities videoItemPerRowOnOrientation:toInterfaceOrientation];
}

- (void)hideProgessBarAndStop:(NSTimer *)timer {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self registerForNotifications];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.hud hideAnimated:NO];
        
        if(self.showing && self.isVisible){
            NSString* toastMessage = NSLocalizedString(@"loading failed", @"loading failed");
            [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
        }
    });
}

- (void)requestUpate:(BOOL)forceUpdate {
    
    if ([NSThread isMainThread]){
        self.hidingTimer =  [NSTimer scheduledTimerWithTimeInterval:TEN_SECONDS target:self selector:@selector(hideProgessBarAndStop:)  userInfo:nil repeats:NO];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.hidingTimer =  [NSTimer scheduledTimerWithTimeInterval:TEN_SECONDS target:self selector:@selector(hideProgessBarAndStop:)  userInfo:nil repeats:NO];
        });
    }
    
}

- (void)setFromDB:(NSString *)noteName {
    self.videos  = [[MYUWebOperator sharedWebOperator] valueForKey:[NSString stringWithFormat:@"%@%@",noteName,@"FromDB"] ];
    
    NSArray *sortedArray = [self.videos sortedArrayUsingSelector:@selector(compare:)];
    self.videos =[NSMutableArray arrayWithArray:sortedArray];
    
    if(self.containsMovie){
        for (MYUMovieItem* item in sortedArray){
            item.title = ((MYUAbstractVideoItem*) item ).videoThumbnail.title;
            [self.videos addObject:item];
        }
    }else{
        for (MYUTVShowItem* item in sortedArray){
            item.title = ((MYUAbstractVideoItem*) item ).videoThumbnail.title;
            [self.videos addObject:item];
        }
    }
    while (self.videos.count>MAX_NUMBER_OF_VIDEOS) {
        [self.videos removeObjectAtIndex:self.videos.count - 1];
    }
    for (id item in self.videos){
        [self.videosNames addObject:((MYUAbstractVideoItem*) item ).videoThumbnail.title];
    }
    
}
#pragma mark - Automatic Fetch

- (void)startAutomaticUpdates {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.hud hideAnimated:NO];
    });
    [self requestUpate:NO];
    if (_updateTimer.isValid){
        DebugLog(@"Clearing thread  already running");
    }else{
        [self.updateThread cancel];
        self.updateThread = [[NSThread alloc] initWithTarget:self selector:@selector(runServince) object:nil];
        [self.updateThread start];
    }
    
}
- (void)stopAutomaticUpdates{
    if ( _updateTimer.isValid){
        [_updateTimer invalidate];
    }
    [self.updateThread cancel];
}


-(void)requestUpateByForcing{
    [self requestUpate:YES];
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}

- (void)runServince {
    
    if(self.updateTimer && self.updateTimer.valid){
        [self.updateTimer invalidate];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:TEN_MINUTES target:self selector:@selector(requestUpateByForcing)  userInfo:nil repeats:YES];
    });
}

- (void)dealloc{
    [self.thumbnailQueue cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
