//
//  MYUTrailerInfo.h
//  MovYU
//
//  Created by Serge Mbamba on 2017/01/23.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYUTrailerInfo : NSObject

@property (readwrite,strong) NSString* itemID;
@property (readwrite,strong) NSString* languageMain;
@property (readwrite,strong) NSString* languageSecond;
@property (readwrite,strong) NSString* key;
@property (readwrite,strong) NSString* site;
@property (readwrite,strong) NSNumber* size;
@property (readwrite,strong) NSString* type;
@property (readwrite,strong) NSString* title;

-(instancetype) initWithDictionary:(NSDictionary *)dictionary;

@end
