//
//  DBConnector.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/16.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Images+CoreDataProperties.h"
#import "Movies+CoreDataClass.h"
#import "TVShows+CoreDataClass.h"
#import "Genres+CoreDataClass.h"
#import "MYUMovieItem.h"
#import "MYUTVShowItem.h"
@interface MYUCoreDataConnector : NSObject

+(BOOL) updateOrInsertGenre:(NSString*) name
                    withID:(int) genreID;

+(void) updateOrInsertImage:(UIImage*) image
                     withID:(NSString*) movieTitle;


+(NSArray*) getImages;



+(NSArray*) getGenres;

+(NSMutableArray*) searchResultsForID : (int) searchID;

+(BOOL) insertSearchResult: (int) genreID
                withActors: (NSString*) actors
                withGenres: (NSString*) genres
                   isMovie: (BOOL) isMovie
            ratingLoweBoud: (float) ratingLowerBound
          ratingUpperBound: (float) ratingUpperBound
          searchID        : (int16_t) searchID
                     title: (NSString*)title
              foundVideos : (NSArray<MYUAbstractVideoItem *> *)foundVideos;
@end
