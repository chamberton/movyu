//
//  MYUFilterVC.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/27.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYUFilterVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, copy) NSArray* genres;
@property (nonatomic, copy) NSArray* ratings;
@end
