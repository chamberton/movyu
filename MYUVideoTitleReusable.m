//
//  MUYVideoTitleReusable.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUVideoTitleReusable.h"
#import "MYUColorAndFontManager.h"
@implementation MYUVideoTitleReusable


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth |
        UIViewAutoresizingFlexibleHeight;
        self.titleLabel.backgroundColor = [MYUColorAndFontManager backgroundColorForUIElementWithTag:(int)self.tag];
         self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [MYUColorAndFontManager fontForUIElementWithTag:(int)self.tag];
        self.titleLabel.textColor = [MYUColorAndFontManager textColorForUIElementWithTag:(int)self.tag];
        self.titleLabel.shadowColor = [MYUColorAndFontManager shadowColorForTitleOfUIElementWithTag:(int)self.tag];
        self.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.titleLabel.text = nil;  // destroy the text
    
}
@end
