//
//  MYUStringFactory.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUStringFactory.h"

@implementation MYUStringFactory

+(NSString*) createConcatenatedStringFromArrayEntries : ( NSArray*) Array{
    NSMutableString* createdString = [NSMutableString string];
    for(NSString* str in Array ){
        
        if (str!=nil && ![str isKindOfClass:[NSNull class]] ){
            createdString =(createdString.length==0)?[NSMutableString stringWithFormat:@"%@",str]:[NSMutableString stringWithFormat:@"%@,%@",createdString,str];
        }
    }
    return [NSString stringWithFormat:@"%@",createdString];
}

+(NSString*) createConcatenatedStringFromDictionaryEntries : ( NSArray*) Array
                                              withUniqueKey:(NSString*) key{
    
    NSMutableString* createdString = [NSMutableString string];
    if (key==nil)
        return createdString;
    
    for(NSDictionary* item in Array){
        NSString* str = [item valueForKey:key];
        
        if (str!=nil && ![str isKindOfClass:[NSNull class]] ){
            createdString =(createdString.length==0)?[NSMutableString stringWithFormat:@"%@",str]:[NSMutableString stringWithFormat:@"%@,%@",createdString,str];
        }
        
        
    }
    return [NSString stringWithFormat:@"%@",createdString];
    
}

+(NSString*) fullLanguageNameFromLanguageCode:(NSString*) codeLanguage{
    
    if (codeLanguage==nil)
        return @"";
    NSLocale *enLocale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale currentLocale] localeIdentifier]] ;
    return [enLocale displayNameForKey:NSLocaleIdentifier value:codeLanguage];
}

+(NSString*) concatenatedFullLanguageNamesCodeArray:(NSArray*) codeLanguageArray{
    NSMutableArray* array =[NSMutableArray array];
    for (NSString* item in codeLanguageArray){
        if (item!=nil && ![item isMemberOfClass:[NSNull class]]){
            NSString* obj = [MYUStringFactory fullLanguageNameFromLanguageCode:item];
            if (obj)
            [array addObject:obj];
        }
    }
    return [MYUStringFactory createConcatenatedStringFromArrayEntries:array];
}

+(NSString*) concatenatedFullCountryNamesCodeArray:(NSArray*) codeCountrArray
                                        countLimit:(int) limit{
    if (limit<=0)
        return @"";
    NSMutableArray* array =[NSMutableArray array];
    for (NSString* item in codeCountrArray){
        if (item!=nil && ![item isMemberOfClass:[NSNull class]]){
            id obj =  [[NSLocale systemLocale] displayNameForKey:NSLocaleCountryCode value:item];
            if (obj)
            [array addObject:obj ];
        }
        if (array.count==limit)
            break;
    }
    return [MYUStringFactory createConcatenatedStringFromArrayEntries:array];
    
}
@end
