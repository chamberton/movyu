//
//  MYUMovieItem.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MYUAbstractVideoItem.h"
#import "MYUDefinitions.h"

@interface MYUMovieItem : MYUAbstractVideoItem<isVideoItem>
@property ( readwrite,nonatomic) NSMutableSet* genreIDS;
@property ( readwrite,nonatomic) NSString    * synopsis;
@property ( readwrite,nonatomic) NSMutableSet* actors;
@property ( readwrite,nonatomic) NSMutableSet* directors;
@property float popularity;
@property float voteAverage;
@property int64_t movieID;

- (NSComparisonResult)compare:(MYUMovieItem *)otherObject ;

@end
