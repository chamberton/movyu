/*
     File: APLTableViewController.h
 Abstract: Table view controller to manage display of quotations from various criterions.
 The controller supports opening and closing of sections. To do this it maintains information about each section using an array of SectionInfo objects.
 
  Version: 3.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Apple Inc. All Rights Reserved.
 
 */

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>     // for MFMailComposeViewControllerDelegate
#import "APLSectionHeaderView.h"    // for SectionHeaderViewDelegate
#import "MYUTVShowsVC.h"
#import "MYUVideoCell.h"
#import "MYUColorAndFontManager.h"
#import "MYUVideoCollectionViewLayout.h"
#import "MYUVideoTitleReusable.h"
#import "MYUVideoCell.h"
#import "MYUThumbnail.h"
#import "MYUDefinitions.h"
#import "MYUWebOperator.h"
#import "MYUColorAndFontManager.h"
#import "MYUTVShowItem.h"
#import "MYUCache.h"
#import "MYUTVShowDetailsVC.h"
#import "MYUVideoFactory.h"
#import "MBProgressHUD.h"
#import "MYUThumbailFactory.h"
#import "MYUCoreDataConnector.h"
#import "MYUCriteria.h"
@interface APLTableViewController : UIViewController <UITableViewDataSource, SectionHeaderViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong,readwrite)   NSMutableArray *refinedVideos;
@property (nonatomic, strong,readwrite)   NSMutableSet *refineVideoNames;
@property (nonatomic, strong,readwrite)   NSMutableArray *filterVideos;
@property (nonatomic, strong,readwrite)   NSMutableSet *filterVideoNames;
@property (nonatomic, strong,readwrite)   NSMutableArray<MYUAbstractVideoItem*> *videos;
@property (nonatomic, strong,readwrite)   NSMutableArray *movies;
@property (nonatomic, strong) NSMutableSet *moviesNames;
@property (nonatomic, strong) NSMutableSet *TVShowNames;
@property (nonatomic, strong,readwrite)   NSMutableArray *tvShows;
@property (nonatomic, strong) NSMutableSet *tvShowsNames;
@property (nonatomic, strong) NSMutableSet *MoviesIDs;
@property (nonatomic, strong) NSMutableSet *TVShowIDs;
@property (nonatomic, strong,readwrite)   NSMutableArray *currentMovies;
@property (nonatomic, strong,readwrite)   NSMutableArray * currentTVShows;
@property (nonatomic, strong,readwrite)   NSMutableSet *currentMoviesNames;
@property (nonatomic, strong,readwrite)   NSMutableSet *currentTVShowsNames;
@property (nonatomic, strong) NSOperationQueue *thumbnailQueue;
@property (retain, nonatomic) NSMutableArray *transforms;
@property (nonatomic,strong)  MBProgressHUD * hud;
@property (nonatomic,retain) dispatch_queue_t savingQueue;
@property (nonatomic) NSMutableArray<MYUCriteria *> *criteria;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

