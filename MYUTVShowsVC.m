//
//  MYUMovieShowsVC.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/14.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUTVShowsVC.h"
#import "MYUVideoCell.h"
#import "MYUColorAndFontManager.h"
#import "MYUVideoCollectionViewLayout.h"
#import "MYUVideoTitleReusable.h"
#import "MYUVideoCell.h"
#import "MYUThumbnail.h"
#import "MYUDefinitions.h"
#import "MYUWebOperator.h"
#import "MYUColorAndFontManager.h"
#import "MYUTVShowItem.h"
#import "MYUCache.h"
#import "MYUTVShowDetailsVC.h"
#import "MYUVideoFactory.h"
#import "MBProgressHUD.h"
#import "MYUThumbailFactory.h"
#import "MYUCoreDataConnector.h"
#import "iToast.h"

@interface MYUTVShowsVC ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong) NSOperationQueue *downlaodingQueue;

@end

@implementation MYUTVShowsVC


#pragma mark - Lifecycle

- (void)viewDidLoad {
    self.containsMovie = NO;
    [super viewDidLoad];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [MYUColorAndFontManager backgroundColorForUIElementWithTag:(int)self.collectionView.tag];
    
    [self.collectionView setCollectionViewLayout:self.videosLayout];
    
    [self.collectionView registerClass:[MYUVideoCell class]
            forCellWithReuseIdentifier:MYUTVShowCellIdentifier];
    
    [self.collectionView registerClass:[MYUVideoTitleReusable class]
            forSupplementaryViewOfKind:ThumbnailCellTitle
                   withReuseIdentifier:ThumbnailCellTitle];
    self.downlaodingQueue = [[NSOperationQueue alloc] init];
    [self registerForNotifications];
    [self startAutomaticUpdates];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isVisible = YES;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.isVisible = YES;
}

#pragma - Notification handler

- (void)handleNotifications:(NSNotification *)note {
    
    NSString* noteName = [note name];
    BOOL latest = [noteName isEqualToString:LATEST_TV_SHOWS_UPDATES];
    BOOL topRated = [noteName isEqualToString:TOP_RATED_TV_SHOWS_UPDATES];
    BOOL airing = [noteName isEqualToString:airing_TODAY_TV_SHOWS_UPDATES];
    
    if (latest || topRated || airing){
        
        MYUWebOperator* operator = [MYUWebOperator sharedWebOperator];
        if(operator.retrievedFromDBTV==false){
            if(operator.topRatedTVShowsError==nil || operator.latestTVShowsError==nil || operator.airingTodayTVShowsError==nil){
                
                NSDictionary* newTVShows = [[MYUWebOperator sharedWebOperator] valueForKey:noteName];
                [self addTVShows:newTVShows];
                
                if( self.videos.count>=MAX_NUMBER_OF_VIDEOS || operator.completedTVShowOperation ){
                    [[NSNotificationCenter defaultCenter] removeObserver:self];
                    [self registerForNotifications];
                    
                    while (self.videos.count>MAX_NUMBER_OF_VIDEOS) {
                        [self.videos removeLastObject];
                    }
                    
                    if(latest){
                        [MYUCoreDataConnector insertSearchResult:TVHSHOWLATEST withActors:nil withGenres:nil isMovie:false ratingLoweBoud:0 ratingUpperBound:0 searchID:TVHSHOWLATEST title:nil foundVideos:self.videos ];
                    }else if(topRated){
                        [MYUCoreDataConnector insertSearchResult:TVHSHOWTOPRATED withActors:nil withGenres:nil isMovie:false ratingLoweBoud:0 ratingUpperBound:0 searchID:TVHSHOWTOPRATED title:nil foundVideos:self.videos ];
                    }else {
                        [MYUCoreDataConnector insertSearchResult:TVSHOWAIRINGTODAY withActors:nil withGenres:nil isMovie:false ratingLoweBoud:0 ratingUpperBound:0 searchID:TVSHOWAIRINGTODAY title:nil foundVideos:self.videos ];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectionView reloadData];
                    });
                }
            }
            
            
        }
        else{
            
            
            [self setFromDB:noteName];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });
        }
        
    }
}


-(void) addTVShows:(NSDictionary*) dictionary{
    
    NSArray* tvShows = [dictionary objectForKey:RESULTS];
    if([tvShows isKindOfClass:[NSArray class]]){
        for (NSDictionary* item in tvShows){
            if(self.videos.count==MAX_NUMBER_OF_VIDEOS)
                break;
            MYUTVShowItem* tvShowItem =[MYUVideoFactory createFromTVShowDictionay:item];
            if ([self.videosNames containsObject:[tvShows valueForKey:ORIGINAL_TITLE]]
                ||  [self.videosNames containsObject:tvShowItem.videoThumbnail.title]) // if the image for the TV show already exists
                continue;
            else if (tvShowItem){
                [ self.videos addObject:tvShowItem];
                [self.videosNames addObject:tvShowItem.videoThumbnail.title];
                
            }
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MYUVideoCell *videoCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:MYUTVShowCellIdentifier forIndexPath:indexPath];
    MYUTVShowItem *tvShow =  self.filteredVideos[indexPath.section];
    MYUThumbnail *thumbnail = tvShow.videoThumbnail;
    videoCell.imageView.image = [[MYUCache sharedCache] imageForMovieTitle:thumbnail.title];
    
    if (!videoCell.imageView.image || [videoCell.imageView.image isEqual:[[MYUCache sharedCache] missingPicture]]){
       
        
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            NSURL *imageURL = thumbnail.imageURL;
            if (!imageURL){
                imageURL = [[MYUWebOperator sharedWebOperator] urlForTVShowImage:(int)tvShow.tvShowID];
            }
            NSString* title = thumbnail.title;
            UIImage *image = nil;
            if (imageURL) {
                image = [MYUThumbailFactory imageWithTitle:title andURL:imageURL];
               
            }
            if(image) {
                [[MYUCache sharedCache] addImage:image forName:thumbnail.title];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
                });
            }
           
        }];
        
        operation.queuePriority = NSOperationQueuePriorityVeryHigh ;
        [self.downlaodingQueue addOperation:operation];
        
    }
    self.showing = NO;
    [self.hidingTimer invalidate];
    if (self.firstToShow){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.hud hideAnimated:YES];
            self.firstToShow = false;
        });
    }
    
    return videoCell;
}


-(void) actualUpdate:(BOOL) force{
    
    
    self.videos =[NSMutableArray array];
    self.videosNames =[NSMutableSet set];
    
    switch (_segmentedControl.selectedSegmentIndex) {
        case Latest:
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleNotifications:)
                                                         name:LATEST_TV_SHOWS_UPDATES
                                                       object:nil];
            [[MYUWebOperator sharedWebOperator] refreshTVShowsLatest] ;
            
            break;
        case airingToday:
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleNotifications:)
                                                         name:airing_TODAY_TV_SHOWS_UPDATES
                                                       object:nil];
            [[MYUWebOperator sharedWebOperator] refreshTVShowsAiringToday] ;
            
            break;
        case TopRated:
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleNotifications:)
                                                         name:TOP_RATED_TV_SHOWS_UPDATES
                                                       object:nil];
            [[MYUWebOperator sharedWebOperator] refreshTVShowsTopRated] ;
            
            break;
        default:
            break;
    }
    
    [self registerForNotifications];
}


-(void) requestUpate:(BOOL) forceUpdate{
    
    self.showing = YES;
    [super requestUpate:forceUpdate];
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        [self.hud hideAnimated:NO];
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        self.hud.label.text = NSLocalizedString(@"Loading", @"HUD loading title");
        self.hud.detailsLabel.text = NSLocalizedString(@"Parsing data", @"HUD title");
        [self actualUpdate:forceUpdate];
        
    });
    
}


-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MYUMovieItem *movie =  self.filteredVideos[indexPath.section];
    [self performSegueWithIdentifier:@"ShowTVShowDetail" sender:movie];
}

- (IBAction)switchedSegment:(id)sender {
    [self.searchBar resignFirstResponder];
    self.searchBar.text = @"";
    [self.hud hideAnimated:NO];
    [self reset];
    
    if(sender==_segmentedControl){
        [self requestUpate:NO];
        [self.collectionView reloadData];
    }
}

- (IBAction)refresh:(id)sender {
    if([MYUWebOperator sharedWebOperator].connectedToInternet){
        [self requestUpate:YES];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString* toastMessage = NSLocalizedString(@"Offline", @"Offline");
            [[[[iToast makeText:toastMessage] setGravity:iToastGravityCenter] setDuration:iToastDurationShort] show];
        });
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ShowTVShowDetail"]) {
        MYUTVShowDetailsVC *tvShowsVC  = [segue.destinationViewController  viewControllers][0];
        MYUTVShowItem *tvShow = (MYUTVShowItem*) sender;
        tvShow.videoThumbnail.poster= [[MYUCache sharedCache] imageForMovieTitle:tvShow.videoThumbnail.title];
        if (  tvShow.videoThumbnail.poster==nil){
            MYUThumbnail *thumbnail = [MYUThumbailFactory  thumnailWithImageURL:tvShow.videoThumbnail.imageURL andTitle:tvShow.videoThumbnail.title];
            tvShow.videoThumbnail = thumbnail;
        }
        
        [tvShowsVC setTvShowItem:tvShow];
        tvShowsVC.title =tvShow.videoThumbnail.title;
        [tvShowsVC setTitle:[NSString stringWithFormat:@"%@",tvShow.videoThumbnail.title]];
        [tvShowsVC.parentViewController setTitle:[NSString stringWithFormat:@"%@",tvShow.videoThumbnail.title]];
        
    }
}

#pragma mark - UI search bar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [searchText stringByTrimmingCharactersInSet:charSet];
    if (![trimmedString isEqualToString:@""]) {
        self.searchText = searchText;
        self.lastSearchTextLength = (int)searchBar.text.length;
        [self.collectionView reloadData];
    }else{
        if(self.lastSearchTextLength>0){
            self.lastSearchTextLength = (int)searchBar.text.length;
            self.searchText = @"";
            [self.collectionView reloadData];
        }
    }
}

@end
