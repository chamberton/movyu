//
//  MYUTVShowSeasonDetails.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUTVShowSeasonDetails.h"
#import "MYUTVShowDetailsCellVC.h"
@interface MYUTVShowSeasonDetails ()


@end

@implementation MYUTVShowSeasonDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.tableView reloadData];
    self.tableView.allowsSelection = NO;
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return _tvShowItem.seasons.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MYUTVShowDetailsCellVC *cell = [tableView dequeueReusableCellWithIdentifier:@"seasonDetails" forIndexPath:indexPath];
    
    NSDictionary* season = _tvShowItem.seasons[indexPath.row];
    cell.seasonLabel.text= [NSString stringWithFormat:@"%@",[season valueForKey:@"season_number"]];
    cell.releaseDateTextField.text =(NSString*)[season valueForKey:@"air_date"];
    cell.episodeLabel.text= [NSString stringWithFormat:@"%@",[season valueForKey:@"episode_count"]];
    return cell;
}

@end
