//
//  MYUWebOperator.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUWebOperator.h"
#import "MYUUtilities.h"
#import "AFNetworking.h"
#import "MYUCache.h"
#import "MYUCoreDataConnector.h"
#import "MYUTrailerInfoFactory.h"
#import "MYUVideoFactory.h"

static MYUWebOperator* sharedWebMYUWebOperator = nil;

@interface MYUWebOperator()
@property (strong, readwrite,nonatomic) NSString* APIKey;
@property (strong, readwrite,nonatomic) NSDictionary* API_URLs;
@property (strong, readwrite,nonatomic) NSString* currentLanguage;
@property (strong, readwrite,nonatomic) AFHTTPSessionManager *manager ;
@property BOOL loadingCast ;
@end
static NSError* allowOnlyOneError;

@implementation MYUWebOperator

+(void) load{
    
    NSDictionary *userInfo = @{
                               NSLocalizedDescriptionKey: NSLocalizedString(@"Mock Error", nil),
                               NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Just a dummy error object.", nil),
                               NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Ignore this is not a real error", nil)
                               };
    allowOnlyOneError= [NSError errorWithDomain:@"myDomain"
                                           code:-57
                                       userInfo:userInfo];
}

+(MYUWebOperator*) sharedWebOperator{
    
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedWebMYUWebOperator = [[MYUWebOperator alloc] init];
        sharedWebMYUWebOperator.loadingCast = false;
        [sharedWebMYUWebOperator configureManager];
    });
    
    return sharedWebMYUWebOperator;
    
}
-(instancetype) init{
    self = [super init];
    if (self){
        _API_URLs  =[[NSBundle mainBundle] objectForInfoDictionaryKey:API_URLS];
        
        _hasLoadedMovieCast = NO;
        _completedTVShowOperation = NO;
        _hasLoadedTVShowCast  =NO;
        _hasLoadedTVShowDetails=NO;
        _APIKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:API_KEY];
        sharedWebMYUWebOperator = self;
        [self setLanguage];
    }
    return self;
}
-(void) setLanguage{
    _currentLanguage  = [[NSLocale currentLocale] localeIdentifier];
    
    if (_currentLanguage==nil)
        _currentLanguage=@"en-US";
    if ([_currentLanguage containsString:@"_"]){
        NSArray*  components = [_currentLanguage componentsSeparatedByString:@"_"];
        _currentLanguage = components[0];
    }
}

-(void) configureManager{
    self.manager = [AFHTTPSessionManager manager];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
}


-(NSString*) urlForMovieCastWithID:(int) ID{
    
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:MOVIE_CAST],ID,_APIKey,_currentLanguage];
    return str;
}
-(NSString*) urlForTVShowCastWithID:(int) ID{
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:TV_SHOW_CREDIT],ID,_APIKey,_currentLanguage];
    return str;
}

-(NSString*) urlForTVShowDetailsWithID:(int) ID{
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:TV_SHOW_DETAILS],ID,_APIKey,_currentLanguage];
    return str;
}

-(NSString*) urlForMovieDetails:(int) ID{
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:MOVIE_DETAILS],ID,_APIKey,_currentLanguage];
    return str;
}

-(NSString*) urlForMoviesInCinema{
    
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:MOVIES_INCINEMA],self.moviesInCinemaPageNumber,_APIKey,_currentLanguage];
    return str;
}

-(NSString*) urlForMostPopularMoviesInCinema{
    
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:MOST_POPULAR_MOVIES],self.mostPopularMoviesInCinemaPageNumber,_APIKey,_currentLanguage];
    return str;
}

-(NSString*) urlForTopRatedTVShows{
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:TOP_RATED_TV_SHOWS],self.topRatedTVShowPageNumber,_APIKey,_currentLanguage];
    return str;
}
-(NSString*) urlForAiringTodayTVShows{
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:AIRING_TODAY_TV_SHOWS],self.airingTodayTVShowPageNumber,_APIKey,_currentLanguage];
    return str;
}
-(NSString*) urlForLatestTVShows{
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:LATEST_TV_SHOWS],self.latestTVShowsPageNumber,_APIKey,_currentLanguage];
    return str;
}

-(NSString*) urlForPhotoPath{
    return [_API_URLs objectForKey:POSTER_PREFIX];
}

-(NSString*) urlForSearchingMovie : (NSString*) movieKeyWord{
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:SEARCHING_MOVIES],self.movieSearchPageNumber,_APIKey,movieKeyWord,_currentLanguage];
    return str;
}
-(NSString*) urlForSearchingTV : (NSString*) movieKeyWord{
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:SEARCHING_TVs],self.tvShowSearchPageNumber,_APIKey,movieKeyWord,_currentLanguage];
    return str;
}
-(NSString*) urlForSearchingMovingInTheLast365Days{
    
    NSDate*   endDate = [NSDate date];
    NSDate*   startDay = [NSDate dateWithTimeIntervalSinceNow:-1*90*24*3600];
    
    return [self urlForMoviesInCinemaWithinDate:startDay end:endDate];
}

-(NSString*) urlForSearchingTVShowTheLast365Days{
    
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:LATEST_TV_SHOWS],self.tvShowSearchPageNumber,_APIKey,_currentLanguage];
    return str;
}
-(NSString*) urlForTVShowWithinDate:(NSDate*) start
                                end:(NSDate*) end{
    if (start==nil  || end==nil)
        return @"";
    NSString* startDate =[MYUUtilities stringFromDate:start withFormat:DAY_DATE_FORMAT];
    NSString* endDate =[MYUUtilities stringFromDate:end withFormat:DAY_DATE_FORMAT];
    NSString* apiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:API_KEY];
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:TV_SHOWS_WITHIN_DATE],startDate,endDate,self.tvShowSearchPageNumber,apiKey,_currentLanguage];
    return str;
}

-(NSString*) urlForMovieTrailer:(int)movieID {
    
    NSString* apiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:API_KEY];
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:MOVIE_TRAILER_KEY],movieID,apiKey,_currentLanguage];
    return str;
}

-(NSString*) urlForTVShowTrailer:(int)movieID {
    
    NSString* apiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:API_KEY];
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:TV_TRAILER_KEY],movieID,apiKey,_currentLanguage];
    return str;
}


- (NSString*)urlForMoviesInCinemaWithinDate:(NSDate *)start
                                        end:(NSDate *)end {
    if (start==nil  || end==nil)
        return @"";
    NSString* startDate =[MYUUtilities stringFromDate:start withFormat:DAY_DATE_FORMAT];
    NSString* endDate =[MYUUtilities stringFromDate:end withFormat:DAY_DATE_FORMAT];
    NSString* apiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:API_KEY];
    NSString* str= [NSString stringWithFormat:[_API_URLs objectForKey:MOVIES_WITHIN_DATE],startDate,endDate,self.movieSearchPageNumber,apiKey,_currentLanguage];
    return str;
}
#pragma mark -  Movies Updates

- (void)refreshMoviesInCinema {
    self.retrievedFromDBMovie = NO;
    self.moviesInCinemaPageNumber = 1;           // reset the page number counter
    self.moviesInCinemas = [NSMutableArray array];  // clear the results container
    
    if (_connectedToInternet && ([[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:MOVIE_IN_CINEMA_UPDATES] withFormat:PRIMARY_DATE_FORMAT ]]>2*ONE_HOUR_IN_SECONDS)){
        [MYUCache  sharedCache].lastMoviesInCinema = [NSMutableArray array];
        [self updateMoviesInCinema];
    }else{
        _mostPopularError = allowOnlyOneError;
        self.retrievedFromDBMovie = true;
        if ([MYUCache  sharedCache].lastMoviesInCinema ==nil || [MYUCache  sharedCache].lastMoviesInCinema.count <=0 )
        {
            _moviesInCinemaError = nil;
            
            self.moviesInCinemaFromDB = [MYUCoreDataConnector searchResultsForID :CINEMAALL];
            if (self.moviesInCinemaFromDB.count>0){
                _moviesInCinemaError = nil;
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:MOVIE_IN_CINEMA_UPDATES
                 object:nil
                 userInfo:nil
                 ];
            }else{
                self.retrievedFromDBTV = NO;
                [self updateMoviesInCinema];
            }
        }else{
            self.moviesInCinemaError = nil;
            sharedWebMYUWebOperator.loadingMovies = YES;
            const NSArray* lastestMovies = [MYUCache  sharedCache].lastMoviesInCinema;
            const NSUInteger count = lastestMovies.count;
            for (NSDictionary* dic in lastestMovies){
                self.moviesInCinemaPageNumber++;
                self.moviesInCinema = dic;
                if(count==self.moviesInCinemaPageNumber){
                    sharedWebMYUWebOperator.loadingMovies = NO;
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:MOVIE_IN_CINEMA_UPDATES object:nil userInfo:nil];
            }
            
        }
        
        if (!_connectedToInternet && [[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_TIME_CINEMA_MOVIES] withFormat:PRIMARY_DATE_FORMAT ]]>ONE_HOUR_IN_SECONDS){
            
            [[NSNotificationCenter defaultCenter] postNotificationName:infoOutdatedNotification object:nil userInfo:nil];
        }
    }
    
}

- (void)refreshMoviesMostPopularInCinema {
    
    self.moviesMostPopulars = [NSMutableArray array];
    self.mostPopularMoviesInCinemaPageNumber = 1;
    self.retrievedFromDBMovie = NO;
    
    if (_connectedToInternet && ([[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_TIME_MOST_POPULAR_MOVIES] withFormat:PRIMARY_DATE_FORMAT ]]>2*ONE_HOUR_IN_SECONDS) ){
        [MYUCache  sharedCache].lastMostPopularInCinema = [NSMutableArray array];
        [self updateMostPopularMovies];
    }else{
        self.moviesInCinemaError = allowOnlyOneError;
        self.retrievedFromDBMovie = true;
        
        if ([MYUCache  sharedCache].lastMostPopularInCinema==nil || [MYUCache  sharedCache].lastMostPopularInCinema.count<=0){
            self.moviesInCinemaError = nil;
            
            self.mostPopularMoviesFromDB = [MYUCoreDataConnector searchResultsForID :CINEMAMOSTPOPULAR];
            if (self.mostPopularMoviesFromDB.count>0){
                self.retrievedFromDBMovie = YES;
                self.mostPopularError = nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:MOST_POPULAR_MOVIES_UPDATES object:nil userInfo:nil];
            }else{
                [self updateMostPopularMovies];
            }
        }else{
            self.mostPopularError = nil;
            const NSArray* mostPopularMovies =  [MYUCache  sharedCache].lastMostPopularInCinema;
            const NSUInteger count = mostPopularMovies.count;
            sharedWebMYUWebOperator.loadingMovies = YES;
            for (NSDictionary* dic in mostPopularMovies ){
                self.mostPopularMoviesInCinemaPageNumber++;
                self.mostPopularMovies = dic;
                if(count==self.mostPopularMoviesInCinemaPageNumber){
                    sharedWebMYUWebOperator.loadingMovies = NO;
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:MOST_POPULAR_MOVIES_UPDATES object:nil userInfo:nil];
            }
        }
        
        if (!_connectedToInternet &&[[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_TIME_MOST_POPULAR_MOVIES] withFormat:PRIMARY_DATE_FORMAT ]]>ONE_HOUR_IN_SECONDS){
            [[NSNotificationCenter defaultCenter] postNotificationName:infoOutdatedNotification object:nil userInfo:nil];
        }
    }
}


- (void)updateMoviesInCinema {
    
    NSURL *url = [NSURL URLWithString:[self urlForMoviesInCinema]];
    self.mostPopularError = allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _moviesInCinema =(NSDictionary*) responseObject;
        if (_moviesInCinema!=nil){
            _moviesInCinemaError = nil;
            const int pages = (int) [(NSNumber*)[_moviesInCinema objectForKey:TOTAL_PAGES] integerValue];
            if (pages-self.moviesInCinemaPageNumber>0 && self.moviesInCinemaPageNumber<MAXIMUM_NUMBER_OF_PAGES){
                self.moviesInCinemaPageNumber++;
                [self updateMoviesInCinema];
                [self.moviesInCinemas addObject:_moviesInCinema];
                [[MYUCache  sharedCache].lastMoviesInCinema addObject:[_moviesInCinema copy]];
                [[NSNotificationCenter defaultCenter] postNotificationName:MOVIE_IN_CINEMA_UPDATES object:nil userInfo:nil];
            }else{
                sharedWebMYUWebOperator.loadingMovies = NO;
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate date] withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_TIME_CINEMA_MOVIES];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:MOVIE_IN_CINEMA_UPDATES object:nil userInfo:nil];
            }
        }
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSNumber* retry = error.userInfo[@"Retry-After"];
        if([retry isKindOfClass:[NSNull class]] ||  self.moviesInCinemaPageNumber>1){
            [[NSNotificationCenter defaultCenter] postNotificationName:MOST_POPULAR_MOVIES_UPDATES object:nil userInfo:nil];
            sharedWebMYUWebOperator.loadingMovies = NO;
        }else{
            self.moviesInCinema = nil;
            self.moviesInCinemaError = error;
            [[NSNotificationCenter defaultCenter] postNotificationName:MOVIE_IN_CINEMA_UPDATES object:nil userInfo:nil];
        }
    }];
}

- (void)updateMostPopularMovies {
    NSURL *url = [NSURL URLWithString:[self urlForMostPopularMoviesInCinema]];
    self.moviesInCinemaError = allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _mostPopularMovies =(NSDictionary*) responseObject;
        if (self.mostPopularMovies){
            self.mostPopularError = nil;
            const int pages = (int)[(NSNumber*)[_mostPopularMovies objectForKey:TOTAL_PAGES] integerValue];
            if (pages-self.mostPopularMoviesInCinemaPageNumber>0 &&  self.mostPopularMoviesInCinemaPageNumber<MAXIMUM_NUMBER_OF_PAGES){
                self.mostPopularMoviesInCinemaPageNumber++;
                [self updateMostPopularMovies];
                [self.moviesMostPopulars addObject:_mostPopularMovies];
                [[MYUCache  sharedCache].lastMostPopularInCinema addObject:[_mostPopularMovies copy]];
                [[NSNotificationCenter defaultCenter] postNotificationName:MOST_POPULAR_MOVIES_UPDATES object:nil userInfo:nil];
            }else{
                sharedWebMYUWebOperator.loadingMovies = NO;
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate date] withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_TIME_MOST_POPULAR_MOVIES];
                [[NSNotificationCenter defaultCenter] postNotificationName:MOST_POPULAR_MOVIES_UPDATES object:nil userInfo:nil];
            }
            
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSNumber* retry = error.userInfo[@"Retry-After"];
        if([retry isKindOfClass:[NSNull class]] ||  self.mostPopularMoviesInCinemaPageNumber>1){
            [[NSNotificationCenter defaultCenter] postNotificationName:MOST_POPULAR_MOVIES_UPDATES object:nil userInfo:nil];
            sharedWebMYUWebOperator.loadingMovies = NO;
        }else{
            _mostPopularMovies = nil;
            _mostPopularError = error;
            [[NSNotificationCenter defaultCenter]  postNotificationName:MOST_POPULAR_MOVIES_UPDATES object:nil userInfo:nil];
        }
        
    }];
}

- (void)trailerForTVShowWithID:(int)movieID :(void (^)(NSArray *arrayOfTrailers))completion {
    NSURL *url = [NSURL URLWithString:[self urlForTVShowTrailer:movieID]];
    __block NSDictionary* response;
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        response =(NSDictionary*) responseObject;
        NSArray* listOfTrailers =  [MYUTrailerInfoFactory createFromJson:response];
        completion(listOfTrailers);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        response = nil;
        NSArray* listOfTrailers =  [MYUTrailerInfoFactory createFromJson:response];
        completion(listOfTrailers);
    }];
    
}
- (void)  trailerForMovieWithID : (int)movieID :(void (^)(NSArray *arrayOfTrailers))completion {
    NSURL *url = [NSURL URLWithString:[self urlForMovieTrailer:movieID]];
    __block NSDictionary* response;
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        response =(NSDictionary*) responseObject;
        NSArray* listOfTrailers =  [MYUTrailerInfoFactory createFromJson:response];
        completion(listOfTrailers);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        response = nil;
        NSArray* listOfTrailers =  [MYUTrailerInfoFactory createFromJson:response];
        completion(listOfTrailers);
    }];
}

- (void)castForMovieWithID:(int)movieID {
    
    
    NSURL *url = [NSURL URLWithString:[self urlForMovieCastWithID:movieID]];
    
    _mostPopularError = allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _movieCast =(NSDictionary*) responseObject;
        if (_movieCast!=nil){
            _movieCastError = nil;
        }
        self.hasLoadedMovieCast = YES;
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        self.hasLoadedMovieCast = YES;
        _movieCast = nil;
        _movieCastError = error;
        
    }];
}

- (void)castForTVWithID:(int)tvShowID {
    
    
    NSURL *url = [NSURL URLWithString:[self urlForTVShowCastWithID:tvShowID]];
    
    _mostPopularError = allowOnlyOneError;
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _tvShowCast =(NSDictionary*) responseObject;
        if (_tvShowCast!=nil){
            _tvShowCastError = nil;
        }
        self.hasLoadedTVShowCast = YES;
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        self.hasLoadedTVShowCast = YES;
        _tvShowCast = nil;
        _tvShowCastError = error;
        
    }];
}

-(void)detailsForTVWithID:(int) tvShowID{
    
    
    NSURL *url = [NSURL URLWithString:[self urlForTVShowDetailsWithID:tvShowID]];
    
    _mostPopularError = allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _tvShowDetails =(NSDictionary*) responseObject;
        if (_tvShowDetails!=nil){
            _tvShowDetailsError = nil;
        }
        self.hasLoadedTVShowDetails = YES;
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        self.hasLoadedTVShowDetails = YES;
        _tvShowDetails = nil;
        _tvShowDetailsError = error;
    }];
}

- (NSURL*)urlForTVShowImage:(int)tvShowID {
    __block NSURL* tvShowURL = nil;
    NSURL *url = [NSURL URLWithString:[self urlForTVShowDetailsWithID:tvShowID]];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        tvShowURL = [MYUVideoFactory urlFrom:responseObject];
        
        dispatch_semaphore_signal(semaphore);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        dispatch_semaphore_signal(semaphore);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    return tvShowURL;
    
}

-(NSURL*)urlForMovieImage: (int) movieID{
    __block NSURL* movieImageURL = nil;
    NSURL *url = [NSURL URLWithString:[self urlForMovieDetails:movieID]];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        movieImageURL = [MYUVideoFactory urlFrom:responseObject];
        dispatch_semaphore_signal(semaphore);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        dispatch_semaphore_signal(semaphore);
    }];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return movieImageURL;
    
}


-(void) detailsForMovieWithID: (int) movieID{
    NSURL *url = [NSURL URLWithString:[self urlForMovieDetails:movieID]];
    
    _mostPopularError = allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _movieCast =(NSDictionary*) responseObject;
        if (_movieCast!=nil){
            _movieCastError = nil;
        }
        self.hasLoadedMovieDetails = YES;
        NSArray* arr = [_movieCast valueForKey:@"genres"];
        
        for (NSDictionary* dic in arr){
            NSNumber* genreID =[dic valueForKey:@"id"] ;
            NSString* genreName =(NSString*)[dic valueForKey:@"name"];
            [MYUVideoFactory addGenre:genreID forKey:genreName];
            [MYUCoreDataConnector  updateOrInsertGenre:genreName withID:[genreID intValue]];
            
        }
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kGenreIDNotification
         object:nil
         userInfo:nil
         ];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        self.hasLoadedMovieDetails = YES;
        _movieCast = nil;
        _movieCastError = error;
        
    }];
}

- (NSTimeInterval)intervalSinceLastSavedDateWithKey:(NSString *)key {
    NSDate* lastRecorded = [MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:key] withFormat:PRIMARY_DATE_FORMAT];
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:lastRecorded];
    return interval;
}

#pragma mark -  TV Show updates

- (void)refreshTVShowsLatest {
    
    
    self.latestTVShowsPageNumber =1;
    self.latestTVShowsS = [NSMutableArray array];
    self.retrievedFromDBTV = NO;
    
    NSTimeInterval interval = [self intervalSinceLastSavedDateWithKey:LAST_UPDATE_LATEST_TV_SHOWS];
    
    if (self.connectedToInternet && interval>2*ONE_HOUR_IN_SECONDS){
        [MYUCache  sharedCache].lastLatestTVShows = [NSMutableArray array];
        [self updateTVShowsLatest];
        
    }else{
        self.topRatedTVShowsError= allowOnlyOneError;
        self.airingTodayTVShowsError= allowOnlyOneError;
        
        if ([MYUCache  sharedCache].lastLatestTVShows==nil || [MYUCache  sharedCache].lastLatestTVShows.count==0){
            
            self.latestTVShowsFromDB = [MYUCoreDataConnector searchResultsForID :TVHSHOWLATEST];
            
            if (self.latestTVShowsFromDB.count>0){
                self.latestTVShowsError = nil;
                self.retrievedFromDBTV = YES;
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:LATEST_TV_SHOWS_UPDATES
                 object:nil
                 userInfo:nil
                 ];
            }else{
                
                [self updateTVShowsLatest];
            }
            
        }else{
            self.latestTVShowsError = nil;
            self.completedTVShowOperation= NO;
            const NSArray* latestTVShows = [MYUCache sharedCache].lastLatestTVShows;
            const NSUInteger count  = latestTVShows.count;
            for(NSDictionary* dic in latestTVShows){
                self.latestTVShowsPageNumber++;
                self.latestTVShows = dic;
                if(count==self.latestTVShowsPageNumber){
                    self.completedTVShowOperation=YES;
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:LATEST_TV_SHOWS_UPDATES object:nil userInfo:nil];
            }
            
        }
        
        if (!_connectedToInternet &&[[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_LATEST_TV_SHOWS] withFormat:PRIMARY_DATE_FORMAT ]]>ONE_HOUR_IN_SECONDS){
            [[NSNotificationCenter defaultCenter]
             postNotificationName:infoOutdatedNotification
             object:nil
             userInfo:nil
             ];
        }
    }
    
}
- (void)refreshTVShowsAiringToday {
    self.completedTVShowOperation= NO;
    self.airingTodayTVShowPageNumber =1;
    self.airingTodayTVShowsS = [NSMutableArray array];
    self.retrievedFromDBTV = NO;
    
    if (_connectedToInternet && ([[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_TIME_AIRING_TODAY_TV_SHOWS] withFormat:PRIMARY_DATE_FORMAT ]]>ONE_HOUR_IN_SECONDS)){
        [MYUCache  sharedCache].lastAiringTodayTVShows = [NSMutableArray array];
        [self updateTVShowsAiringToday];
    }else{
        
        self.topRatedTVShowsError= allowOnlyOneError;
        self.latestTVShowsError= allowOnlyOneError;
        
        if ([MYUCache  sharedCache].lastAiringTodayTVShows ==nil || [MYUCache  sharedCache].lastAiringTodayTVShows.count <=0 )
        {
            self.airingTodayTVShowsFromDB = [MYUCoreDataConnector searchResultsForID :TVSHOWAIRINGTODAY];
            if (self.airingTodayTVShowsFromDB.count>0){
                self.retrievedFromDBTV = YES;
                self.airingTodayTVShowsError = nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:airing_TODAY_TV_SHOWS_UPDATES object:nil userInfo:nil];
            }else{
                [self updateTVShowsAiringToday];
            }
            
        }else{
            self.airingTodayTVShowsError = nil;
            const NSArray* airingTVShows =[MYUCache  sharedCache].lastAiringTodayTVShows;
            const NSUInteger count = airingTVShows.count;
            for (NSDictionary* dic in airingTVShows){
                self.airingTodayTVShowPageNumber++;
                self.airingTodayTVShows = dic;
                if(count==  self.airingTodayTVShowPageNumber){
                    self.completedTVShowOperation=YES;
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:airing_TODAY_TV_SHOWS_UPDATES object:nil userInfo:nil];
                
            }
            
        }
        if (!_connectedToInternet && [[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_TIME_AIRING_TODAY_TV_SHOWS] withFormat:PRIMARY_DATE_FORMAT ]]>2*ONE_HOUR_IN_SECONDS){
            [[NSNotificationCenter defaultCenter] postNotificationName:infoOutdatedNotification object:nil userInfo:nil];
        }
    }
    
}
- (void)refreshTVShowsTopRated {
    
    self.topRatedTVShowPageNumber=1;
    self.topRatedTVShowsS = [NSMutableArray array];
    self.retrievedFromDBTV = NO;
    
    if (_connectedToInternet && ([[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_TIME_TOP_RATED_TV_SHOWS] withFormat:PRIMARY_DATE_FORMAT ]]>ONE_HOUR_IN_SECONDS)){
        [MYUCache  sharedCache].lastTopRatedTVShows = [NSMutableArray array];
        [self updateTVShowsTopRated];
    }else{
        
        self.airingTodayTVShowsError= allowOnlyOneError;
        self.latestTVShowsError= allowOnlyOneError;
        
        if ([MYUCache  sharedCache].lastTopRatedTVShows==nil || [MYUCache  sharedCache].lastTopRatedTVShows.count <=0 ){
            
            self.topRatedTVShowsFromDB = [MYUCoreDataConnector searchResultsForID :TVHSHOWTOPRATED];
            if (self.topRatedTVShowsFromDB.count>0){
                self.retrievedFromDBTV = YES;
                self.topRatedTVShowsError = nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:TOP_RATED_TV_SHOWS_UPDATES  object:nil userInfo:nil];
                
            }else{
                [self updateTVShowsTopRated];
            }
        }else{
            self.topRatedTVShowsError = nil;
            self.completedTVShowOperation = NO;
            const NSArray* topRatedTVShows = [MYUCache  sharedCache].lastTopRatedTVShows;
            const NSUInteger count = topRatedTVShows.count;
            
            for (NSDictionary* dic in topRatedTVShows){
                self.topRatedTVShowPageNumber++;
                self.topRatedTVShows = dic;
                if(count==self.topRatedTVShowPageNumber){
                    self.completedTVShowOperation = YES;
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:TOP_RATED_TV_SHOWS_UPDATES object:nil userInfo:nil];
            }
            
        }
        
        if (!_connectedToInternet &&[[NSDate date] timeIntervalSinceDate:[MYUUtilities dateFromString:[MYUUserDefaults  stringForKey:LAST_UPDATE_TIME_TOP_RATED_TV_SHOWS] withFormat:PRIMARY_DATE_FORMAT ]]>ONE_HOUR_IN_SECONDS){
            [[NSNotificationCenter defaultCenter] postNotificationName:infoOutdatedNotification object:nil userInfo:nil];
        }
    }
    
    
}

- (void)updateTVShowsLatest {
    NSURL *url = [NSURL URLWithString:[self urlForLatestTVShows]];
    self.airingTodayTVShowsError= allowOnlyOneError;
    self.topRatedTVShowsError= allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _latestTVShows =(NSDictionary*) responseObject;
        if (_latestTVShows!=nil){
            _latestTVShowsError = nil;
            
            const int pages = (int)[(NSNumber*)[_latestTVShows objectForKey:TOTAL_PAGES] integerValue];
            if (pages-self.latestTVShowsPageNumber>0 &&  self.latestTVShowsPageNumber<MAXIMUM_NUMBER_OF_PAGES){
                self.latestTVShowsPageNumber++;
                [self updateTVShowsLatest];
                [self.latestTVShowsS addObject:_latestTVShows];
                [[MYUCache  sharedCache].lastLatestTVShows addObject:[_latestTVShows copy]];
                [[NSNotificationCenter defaultCenter] postNotificationName:LATEST_TV_SHOWS_UPDATES object:nil userInfo:nil];
            }else{
                self.completedTVShowOperation= YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:LATEST_TV_SHOWS_UPDATES object:nil userInfo:nil];
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate date] withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_LATEST_TV_SHOWS];
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSNumber* retry = error.userInfo[@"Retry-After"];
        if([retry isKindOfClass:[NSNull class]] || self.latestTVShowsPageNumber>0){
            self.completedTVShowOperation= YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:LATEST_TV_SHOWS_UPDATES object:nil userInfo:nil];
        }else{
            self.latestTVShows= nil;
            self.latestTVShowsError = error;
            [[NSNotificationCenter defaultCenter] postNotificationName:LATEST_TV_SHOWS_UPDATES object:nil userInfo:nil];
        }
        
    }];
    
}

- (void)updateTVShowsTopRated {
    NSURL *url = [NSURL URLWithString:[self urlForTopRatedTVShows]];
    self.airingTodayTVShowsError= allowOnlyOneError;
    self.latestTVShowsError= allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _topRatedTVShows =(NSDictionary*) responseObject;
        if (_topRatedTVShows!=nil){
            _topRatedTVShowsError = nil;
            const int pages = (int)[(NSNumber*)[_topRatedTVShows objectForKey:TOTAL_PAGES] integerValue];
            if (pages-self.topRatedTVShowPageNumber>0 &&  self.topRatedTVShowPageNumber<MAXIMUM_NUMBER_OF_PAGES){
                self.topRatedTVShowPageNumber++;
                [self updateTVShowsTopRated];
                [self.topRatedTVShowsS addObject:_topRatedTVShows];
                [[MYUCache  sharedCache].lastTopRatedTVShows addObject:[_topRatedTVShows copy]];
                [[NSNotificationCenter defaultCenter] postNotificationName:TOP_RATED_TV_SHOWS_UPDATES object:nil userInfo:nil];
                
            }else{
                self.completedTVShowOperation= YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:TOP_RATED_TV_SHOWS_UPDATES object:nil userInfo:nil];
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate date] withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_TIME_TOP_RATED_TV_SHOWS];
                
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSNumber* retry = error.userInfo[@"Retry-After"];
        if([retry isKindOfClass:[NSNull class]] || self.topRatedTVShowPageNumber>1){
            self.completedTVShowOperation= YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:TOP_RATED_TV_SHOWS_UPDATES object:nil userInfo:nil ];
        }else{
            
            self.topRatedTVShows= nil;
            self.topRatedTVShowsError = error;
            [[NSNotificationCenter defaultCenter] postNotificationName:TOP_RATED_TV_SHOWS_UPDATES object:nil userInfo:nil];
        }
    }];
    
}

- (void)updateTVShowsAiringToday {
    NSURL *url = [NSURL URLWithString:[self urlForAiringTodayTVShows]];
    _latestTVShowsError  = allowOnlyOneError;
    _topRatedTVShowsError= allowOnlyOneError;
    
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _airingTodayTVShows =(NSDictionary*) responseObject;
        if (_airingTodayTVShows!=nil){
            _airingTodayTVShowsError = nil;
            int pages = (int)[(NSNumber*)[_airingTodayTVShows objectForKey:TOTAL_PAGES] integerValue];
            if (pages-self.airingTodayTVShowPageNumber>0 &&  self.airingTodayTVShowPageNumber<MAXIMUM_NUMBER_OF_PAGES){
                self.airingTodayTVShowPageNumber++;
                [self updateTVShowsAiringToday];
                [self.airingTodayTVShowsS addObject:_airingTodayTVShows];
                [[MYUCache  sharedCache].lastAiringTodayTVShows addObject:[_airingTodayTVShows copy]];
                [[NSNotificationCenter defaultCenter] postNotificationName:airing_TODAY_TV_SHOWS_UPDATES object:nil userInfo:nil];
            }else{
                self.completedTVShowOperation= YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:airing_TODAY_TV_SHOWS_UPDATES object:nil userInfo:nil];
                [MYUUserDefaults storeString:[MYUUtilities stringFromDate:[NSDate date] withFormat:PRIMARY_DATE_FORMAT] forKey:LAST_UPDATE_TIME_AIRING_TODAY_TV_SHOWS];
                
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSNumber* retry = error.userInfo[@"Retry-After"];
        if([retry isKindOfClass:[NSNull class]] ||  self.airingTodayTVShowPageNumber>0){
            self.completedTVShowOperation= YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:airing_TODAY_TV_SHOWS_UPDATES object:nil userInfo:nil];
        }else{
            self.completedTVShowOperation= YES;
            self.airingTodayTVShows= nil;
            self.airingTodayTVShowsError = error;
            [[NSNotificationCenter defaultCenter]  postNotificationName:airing_TODAY_TV_SHOWS_UPDATES object:nil userInfo:nil];
        }
    }];
    
}
-(void) searchTVShows:(BOOL) latestOnly
        titleKeyWord : (NSString*) titleKeyWord
        actorKeyWord : (NSString*) actorKeyWord
              genres : (NSArray*)    genreIDs
       ratingLowBound: (float)       ratingLowBound
     ratingUpperBound: (float)       ratingUpperBound
               others: (NSDictionary*) other{
    
    sharedWebMYUWebOperator.laodingTVShows = YES; // reset the completion flag
    self.tvShowSearchPageNumber = 1;           // reset the page number counter
    self.TVSHowsInSearch = [NSMutableArray array];  // clear the results container
    
    if (_connectedToInternet==NO){
        return ;
    }
    if (latestOnly){
        [self updateSearchedTVShowWithinTheLastYear];
    }else{
        [self updateSearchedTVs:titleKeyWord];
    }
    
}

-(void) searchMovies:(BOOL) latestOnly
       titleKeyWord : (NSString*) titleKeyWord
       actorKeyWord : (NSString*) actorKeyWord
             genres : (NSArray*)    genreIDs
      ratingLowBound: (float)       ratingLowBound
    ratingUpperBound: (float)       ratingUpperBound
              others: (NSDictionary*) other{
    
    sharedWebMYUWebOperator.loadingMovies = YES; // reset the completion flag
    self.movieSearchPageNumber = 1;           // reset the page number counter
    self.moviesInSearch = [NSMutableArray array];  // clear the results container
    
    if (_connectedToInternet==NO){
        return ;
    }
    if (latestOnly){
        [self updateSearchedMoviesWithinTheLastYear];
    }else{
        [self updateSearchedMovies:titleKeyWord];
    }
    
}
- (void)updateSearchedMovies:(NSString*) titleKeyWord{
    NSURL *url = [NSURL URLWithString:[self urlForSearchingMovie:titleKeyWord]];
    usleep(1000);
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _searchedMovies =(NSDictionary*) responseObject;
        if (_searchedMovies!=nil){
            _searchedMoviesError = nil;
            int pages = (int)[(NSNumber*)[_searchedMovies objectForKey:TOTAL_PAGES] integerValue];
            if (pages<=self.movieSearchPageNumber){
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_MOVIES_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            @"YES", @"completedMovies",
                            [NSNumber numberWithInt:pages ],@"pages",
                            [NSNumber numberWithInt: self.movieSearchPageNumber ],@"currentPages",
                            nil]
                 ];
                return;
            }else{
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_MOVIES_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithInt: self.movieSearchPageNumber ],@"pages",
                            [NSNumber numberWithInt: self.movieSearchPageNumber ],@"currentPages",
                            @"NO", @"completedMovies", nil]
                 ];
                self.movieSearchPageNumber++;
                [self updateSearchedMovies:titleKeyWord];
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        _searchedMovies= nil;
        _searchedMoviesError = error;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:SEARCH_MOVIES_UPDATES
         object:nil
         userInfo:nil
         ];
    }];
    
}

-(void) updateSearchedMoviesWithinTheLastYear{
    NSURL *url = [NSURL URLWithString:[self urlForSearchingMovingInTheLast365Days]];
    usleep(1000);
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _searchedMovies =(NSDictionary*) responseObject;
        if (_searchedMovies!=nil){
            _searchedMoviesError = nil;
            int pages = (int)[(NSNumber*)[_searchedMovies objectForKey:TOTAL_PAGES] integerValue];
            if (pages<=self.movieSearchPageNumber){
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_MOVIES_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithInt:pages ],@"pages",
                            [NSNumber numberWithInt: self.movieSearchPageNumber ],@"currentPages",
                            @"YES", @"completedMovies",nil]
                 ];
                return;
            }else{
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_MOVIES_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithInt:pages ],@"pages",
                            [NSNumber numberWithInt: self.movieSearchPageNumber ],@"currentPages",
                            @"NO", @"completedMovies", nil]
                 ];
                self.movieSearchPageNumber++;
                [self updateSearchedMoviesWithinTheLastYear];
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        _searchedMovies= nil;
        _searchedMoviesError = error;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:SEARCH_MOVIES_UPDATES
         object:nil
         userInfo:nil
         ];
        
    }];
    
}
-(void) updateSearchedTVs:(NSString*) titleKeyWord{
    NSURL *url = [NSURL URLWithString:[self urlForSearchingTV:titleKeyWord]];
    usleep(1000);
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _searchedTVShows =(NSDictionary*) responseObject;
        if (_searchedTVShows!=nil){
            _searchedTVShowError = nil;
            int pages = (int)[(NSNumber*)[_searchedTVShows objectForKey:TOTAL_PAGES] integerValue];
            if (pages<=self.tvShowSearchPageNumber){
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_TV_SHOWS_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            @"YES", @"completedTVShows",
                            [NSNumber numberWithInt:pages ],@"pages",
                            [NSNumber numberWithInt: self.tvShowSearchPageNumber ],@"currentPages",
                            nil]
                 ];
                return;
            }else{
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_TV_SHOWS_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithInt: self.tvShowSearchPageNumber ],@"pages",
                            [NSNumber numberWithInt: self.tvShowSearchPageNumber ],@"currentPages",
                            @"NO", @"completedTVShows", nil]
                 ];
                self.tvShowSearchPageNumber++;
                [self updateSearchedTVs:titleKeyWord];
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        _searchedTVShows= nil;
        _searchedTVShowError = error;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:SEARCH_TV_SHOWS_UPDATES
         object:nil
         userInfo:nil
         ];
        
    }];
    
}
-(void) updateSearchedTVShowWithinTheLastYear{
    NSURL *url = [NSURL URLWithString:[self urlForSearchingTVShowTheLast365Days]];
    usleep(1000);
    [_manager GET:url.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        _searchedTVShows =(NSDictionary*) responseObject;
        if (_searchedTVShows!=nil){
            _searchedTVShowError = nil;
            int pages = (int)[(NSNumber*)[_searchedTVShows objectForKey:TOTAL_PAGES] integerValue];
            if (pages<=self.tvShowSearchPageNumber){
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_TV_SHOWS_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithInt:pages ],@"pages",
                            [NSNumber numberWithInt: self.tvShowSearchPageNumber ],@"currentPages",
                            @"YES", @"completedTVShows",nil]
                 ];
                return;
            }else{
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:SEARCH_TV_SHOWS_UPDATES
                 object:nil
                 userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithInt:self.tvShowSearchPageNumber ],@"pages",
                            [NSNumber numberWithInt: self.tvShowSearchPageNumber ],@"currentPages",
                            @"NO", @"completedTVShows", nil]];
                self.tvShowSearchPageNumber++;
                [self updateSearchedTVShowWithinTheLastYear];
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        _searchedMovies= nil;
        _searchedTVShowError = error;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:SEARCH_TV_SHOWS_UPDATES
         object:nil
         userInfo:nil
         ];
        
    }];
    
}


@end
