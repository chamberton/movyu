//
//  Searches+CoreDataProperties.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "Searches+CoreDataProperties.h"

@implementation Searches (CoreDataProperties)

+ (NSFetchRequest<Searches *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Searches"];
}

@dynamic actors;
@dynamic genres;
@dynamic isMovie;
@dynamic ratingLowerBound;
@dynamic ratingUpperBound;
@dynamic searchID;
@dynamic title;
@dynamic foundMovies;
@dynamic foundTVShows;

@end
