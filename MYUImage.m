//
//  MYUCache.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/13.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUCache.h"
#import "MYUReachability.h"
#import "MYUWebOperator.h"
#import "MYUCoreDataConnector.h"

static MYUCache* sharedCacheInstance=nil;

@interface MYUCache()
@property (nonatomic, strong,readwrite) NSMutableSet* imageNames;
@property (nonatomic, strong,readwrite) NSMutableSet* missingImageNames;
@property (nonatomic, strong,readwrite) NSMutableDictionary* images;
@property (nonatomic, retain,readwrite) NSTimer* clearingTimer;
@property (nonatomic,strong)            NSThread* serviceThread;
@property (nonatomic, retain,readwrite) UIImage* missingPosterPlaceHolderImage;
@property (atomic)           BOOL clearing;
@property BOOL shouldCancel;
@end

@implementation MYUCache


+(MYUCache*) sharedCache{
    
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedCacheInstance = [[MYUCache alloc] init];
    });
    return sharedCacheInstance;
}

-(instancetype) init{
    
    if (sharedCacheInstance==nil){ // check that the singleton is null before creating an instance
        self = [super init];
        if (self){
            [self clearCache:YES];
            [self acquireMovieImagesFromDB];
            sharedCacheInstance = self;
        }
    }
    return sharedCacheInstance; // return the singleton
}

-(void) acquireMovieImagesFromDB{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray<Images*>* arr =[MYUCoreDataConnector getImages];
        NSArray *derivedArray = [arr valueForKey:@"movieTitle"];
        
        _imageNames =[NSMutableSet setWithArray:derivedArray];
        
        NSMutableArray* images = [NSMutableArray array];
        
        // Create the image from the image raw data
        [arr makeObjectsPerformSelector:@selector(addImageToArray:) withObject:images];
        
        _images =[NSMutableDictionary dictionaryWithObjects:images forKeys:derivedArray];
    });
    
}


-(BOOL) addImage:(UIImage*) image
         forName:(NSString*) name{
    
    while(_clearing){ // lock during the clearing of the cache, traversing the array during its modification if to be avoided
        ;
    }
    
    if(![_imageNames containsObject:name]){ // verify that we do not already have the image
        [_imageNames addObject:name];
        [_images setValue:image forKey:name];
        return YES;
    }
    return NO;
}

-(void) saveAll{
    _shouldCancel = NO;
    for (NSString* str in _imageNames){
        
        UIImage* image = [_images valueForKey:str];
        
        if (image){
            if ([UIApplication sharedApplication].applicationState==UIApplicationStateBackground || _shouldCancel){
              
                if ([[NSThread currentThread] isMainThread]) {   // Requires main thread
                    [MYUCoreDataConnector updateOrInsertImage:image withID:str];
                }
            }else{
                break;
            }
            
        }else{
            continue;
        }
    }
}
-(void)cancelSaving{
    _shouldCancel = YES;
}

-(void) addToMissingImages:(NSString*) title{
    
    if(![_missingImageNames containsObject:title]){
        [_missingImageNames addObject:title];
    }
}

- (UIImage *)imageForMovieTitle:(NSString*) title{
    
    while(_clearing){
        ;
    }
    UIImage* image;
    
    if([_imageNames containsObject:title]){
        image = [_images objectForKey:title];
    }
   
    return image;
}

-(void) clearCache:(BOOL) forceFully{
    
    [MYUReachability checkForReachability];
    if ([MYUWebOperator sharedWebOperator].connectedToInternet || forceFully){
        if([MYUUtilities checkAndSet:&_clearing]){
            _imageNames         = [NSMutableSet set];
            
            _images             = [NSMutableDictionary dictionary];
            _missingImageNames  = [NSMutableSet set];
            _lastMoviesInCinema = [NSMutableArray array];
            _lastMostPopularInCinema= [NSMutableArray array];
            _lastLatestTVShows     = [NSMutableArray array];
            _lastTopRatedTVShows   = [NSMutableArray array];
            _lastAiringTodayTVShows = [NSMutableArray array];
            
            
        }
    }
    _clearing = NO;
}


#pragma Cache Maintenance

-(void) startClearingThread{
    
    if (_clearingTimer.isValid){
        DebugLog(@"Clearing thread  already running");
    }else{
        
        [self.serviceThread cancel];
        self.serviceThread = [[NSThread alloc] initWithTarget:self selector:@selector(runServince) object:nil];
        [self.serviceThread start];
    }
}
-(void) stopClearingThread{
    if ( _clearingTimer.isValid){
        [_clearingTimer invalidate];
    }
    [self.serviceThread cancel];
    
}
- (void)runServince
{
    if(self.clearingTimer && self.clearingTimer.valid){
        [self.clearingTimer invalidate];
    }
    // clearCache every hour
    dispatch_async(dispatch_get_main_queue(), ^{
        self.clearingTimer = [NSTimer scheduledTimerWithTimeInterval:ONE_HOUR_IN_SECONDS target:self selector:@selector(clearCacheForcefully) userInfo:nil repeats:YES];
    });
    
}
-(void)clearCacheForcefully{
    [self clearCache:NO];
}


-(UIImage*) missingPicture{
    if (_missingPosterPlaceHolderImage==nil)
        _missingPosterPlaceHolderImage =[ UIImage imageNamed:MISSING_POSTER];
    return _missingPosterPlaceHolderImage;
}



-(void) dealloc{
    [self stopClearingThread];
    [_clearingTimer invalidate];
}
@end
