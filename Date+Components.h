//
//  Date+Components.h
//  MovYU
//  Categorie for NSDate, adding possibilites of directly acquiring the year, month and day as integers
//  Created by Serge Mbamba on 2016/10/15.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

@interface NSDate(Date_Components)

/*
 *  returns the year of date in integer, 0 if null
 */
-(NSUInteger) year;
/*
 *  returns the month of date in integer, 0 if null
 */
-(NSUInteger) month;
/*
 *  returns the day of date in integer,0 if null
 */
-(NSUInteger) day;
@end
