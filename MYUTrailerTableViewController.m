//
//  MYUTrailerTableViewController.m
//  MovYU
//
//  Created by Serge Mbamba on 2017/01/22.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "MYUTrailerTableViewController.h"
#import "MYUVideoViewController.h"

@interface MYUTrailerTableViewController ()

@end

@implementation MYUTrailerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.trailerList.count;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"playTrailer" sender:self.trailerList[indexPath.row]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"trailerListCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.detailTextLabel.text = self.trailerList[indexPath.row].title;
    cell.textLabel.text = self.trailerList[indexPath.row].type;
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"playTrailer"]){
        MYUVideoViewController* destinationVC = segue.destinationViewController;
        destinationVC.item = sender;
    }
}

@end
