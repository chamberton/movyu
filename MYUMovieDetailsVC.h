//
//  MYUMovieDetailsVC.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/14.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//


#import "MYUMovieItem.h"

@interface MYUMovieDetailsVC : UIViewController<UIScrollViewDelegate>
@property (retain, nonatomic,readwrite) MYUMovieItem *movieItem;

-(void) updateGenreTextField;

-(BOOL) shouldShowMore;
@end
