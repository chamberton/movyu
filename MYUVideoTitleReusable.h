//
//  MUYVideoTitleReusable.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//



@interface MYUVideoTitleReusable : UICollectionReusableView

@property (nonatomic, strong, readwrite) UILabel *titleLabel;

@end
