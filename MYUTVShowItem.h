//
//  MYUTVShowItem.h
//  MovYU
//  TV show item
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//


#import "MYUAbstractVideoItem.h"
#import "MYUDefinitions.h"
#import "MYUVideoProtocol.h"

@interface MYUTVShowItem : MYUAbstractVideoItem<isVideoItem>

@property ( readwrite,nonatomic) NSMutableSet* genreIDS;
@property ( readwrite,nonatomic) NSMutableSet* actors;
@property ( readwrite,nonatomic) NSString*  synopsis;
@property float popularity;
@property float voteAverage;
@property ( readwrite,nonatomic) NSDate* airDate;
@property ( readwrite,nonatomic) NSMutableArray* seasons;
@property ( readwrite,nonatomic) NSArray* originCountries;
@property ( readwrite,nonatomic) NSArray* Languages;
@property int64_t tvShowID;

@end
