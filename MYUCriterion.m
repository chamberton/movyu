//
//  MYUGenre.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/03.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUCriterion.h"

@implementation MYUCriterion
-(instancetype) init{
    self = [super init];
    if (self){
        _selected = NO;
    }
    return self;
}
@end
