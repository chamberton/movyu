//
//  Utilities.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//
#include <sys/utsname.h>

static NSMutableDictionary* dictionaryOfVideoItemDimensions;
static NSDictionary* deviceTypesDictionary;
static CGRect screenDimensions;

@implementation MYUUtilities
+(void) load{
    
    screenDimensions = [[UIScreen mainScreen] bounds];
    int code[] =     {kiPhone1G   ,kiPhone3G    ,kiPhone3GS  ,kiPhone4     ,kiPhone4Verizon ,kiPhone4S    ,kiPhone5GSM ,kiPhone5CDMA,
        kiPhone5CGSM,kiPhone5CCDMA,kiPhone5SGSM,kiPhone5SCDMA,kiPhone6P       ,kiPhone6      ,kiPhone6SP ,kiPhone6S,
        kiPodTouch1G,kiPodTouch2G ,kiPodTouch3G,kiPodTouch4G ,kiPodTouch5G    ,kiPad         ,kiPad2Wifi ,kiPad2GSM,
        kiPad2CMDA  , kiPad2Wifi  ,kiPadMiniWifi,kiPadMiniGSM,kiPadMiniCDMA   ,kiPad3Wifi    ,kiPad3CMDA  ,kiPad3GSM   ,
        kiPad4Wifi  ,kiPad4GSM    ,kiPad4CMDA  ,kiPadAirWifi ,kiPadAirCellular,kiPadMini2GWifi,kiPadMini2GCellular,kSimulator,
        kSimulator};
    NSMutableArray *deviceCodes = [NSMutableArray array];
    int size  = sizeof(code)/sizeof(int);
    for (NSInteger i = 0; i < size; i++)
        [deviceCodes addObject:[NSNumber numberWithInteger:code[i]]];
    NSArray* deviceNames = @[ @"iPhone1,1",@"iPhone1,2",@"iPhone2,1",@"iPhone3,1",@"iPhone3,2",@"iPhone4,1",@"iPhone5,1",@"iPhone5,2",
                              @"iPhone5,3",@"iPhone5,4",@"iPhone6,1",@"iPhone6,2",@"iPhone7,1",@"iPhone7,2",@"iPhone8,2",@"iPhone8,1",
                              @"iPod1,1"  ,@"iPod2,1"  ,@"iPod3,1"  ,@"iPod4,1"  ,@"iPod5,1"  ,@"iPad1,1"  ,@"iPad2,1"  ,@"iPad2,2",
                              @"iPad2,3"  ,@"iPad2,4"  ,@"iPad2,5"  ,@"iPad2,6"  ,@"iPad2,7"  ,@"iPad3,1"  ,@"iPad3,2"  ,@"iPad3,3",
                              @"iPad3,4",  @"iPad3,5"  ,@"iPad3,6"  ,@"iPad4,1",  @"iPad4,2"  ,@"iPad4,4"  ,@"iPad4,5"  ,@"i386",
                              @"x86_64"];
    // set all the devices name and types
    deviceTypesDictionary =[NSDictionary dictionaryWithObjects:deviceCodes forKeys:deviceNames];
}

+(NSUInteger) videoItemPerRowOnOrientation : (UIInterfaceOrientation) orientation{
    switch (orientation) {
        case   UIDeviceOrientationLandscapeLeft:
        case   UIDeviceOrientationLandscapeRight:
            
            return 3;
            
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            
            return 2;
        default:
            return 2;
            
    }
}

+(NSUInteger) videoItemPerRowOnDeviceOrientation : (UIDeviceOrientation) orientation{
       switch (orientation) {
        case   UIDeviceOrientationLandscapeLeft:
        case   UIDeviceOrientationLandscapeRight:
            
            return 3;
            
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            
            return 2;
        default:
            return 2;
            
    }
}

+ (NSDictionary *)sizeForVideoItem:(UIDeviceOrientation)orientation{
    
    dictionaryOfVideoItemDimensions = [NSMutableDictionary dictionaryWithCapacity:4];
    NSNumber* verticalSpacing = [NSNumber numberWithFloat:12.0f];
    
    NSNumber* titleHeight ;
    NSNumber* numberOfColumns ;
    CGFloat width ;
    CGFloat height;
    
    if (orientation==UIDeviceOrientationLandscapeLeft || orientation==UIDeviceOrientationLandscapeRight){
        numberOfColumns = [NSNumber numberWithUnsignedInteger:3];
        titleHeight = [NSNumber numberWithFloat:20.0f];
        width  = CGRectGetHeight(screenDimensions)/[numberOfColumns  floatValue]- 30.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            height= 300.0f;
        }else

        if ([MYUUtilities deviceType]<kiPhone6P) // limit size to 150 for iPhone5 and lower
            height= 150.0f;
        else
            height= 200.0f;
    }else{
        height= 250.0f;
        numberOfColumns = [NSNumber numberWithUnsignedInteger:2];
        titleHeight = [NSNumber numberWithFloat:26.0f];
        width=CGRectGetWidth(screenDimensions)/[numberOfColumns  floatValue] - 30.0f;
    }
    
    
    CGSize size = CGSizeMake(width,height);
    NSValue *sizeObj = [NSValue valueWithCGSize:size];
    [dictionaryOfVideoItemDimensions setValue:sizeObj            forKey:ITEM_SIZE];
    [dictionaryOfVideoItemDimensions setValue:titleHeight        forKey:TITLE_HEIGHT];
    [dictionaryOfVideoItemDimensions setValue:numberOfColumns    forKey:NUMER_OF_COLUMN];
    [dictionaryOfVideoItemDimensions setValue:verticalSpacing    forKey:VERTICAL_SPACING];
    
    return dictionaryOfVideoItemDimensions;
}

+ (NSDictionary *)sizeForVideoItemInterface:(UIInterfaceOrientation)orientation{
    
    dictionaryOfVideoItemDimensions = [NSMutableDictionary dictionaryWithCapacity:4];
    NSNumber* verticalSpacing = [NSNumber numberWithFloat:12.0f];
    
    NSNumber* titleHeight ;
    NSNumber* numberOfColumns ;
    CGFloat width ;
    CGFloat height;
    
    if (orientation==UIDeviceOrientationLandscapeLeft || orientation==UIDeviceOrientationLandscapeRight){
        numberOfColumns = [NSNumber numberWithUnsignedInteger:3];
        titleHeight = [NSNumber numberWithFloat:20.0f];
        width  = CGRectGetHeight(screenDimensions)/[numberOfColumns  floatValue]- 30.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            height= 300.0f;
        }else
            
            if ([MYUUtilities deviceType]<kiPhone6P) // limit size to 150 for iPhone5 and lower
                height= 150.0f;
            else
                height= 200.0f;
    }else{
        height= 250.0f;
        numberOfColumns = [NSNumber numberWithUnsignedInteger:2];
        titleHeight = [NSNumber numberWithFloat:26.0f];
        width=CGRectGetWidth(screenDimensions)/[numberOfColumns  floatValue] - 30.0f;
    }
    
    
    CGSize size = CGSizeMake(width,height);
    NSValue *sizeObj = [NSValue valueWithCGSize:size];
    [dictionaryOfVideoItemDimensions setValue:sizeObj            forKey:ITEM_SIZE];
    [dictionaryOfVideoItemDimensions setValue:titleHeight        forKey:TITLE_HEIGHT];
    [dictionaryOfVideoItemDimensions setValue:numberOfColumns    forKey:NUMER_OF_COLUMN];
    [dictionaryOfVideoItemDimensions setValue:verticalSpacing    forKey:VERTICAL_SPACING];
    
    return dictionaryOfVideoItemDimensions;
}


+(NSDate*) dateFromString:(NSString*) dateString
               withFormat:(NSString*) format{
    
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    if (format==nil){
        DebugLog(@"Specified date format is null");
       return [NSDate dateWithTimeIntervalSince1970:0];
    }
    [df setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [df setDateFormat:format];
    if (df==nil || dateString==nil){
        DebugLog(@"could not set date formatter to format or date string is null ");
        return [NSDate dateWithTimeIntervalSince1970:0];
    }
    [df setLocale:posix];
    return [df dateFromString: dateString];
    
}

+(NSString*) stringFromDate:(NSDate*) date
                 withFormat:(NSString*) format{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    if (format==nil){
        DebugLog(@"Specified date format is null");
        return nil;
    }
    [df setDateFormat:format];
    if (df==nil || date==nil){
        DebugLog(@"could not set date formatter to format or date string is null");
        return nil;
    }
    return [df stringFromDate:date];
    
}

+(NSString*) formatStringWithDecimalOutPut:(float) floatingNumber
                      numberOfDecimalPoint:(int) decimalPoint{
    NSString* format = [NSString stringWithFormat:@"%%0.%df",decimalPoint];
    NSString* formattedOutput = [NSString stringWithFormat:format, floatingNumber];
    return formattedOutput;
}

+(UIImage*)imageFromColor:(UIColor*) color
                 withSize:(CGRect) rect{
    if (color==nil)
        return nil;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,[color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


+(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
           withFont :(UIFont*) font
{
    if (text==nil || text.length==0 || font==nil)
        return image;
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle};
    
    [text drawInRect:rect withAttributes:attributes] ;
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
+(CGRect)videoItemSize{
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    [MYUUtilities  sizeForVideoItem:(UIDeviceOrientation) orientation];
    return [(NSValue*)[dictionaryOfVideoItemDimensions  objectForKey:ITEM_SIZE] CGRectValue];
}

+(CGRect)defaultImageSize{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
         return CGRectMake(0, 0, 50, 175);
    }
    return CGRectMake(0, 0, 15, 125);
}

+(BOOL) checkAndSet:(BOOL*) running{
    if(!running){  // if the pointer is null
        *running = YES;
        return false;
    }
    if (!(*running)){
        *running = YES;
        return YES;
    }
    return NO;
}
+ (NSString*) platformCode {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString* platform =  [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    return platform;
}

+ (NSString*) platformName {
    NSString* platform = [MYUUtilities platformCode];
    return platform;
}

+(DeviceType) deviceType{
    NSString *platform = [MYUUtilities platformCode];
    NSNumber* type =(NSNumber*)[deviceTypesDictionary valueForKey:platform] ;
    return type==nil?kUnknownPlatform:[type intValue];
}
+ (UIImage *)resizeImage:(UIImage *)image ToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
JPEGCompressionLevel next( JPEGCompressionLevel level){
    switch (level) {
        case lowest:
            return low;
        case low:
            return medium;
        case medium:
            return high;
        case high:
            return highest;
        default:
            return highest;
    }
    return highest;
}

+(NSData*) compress : (UIImage*) image
          desiredSize: (float) desiredSize
               isJPEG: (BOOL) isJPEG {
    JPEGCompressionLevel compressionRateToUse = low;
    return isJPEG ? UIImageJPEGRepresentation(image, compressionRateToUse*0.25) :  UIImagePNGRepresentation(image);

}
@end
