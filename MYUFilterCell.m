//
//  MYUFilterCell.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/27.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUFilterCell.h"

@implementation MYUFilterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
