//
//  MYUTVShowItem.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUTVShowItem.h"

@implementation MYUTVShowItem
- (NSComparisonResult)compare:(MYUTVShowItem *)otherObject {
    return self.popularity<otherObject.popularity;
}


@end
