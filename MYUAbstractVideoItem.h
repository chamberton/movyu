//
//  MYUAbstractVideoItem.h
//  MovYU
//  Abstract  video item class
//  Created by Serge Mbamba on 2016/10/12.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//


#import "MYUThumbnail.h"
#import "MYUDefinitions.h"



@interface MYUAbstractVideoItem : NSObject
@property (nonatomic, readwrite, strong) MYUThumbnail* videoThumbnail;
@property (nonatomic, readwrite, strong) NSDictionary* details;
@property (nonatomic, readwrite, strong) NSString* title;
@property (nonatomic, readwrite, strong) NSString* imageURL;
@property (nonatomic) NSUInteger rating;
@property ( readwrite,nonatomic) NSString* combinedActors;
@property ( readwrite,nonatomic) NSString* combinedDirectors;
@property ( readwrite,nonatomic) NSString* combinedGenres;
@property ( readwrite,nonatomic) NSString* originalLanguage;
@property ( readwrite,nonatomic) NSString* productionCountry;
@property ( readwrite,nonatomic) NSDate* releaseDate;

@property BOOL isMovie;
@property BOOL isAdultOnly;
// protocol methods, implemented in abstract but abstract not adpating the protocol
- (instancetype) initWithThumbnail   : (MYUThumbnail*) customObject
                           andDetails: (NSDictionary*) details;
-(NSString*)    videoTitle;
-(UIImage*)     videoThumnailImage;

@end
