//
//  TVShows+CoreDataClass.h
//  MovYU
//
//  Created by Serge Mbamba on 2017/04/26.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Searches, Seasons;

NS_ASSUME_NONNULL_BEGIN

@interface TVShows : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TVShows+CoreDataProperties.h"
