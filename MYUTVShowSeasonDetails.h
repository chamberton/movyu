//
//  MYUTVShowSeasonDetails.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYUTVShowItem.h"
@interface MYUTVShowSeasonDetails : UITableViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;
@property (retain, nonatomic,readwrite) MYUTVShowItem *tvShowItem;
@end
