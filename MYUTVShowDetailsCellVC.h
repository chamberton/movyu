//
//  MYUTVShowDetailsCellVC.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/17.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYUTVShowDetailsCellVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *releaseDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *seasonLabel;
@property (weak, nonatomic) IBOutlet UITextField *episodeLabel;
@end
