//
//  MYUTrailerInfo.m
//  MovYU
//
//  Created by Serge Mbamba on 2017/01/23.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "MYUTrailerInfo.h"
#import "MYUStringFactory.h"
@implementation MYUTrailerInfo


-(instancetype) initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self){
        id value = dictionary[@"id"];
        self.itemID =  (value && ![value isKindOfClass:[NSNull class]])?value:nil;
        
        value = dictionary[@"iso_639_1"]; // language specificaion : iso_3166_1
        
        self.languageSecond = (value && ![value isKindOfClass:[NSNull class]])?value:nil;
        self.languageMain  = [MYUStringFactory fullLanguageNameFromLanguageCode:self.languageSecond];
        value = dictionary[@"key"];
        
        self.key = (value && ![value isKindOfClass:[NSNull class]])?value:nil;
        
        value = dictionary[@"name"];
        self.title =(value && ![value isKindOfClass:[NSNull class]])?value:nil;
        
        value = dictionary[@"site"];
        self.site =(value && ![value isKindOfClass:[NSNull class]])?value:nil;
        
        value = dictionary[@"size"];
        self.size =(value && ![value isKindOfClass:[NSNull class]])?value:nil;
        
        value = dictionary[@"type"];
        self.type =(value && ![value isKindOfClass:[NSNull class]])?value:nil;
    }
    
    return self;
}
@end
