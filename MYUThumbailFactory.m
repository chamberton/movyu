//
//  MYUThumbailFactory.m
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/18.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUThumbailFactory.h"
#import "MYUCoreDataConnector.h"

@implementation MYUThumbailFactory

+ (MYUThumbnail *)thumnailWithImageURL:(NSURL *)imageURL
                              andTitle: (NSString*) title
{
    if (title==nil)
        return nil;
    
    @autoreleasepool {
        MYUThumbnail* obj = [[MYUThumbnail alloc] init];
        obj.imageURL = imageURL;
        
        if (imageURL==nil)
        {
            obj.poster = [[MYUCache sharedCache] missingPicture];
        }
        
        obj.title = title;
        return obj;
    }
    
}

+ (MYUThumbnail *)thumnailWithImage: (UIImage*) image
                           andTitle:(NSString*) title{
    if (title==nil)
        return nil;
    @autoreleasepool {
        MYUThumbnail* obj = [[MYUThumbnail alloc] init];
        if (image==nil){
            obj.poster = [[MYUCache sharedCache] missingPicture];
        }
        else
            obj.poster = image ;
        obj.title = title;
        return obj;
    }
}
+ (UIImage *)imageWithTitle:(NSString *) title
                     andURL: (NSURL*) imageURL{
    
    if (imageURL && title) {
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData scale:[UIScreen mainScreen].scale];
       
        @autoreleasepool {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
            dispatch_async(queue, ^{
                if ([[MYUCache sharedCache] addImage:image forName:title]){
                    [MYUCoreDataConnector updateOrInsertImage:image withID:title];
                }
            });
        }

        if(image){
            return  image;
        }
    }
    
    [[MYUCache sharedCache] addToMissingImages:title];
    return [[MYUCache sharedCache] missingPicture];;
}


@end
