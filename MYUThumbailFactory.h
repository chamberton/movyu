//
//  MYUThumbailFactory.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/10/18.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import "MYUThumbnail.h"
#import "MYUCache.h"

@interface MYUThumbailFactory : NSObject


+ (MYUThumbnail *)thumnailWithImageURL:(NSURL *)imageURL
                              andTitle: (NSString*) title;

+ (MYUThumbnail *)thumnailWithImage: (UIImage*) image
                           andTitle:(NSString*) title;
+ (UIImage *)imageWithTitle:(NSString *) title
                     andURL: (NSURL*) imageURL;
@end
