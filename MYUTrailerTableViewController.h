//
//  MYUTrailerTableViewController.h
//  MovYU
//
//  Created by Serge Mbamba on 2017/01/22.
//  Copyright © 2017 Serge Mbamba. All rights reserved.
//

#import "MYUMovieItem.h"
#import "MYUTrailerInfo.h"
#import <UIKit/UIKit.h>

@interface MYUTrailerTableViewController : UITableViewController

@property (readwrite,nonatomic) MYUAbstractVideoItem* videoItem;
@property (readwrite,nonatomic) NSArray<MYUTrailerInfo *>* trailerList;

@end
