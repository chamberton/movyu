//
//  Seasons+CoreDataClass.h
//  MovYU
//
//  Created by Serge Mbamba on 2016/11/01.
//  Copyright © 2016 Serge Mbamba. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TVShows;

NS_ASSUME_NONNULL_BEGIN

@interface Seasons : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Seasons+CoreDataProperties.h"
